(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-meus-pagamentos-meus-pagamentos-module"], {
    /***/
    "./src/app/pages/meus-pagamentos/meus-pagamentos-routing.module.ts":
    /*!*************************************************************************!*\
      !*** ./src/app/pages/meus-pagamentos/meus-pagamentos-routing.module.ts ***!
      \*************************************************************************/

    /*! exports provided: MeusPagamentosPageRoutingModule */

    /***/
    function srcAppPagesMeusPagamentosMeusPagamentosRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MeusPagamentosPageRoutingModule", function () {
        return MeusPagamentosPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _meus_pagamentos_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./meus-pagamentos.page */
      "./src/app/pages/meus-pagamentos/meus-pagamentos.page.ts");

      var routes = [{
        path: '',
        component: _meus_pagamentos_page__WEBPACK_IMPORTED_MODULE_3__["MeusPagamentosPage"]
      }];

      var MeusPagamentosPageRoutingModule = function MeusPagamentosPageRoutingModule() {
        _classCallCheck(this, MeusPagamentosPageRoutingModule);
      };

      MeusPagamentosPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], MeusPagamentosPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/meus-pagamentos/meus-pagamentos.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/meus-pagamentos/meus-pagamentos.module.ts ***!
      \*****************************************************************/

    /*! exports provided: MeusPagamentosPageModule */

    /***/
    function srcAppPagesMeusPagamentosMeusPagamentosModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MeusPagamentosPageModule", function () {
        return MeusPagamentosPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _meus_pagamentos_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./meus-pagamentos-routing.module */
      "./src/app/pages/meus-pagamentos/meus-pagamentos-routing.module.ts");
      /* harmony import */


      var _meus_pagamentos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./meus-pagamentos.page */
      "./src/app/pages/meus-pagamentos/meus-pagamentos.page.ts");

      var MeusPagamentosPageModule = function MeusPagamentosPageModule() {
        _classCallCheck(this, MeusPagamentosPageModule);
      };

      MeusPagamentosPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _meus_pagamentos_routing_module__WEBPACK_IMPORTED_MODULE_5__["MeusPagamentosPageRoutingModule"]],
        declarations: [_meus_pagamentos_page__WEBPACK_IMPORTED_MODULE_6__["MeusPagamentosPage"]],
        exports: [_meus_pagamentos_page__WEBPACK_IMPORTED_MODULE_6__["MeusPagamentosPage"]]
      })], MeusPagamentosPageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-meus-pagamentos-meus-pagamentos-module-es5.js.map