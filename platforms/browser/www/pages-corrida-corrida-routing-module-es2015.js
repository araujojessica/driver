(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-corrida-corrida-routing-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/corrida/corrida.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/corrida/corrida.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (" <ion-header>\r\n  <nav class=\"navbar navbar-expand-lg navbar-light bg-dark\">\r\n      <a class=\"navbar-brand\" href=\"#\">\r\n      <img src=\"/assets/img/logo-branco.png\" style=\"width: 2.5em;\"/>\r\n      </a>\r\n      <ion-buttons slot=\"end\">\r\n        <ion-menu-button color=\"medium\"></ion-menu-button>\r\n      </ion-buttons>\r\n  </nav>\r\n</ion-header>\r\n<ion-content>\r\n  <div #map>\r\n    \r\n      <div class=\"pagamentos\" [hidden]=\"!destination\" >\r\n\r\n        <div  (click) = \"navegar();\" ><img src=\"/assets/img/navigator.png\" style=\"width: 17%;margin-left: 40%;\" /></div>\r\n        <br>\r\n        <button type=\"button\" class=\"btn btn-primary btn-lg rounded-pill pegarCarro\" style=\"background: red;\" (click)=\"cheguei();\">\r\n          CHEGUEI\r\n        </button>\r\n       <br><br>\r\n        <button type=\"button\" class=\"btn btn-primary btn-lg rounded-pill pegarCarro\" (click)=\"ligar();\">\r\n          LIGAR\r\n        </button>\r\n        <br><br>\r\n        <button (click) = \"cancelar();\" type=\"button\" class=\"btn btn-primary btn-lg rounded-pill pegarCarro\" >\r\n          CANCELAR\r\n        </button>\r\n        <br><br>\r\n        <button type=\"button\" (click)=\"inciarCorrida();\" class=\"btn btn-primary btn-lg rounded-pill pegarCarro\" >\r\n          INICIAR CORRIDA\r\n        </button>\r\n    \r\n      </div>\r\n\r\n  </div>\r\n\r\n  \r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/corrida/corrida-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/corrida/corrida-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: CorridaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CorridaPageRoutingModule", function() { return CorridaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _corrida_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./corrida.page */ "./src/app/pages/corrida/corrida.page.ts");




const routes = [
    {
        path: '',
        component: _corrida_page__WEBPACK_IMPORTED_MODULE_3__["CorridaPage"],
    }
];
let CorridaPageRoutingModule = class CorridaPageRoutingModule {
};
CorridaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], CorridaPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/corrida/corrida.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/corrida/corrida.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@charset \"UTF-8\";\n#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n#container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n#container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #8c8c8c;\n  margin: 0;\n}\n#container a {\n  text-decoration: none;\n}\n#map {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  background: #eee;\n}\n.boxConteudo {\n  background: white;\n  padding: 10px;\n  border-radius: 20px;\n  outline: 0;\n  box-shadow: 0px 0px 10px #e4e4e4;\n  position: absolute !important;\n  display: block;\n  margin-left: 13px;\n  margin-right: auto;\n  z-index: 9999 !important;\n  /* número máximo é 9999 */\n  width: calc(100% - 25px) !important;\n}\n.pagamentos {\n  background: white;\n  padding: 10px;\n  border-radius: 20px;\n  box-shadow: 0px 0px 10px #e4e4e4;\n  position: fixed !important;\n  z-index: 9999 !important;\n  /* número máximo é 9999 */\n  width: 100%;\n  bottom: 0;\n}\nbutton.btn.btn-primary.btn-lg.rounded-pill.escolherDestino {\n  font-size: 15px;\n}\n.boxConteudo label {\n  text-transform: uppercase;\n  font-size: 12px;\n}\n.boxConteudo .form-control {\n  height: calc(1.5em + .75rem + 4px);\n}\n.buttonArea button {\n  width: 100%;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.buttonArea svg {\n  font-size: 25px;\n  margin-right: 5px;\n}\nbutton.navbar-toggler {\n  border-radius: 100px !important;\n  height: 55px !important;\n  background: #ffffff;\n  border: #92f328 solid 0px;\n  outline: none;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.boxConteudo .btn {\n  font-weight: bold;\n}\n.mapa iframe {\n  border: 0;\n  width: 100%;\n  height: 100vh;\n  position: relative;\n  top: 0px;\n  z-index: -1;\n}\n.saudacao {\n  text-align: center;\n}\n.homePassageiro .boxConteudo {\n  position: absolute !important;\n  top: 30px;\n  width: calc(100% - 0px);\n  margin: 0 auto;\n  padding: 15px 5px;\n}\n.homePassageiro .buttonArea button.rounded-pill {\n  font-size: 15px;\n}\nbutton.escolherDestino {\n  background: #001c4e;\n  border-color: #001c4e;\n  width: calc(100% - 60px) !important;\n  float: left;\n  background-color: white;\n  border-radius: 100px;\n  width: 50px;\n  height: 50px;\n  color: #fff;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\nbutton.pegarCarro {\n  background: #001c4e;\n  border-color: #001c4e;\n  width: 100%;\n  border-radius: 100px;\n  height: 50px;\n  color: #fff;\n  font-size: 18px;\n  --box-shadow: 0 0 0 0;\n  --outline: 0;\n}\nbutton.embarqueRapido {\n  float: right;\n  padding: 0px 12px;\n  width: 45px !important;\n  height: 45px;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\nbutton.embarqueRapido svg {\n  width: 19px;\n  color: #416313;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\nbutton.btn.btn-primary.btn-lg.rounded-pill.embarqueRapido {\n  background: #b1ff49;\n  border: none;\n  border-radius: 100%;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.formDestino {\n  border: none;\n  margin-top: 5px;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.endDestinoRecente {\n  font-size: 11px;\n  background: #eee;\n  padding: 14px;\n  border-radius: 5px;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.endDestinoRecente b {\n  font-weight: normal;\n  color: darkgray;\n}\n.tituloDestinoRecente {\n  text-transform: uppercase;\n  font-weight: bold;\n}\n.navbar-collapse {\n  position: fixed;\n  top: 0px;\n  left: 0;\n  padding-left: 15px;\n  padding-right: 15px;\n  padding-bottom: 15px;\n  width: 75%;\n  height: 100%;\n  background: #001c4e;\n  text-transform: uppercase;\n  z-index: 9999;\n  box-shadow: 0px 0px 90px black;\n}\n.navbar-collapse a.nav-link {\n  color: white !important;\n  font-weight: bold;\n  font-size: 19px;\n}\n.navbar-dark .navbar-toggler {\n  background: #001c4e;\n}\n.navbar-dark span.navbar-toggler-icon {\n  padding: 15px;\n}\n.navbar-collapse.collapsing {\n  left: -75%;\n  transition: height 0s ease;\n}\n.navbar-collapse.show {\n  margin-top: 0px;\n  left: 0;\n  transition: left 300ms ease-in-out;\n}\n.navbar-toggler.collapsed ~ .navbar-collapse {\n  transition: left 500ms ease-in-out;\n}\n.navbar-light .navbar-toggler-icon {\n  padding: 15px;\n  background-color: white !important;\n  border-radius: 100px;\n}\n.animacao {\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n  margin-top: 50%;\n  width: 50px;\n  height: 50px;\n  border: 7px solid #a9a9a9;\n  border-top-color: #001c4e;\n  border-radius: 50px;\n  -webkit-animation: is-rotating 1.5s infinite;\n          animation: is-rotating 1.5s infinite;\n}\n.position {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: block;\n  position: absolute;\n  top: 5px;\n  left: 10px;\n}\n.navbar-collapse {\n  position: fixed;\n  top: 0px;\n  left: 0;\n  padding-left: 15px;\n  padding-right: 15px;\n  padding-bottom: 15px;\n  width: 75%;\n  height: 100%;\n  background: #001c4e;\n  text-transform: uppercase;\n  z-index: 9999;\n  box-shadow: 0px 0px 90px black;\n}\n.navbar-collapse a.nav-link {\n  color: white !important;\n  font-weight: bold;\n  font-size: 19px;\n}\nbutton.escolherDestino {\n  background: #001c4e;\n  border-color: #001c4e;\n  width: calc(100% - 60px) !important;\n  float: left;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.formDestino {\n  border: none;\n  margin-top: 5px;\n}\n.endDestinoRecente {\n  font-size: 11px;\n  background: #eee;\n  padding: 14px;\n  border-radius: 5px;\n}\n.endDestinoRecente b {\n  font-weight: normal;\n  color: darkgray;\n}\n.tituloDestinoRecente {\n  text-transform: uppercase;\n  font-weight: bold;\n}\nbutton.btn.btn-primary.btn-lg.rounded-pill.escolherDestino {\n  font-size: 15px;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.navbar-dark .navbar-toggler {\n  background: #001c4e;\n}\n.navbar-dark span.navbar-toggler-icon {\n  padding: 15px;\n}\n.navbar-collapse.collapsing {\n  left: -75%;\n  transition: height 0s ease;\n}\n.navbar-collapse.show {\n  margin-top: 0px;\n  left: 0;\n  transition: left 300ms ease-in-out;\n}\n.navbar-toggler.collapsed ~ .navbar-collapse {\n  transition: left 500ms ease-in-out;\n}\n.else {\n  font-size: 1.2rem;\n  display: block;\n  padding-top: 22px;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 20px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n.elseuaiLeva {\n  font-size: 1.2rem;\n  display: block;\n  padding-top: 2px;\n  padding-left: 100px;\n  text-transform: uppercase;\n  margin: 15px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\nspan {\n  font-size: 1rem;\n  display: block;\n  text-align: right;\n  text-transform: uppercase;\n  padding-top: 3px;\n  font-weight: bold;\n  color: #123b7d;\n}\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 10px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n.navbar.bg-dark {\n  background: #001c4e !important;\n}\nion-menu-button {\n  background-color: white;\n  border-radius: 100px;\n  width: 50px;\n  height: 50px;\n}\n.navbar-brand {\n  display: inline-block;\n  padding-top: 0.3125rem;\n  padding-bottom: 0.3125rem;\n  margin-right: 1rem;\n  font-size: 1.25rem;\n  line-height: inherit;\n  white-space: nowrap;\n}\n.navbar {\n  position: relative;\n  display: flex;\n  flex-wrap: wrap;\n  align-items: center;\n  justify-content: space-between;\n  padding: 0.5rem 1rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29ycmlkYS9jb3JyaWRhLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUFBaEI7RUFDRSxrQkFBQTtFQUVBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxRQUFBO0VBQ0EsMkJBQUE7QUFDRjtBQUVBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FBQ0Y7QUFFQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUVBLGNBQUE7RUFFQSxTQUFBO0FBREY7QUFJQTtFQUNFLHFCQUFBO0FBREY7QUFJQTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQURGO0FBS0E7RUFDRSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUVFLFVBQUE7RUFDRixnQ0FBQTtFQUNBLDZCQUFBO0VBRUEsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSx3QkFBQTtFQUEwQix5QkFBQTtFQUMxQixtQ0FBQTtBQUhGO0FBTUE7RUFDRSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGdDQUFBO0VBQ0EsMEJBQUE7RUFDQSx3QkFBQTtFQUEwQix5QkFBQTtFQUMxQixXQUFBO0VBQ0QsU0FBQTtBQUZEO0FBTUE7RUFDRSxlQUFBO0FBSEY7QUFLQTtFQUNFLHlCQUFBO0VBQ0EsZUFBQTtBQUZGO0FBS0E7RUFDRSxrQ0FBQTtBQUZGO0FBS0E7RUFFRSxXQUFBO0VBR0EscUJBQUE7RUFDRSxVQUFBO0FBTEo7QUFRQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQUxBO0FBUUE7RUFDQSwrQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSxxQkFBQTtFQUNFLFVBQUE7QUFMRjtBQVFBO0VBQ0EsaUJBQUE7QUFMQTtBQVFBO0VBQ0UsU0FBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsV0FBQTtBQUxGO0FBUUE7RUFDRSxrQkFBQTtBQUxGO0FBUUE7RUFDRSw2QkFBQTtFQUNBLFNBQUE7RUFDQSx1QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQUxGO0FBUUE7RUFDRSxlQUFBO0FBTEY7QUFRQTtFQUNFLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxtQ0FBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtFQUNBLG9CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EscUJBQUE7RUFDRSxVQUFBO0FBTEo7QUFTQTtFQUNFLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0Esb0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxxQkFBQTtFQUNFLFlBQUE7QUFOSjtBQVNBO0VBQ0UsWUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7RUFDRSxVQUFBO0FBTko7QUFTQTtFQUNFLFdBQUE7RUFDQSxjQUFBO0VBQ0EscUJBQUE7RUFDRSxVQUFBO0FBTko7QUFTQTtFQUNHLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7RUFDQyxVQUFBO0FBTko7QUFTQTtFQUNFLFlBQUE7RUFDQSxlQUFBO0VBQ0EscUJBQUE7RUFDRSxVQUFBO0FBTko7QUFTQTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0UsVUFBQTtBQU5KO0FBU0E7RUFDRSxtQkFBQTtFQUNBLGVBQUE7QUFORjtBQVNBO0VBQ0UseUJBQUE7RUFDQSxpQkFBQTtBQU5GO0FBU0E7RUFDRSxlQUFBO0VBQ0EsUUFBQTtFQUNBLE9BQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0Esb0JBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxhQUFBO0VBQ0EsOEJBQUE7QUFORjtBQVNBO0VBQ0EsdUJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUFOQTtBQVVBO0VBQ0UsbUJBQUE7QUFQRjtBQVVBO0VBQ0UsYUFBQTtBQVBGO0FBWUE7RUFDRSxVQUFBO0VBQ0EsMEJBQUE7QUFURjtBQVlBO0VBQ0UsZUFBQTtFQUNBLE9BQUE7RUFDQSxrQ0FBQTtBQVRGO0FBWUE7RUFDRSxrQ0FBQTtBQVRGO0FBWUE7RUFDRSxhQUFBO0VBQ0Esa0NBQUE7RUFDQSxvQkFBQTtBQVRGO0FBYUE7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFFQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLDRDQUFBO1VBQUEsb0NBQUE7QUFYRjtBQWNBO0VBQ0UsMEJBQUE7RUFBQSx1QkFBQTtFQUFBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7QUFYRjtBQWNBO0VBQ0UsZUFBQTtFQUNBLFFBQUE7RUFDQSxPQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLDhCQUFBO0FBWEY7QUFjQTtFQUNBLHVCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FBWEE7QUFjQTtFQUNFLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxtQ0FBQTtFQUNBLFdBQUE7RUFDQSxxQkFBQTtFQUNFLFVBQUE7QUFYSjtBQWNBO0VBQ0UsWUFBQTtFQUNBLGVBQUE7QUFYRjtBQWFBO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0FBVkY7QUFhQTtFQUNFLG1CQUFBO0VBQ0EsZUFBQTtBQVZGO0FBYUE7RUFDRSx5QkFBQTtFQUNBLGlCQUFBO0FBVkY7QUFZQTtFQUNFLGVBQUE7RUFDQSxxQkFBQTtFQUNFLFVBQUE7QUFUSjtBQVlBO0VBQ0UsbUJBQUE7QUFURjtBQVlBO0VBQ0UsYUFBQTtBQVRGO0FBY0E7RUFDRSxVQUFBO0VBQ0EsMEJBQUE7QUFYRjtBQWNBO0VBQ0UsZUFBQTtFQUNBLE9BQUE7RUFDQSxrQ0FBQTtBQVhGO0FBY0E7RUFDRSxrQ0FBQTtBQVhGO0FBY0E7RUFDRSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBWEY7QUFjRTtFQUNJLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBRUEsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUFaTjtBQWVFO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUFaRjtBQWVFO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQVpGO0FBZUU7RUFDSSw4QkFBQTtBQVpOO0FBZUU7RUFDRSx1QkFBQTtFQUNBLG9CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFaSjtBQWVBO0VBQ0UscUJBQUE7RUFDQSxzQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EsbUJBQUE7QUFaRjtBQWVBO0VBQ0Usa0JBQUE7RUFDQSxhQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7RUFDQSxvQkFBQTtBQVpGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY29ycmlkYS9jb3JyaWRhLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjaGFyc2V0IFwiVVRGLThcIjtcbiNjb250YWluZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG59XG5cbiNjb250YWluZXIgc3Ryb25nIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBsaW5lLWhlaWdodDogMjZweDtcbn1cblxuI2NvbnRhaW5lciBwIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBsaW5lLWhlaWdodDogMjJweDtcbiAgY29sb3I6ICM4YzhjOGM7XG4gIG1hcmdpbjogMDtcbn1cblxuI2NvbnRhaW5lciBhIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuXG4jbWFwIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBiYWNrZ3JvdW5kOiAjZWVlO1xufVxuXG4uYm94Q29udGV1ZG8ge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgb3V0bGluZTogMDtcbiAgYm94LXNoYWRvdzogMHB4IDBweCAxMHB4ICNlNGU0ZTQ7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luLWxlZnQ6IDEzcHg7XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbiAgei1pbmRleDogOTk5OSAhaW1wb3J0YW50O1xuICAvKiBuw7ptZXJvIG3DoXhpbW8gw6kgOTk5OSAqL1xuICB3aWR0aDogY2FsYygxMDAlIC0gMjVweCkgIWltcG9ydGFudDtcbn1cblxuLnBhZ2FtZW50b3Mge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgYm94LXNoYWRvdzogMHB4IDBweCAxMHB4ICNlNGU0ZTQ7XG4gIHBvc2l0aW9uOiBmaXhlZCAhaW1wb3J0YW50O1xuICB6LWluZGV4OiA5OTk5ICFpbXBvcnRhbnQ7XG4gIC8qIG7Dum1lcm8gbcOheGltbyDDqSA5OTk5ICovXG4gIHdpZHRoOiAxMDAlO1xuICBib3R0b206IDA7XG59XG5cbmJ1dHRvbi5idG4uYnRuLXByaW1hcnkuYnRuLWxnLnJvdW5kZWQtcGlsbC5lc2NvbGhlckRlc3Rpbm8ge1xuICBmb250LXNpemU6IDE1cHg7XG59XG5cbi5ib3hDb250ZXVkbyBsYWJlbCB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLmJveENvbnRldWRvIC5mb3JtLWNvbnRyb2wge1xuICBoZWlnaHQ6IGNhbGMoMS41ZW0gKyAuNzVyZW0gKyA0cHgpO1xufVxuXG4uYnV0dG9uQXJlYSBidXR0b24ge1xuICB3aWR0aDogMTAwJTtcbiAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xuICBvdXRsaW5lOiAwO1xufVxuXG4uYnV0dG9uQXJlYSBzdmcge1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG5idXR0b24ubmF2YmFyLXRvZ2dsZXIge1xuICBib3JkZXItcmFkaXVzOiAxMDBweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDU1cHggIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgYm9yZGVyOiAjOTJmMzI4IHNvbGlkIDBweDtcbiAgb3V0bGluZTogbm9uZTtcbiAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xuICBvdXRsaW5lOiAwO1xufVxuXG4uYm94Q29udGV1ZG8gLmJ0biB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4ubWFwYSBpZnJhbWUge1xuICBib3JkZXI6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogMHB4O1xuICB6LWluZGV4OiAtMTtcbn1cblxuLnNhdWRhY2FvIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uaG9tZVBhc3NhZ2Vpcm8gLmJveENvbnRldWRvIHtcbiAgcG9zaXRpb246IGFic29sdXRlICFpbXBvcnRhbnQ7XG4gIHRvcDogMzBweDtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDBweCk7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBwYWRkaW5nOiAxNXB4IDVweDtcbn1cblxuLmhvbWVQYXNzYWdlaXJvIC5idXR0b25BcmVhIGJ1dHRvbi5yb3VuZGVkLXBpbGwge1xuICBmb250LXNpemU6IDE1cHg7XG59XG5cbmJ1dHRvbi5lc2NvbGhlckRlc3Rpbm8ge1xuICBiYWNrZ3JvdW5kOiAjMDAxYzRlO1xuICBib3JkZXItY29sb3I6ICMwMDFjNGU7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSA2MHB4KSAhaW1wb3J0YW50O1xuICBmbG9hdDogbGVmdDtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xuICB3aWR0aDogNTBweDtcbiAgaGVpZ2h0OiA1MHB4O1xuICBjb2xvcjogI2ZmZjtcbiAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xuICBvdXRsaW5lOiAwO1xufVxuXG5idXR0b24ucGVnYXJDYXJybyB7XG4gIGJhY2tncm91bmQ6ICMwMDFjNGU7XG4gIGJvcmRlci1jb2xvcjogIzAwMWM0ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xuICBoZWlnaHQ6IDUwcHg7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXNpemU6IDE4cHg7XG4gIC0tYm94LXNoYWRvdzogMCAwIDAgMDtcbiAgLS1vdXRsaW5lOiAwO1xufVxuXG5idXR0b24uZW1iYXJxdWVSYXBpZG8ge1xuICBmbG9hdDogcmlnaHQ7XG4gIHBhZGRpbmc6IDBweCAxMnB4O1xuICB3aWR0aDogNDVweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDQ1cHg7XG4gIC0tYm94LXNoYWRvdzogMCAwIDAgMDtcbiAgb3V0bGluZTogMDtcbn1cblxuYnV0dG9uLmVtYmFycXVlUmFwaWRvIHN2ZyB7XG4gIHdpZHRoOiAxOXB4O1xuICBjb2xvcjogIzQxNjMxMztcbiAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xuICBvdXRsaW5lOiAwO1xufVxuXG5idXR0b24uYnRuLmJ0bi1wcmltYXJ5LmJ0bi1sZy5yb3VuZGVkLXBpbGwuZW1iYXJxdWVSYXBpZG8ge1xuICBiYWNrZ3JvdW5kOiAjYjFmZjQ5O1xuICBib3JkZXI6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gIC0tYm94LXNoYWRvdzogMCAwIDAgMDtcbiAgb3V0bGluZTogMDtcbn1cblxuLmZvcm1EZXN0aW5vIHtcbiAgYm9yZGVyOiBub25lO1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIC0tYm94LXNoYWRvdzogMCAwIDAgMDtcbiAgb3V0bGluZTogMDtcbn1cblxuLmVuZERlc3Rpbm9SZWNlbnRlIHtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBiYWNrZ3JvdW5kOiAjZWVlO1xuICBwYWRkaW5nOiAxNHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIC0tYm94LXNoYWRvdzogMCAwIDAgMDtcbiAgb3V0bGluZTogMDtcbn1cblxuLmVuZERlc3Rpbm9SZWNlbnRlIGIge1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBjb2xvcjogZGFya2dyYXk7XG59XG5cbi50aXR1bG9EZXN0aW5vUmVjZW50ZSB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4ubmF2YmFyLWNvbGxhcHNlIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB0b3A6IDBweDtcbiAgbGVmdDogMDtcbiAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgd2lkdGg6IDc1JTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBiYWNrZ3JvdW5kOiAjMDAxYzRlO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICB6LWluZGV4OiA5OTk5O1xuICBib3gtc2hhZG93OiAwcHggMHB4IDkwcHggYmxhY2s7XG59XG5cbi5uYXZiYXItY29sbGFwc2UgYS5uYXYtbGluayB7XG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxOXB4O1xufVxuXG4ubmF2YmFyLWRhcmsgLm5hdmJhci10b2dnbGVyIHtcbiAgYmFja2dyb3VuZDogIzAwMWM0ZTtcbn1cblxuLm5hdmJhci1kYXJrIHNwYW4ubmF2YmFyLXRvZ2dsZXItaWNvbiB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG5cbi5uYXZiYXItY29sbGFwc2UuY29sbGFwc2luZyB7XG4gIGxlZnQ6IC03NSU7XG4gIHRyYW5zaXRpb246IGhlaWdodCAwcyBlYXNlO1xufVxuXG4ubmF2YmFyLWNvbGxhcHNlLnNob3cge1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIGxlZnQ6IDA7XG4gIHRyYW5zaXRpb246IGxlZnQgMzAwbXMgZWFzZS1pbi1vdXQ7XG59XG5cbi5uYXZiYXItdG9nZ2xlci5jb2xsYXBzZWQgfiAubmF2YmFyLWNvbGxhcHNlIHtcbiAgdHJhbnNpdGlvbjogbGVmdCA1MDBtcyBlYXNlLWluLW91dDtcbn1cblxuLm5hdmJhci1saWdodCAubmF2YmFyLXRvZ2dsZXItaWNvbiB7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xufVxuXG4uYW5pbWFjYW8ge1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbiAgbWFyZ2luLXRvcDogNTAlO1xuICB3aWR0aDogNTBweDtcbiAgaGVpZ2h0OiA1MHB4O1xuICBib3JkZXI6IDdweCBzb2xpZCAjYTlhOWE5O1xuICBib3JkZXItdG9wLWNvbG9yOiAjMDAxYzRlO1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICBhbmltYXRpb246IGlzLXJvdGF0aW5nIDEuNXMgaW5maW5pdGU7XG59XG5cbi5wb3NpdGlvbiB7XG4gIHdpZHRoOiBmaXQtY29udGVudDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1cHg7XG4gIGxlZnQ6IDEwcHg7XG59XG5cbi5uYXZiYXItY29sbGFwc2Uge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHRvcDogMHB4O1xuICBsZWZ0OiAwO1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE1cHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICB3aWR0aDogNzUlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQ6ICMwMDFjNGU7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIHotaW5kZXg6IDk5OTk7XG4gIGJveC1zaGFkb3c6IDBweCAwcHggOTBweCBibGFjaztcbn1cblxuLm5hdmJhci1jb2xsYXBzZSBhLm5hdi1saW5rIHtcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDE5cHg7XG59XG5cbmJ1dHRvbi5lc2NvbGhlckRlc3Rpbm8ge1xuICBiYWNrZ3JvdW5kOiAjMDAxYzRlO1xuICBib3JkZXItY29sb3I6ICMwMDFjNGU7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSA2MHB4KSAhaW1wb3J0YW50O1xuICBmbG9hdDogbGVmdDtcbiAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xuICBvdXRsaW5lOiAwO1xufVxuXG4uZm9ybURlc3Rpbm8ge1xuICBib3JkZXI6IG5vbmU7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cblxuLmVuZERlc3Rpbm9SZWNlbnRlIHtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBiYWNrZ3JvdW5kOiAjZWVlO1xuICBwYWRkaW5nOiAxNHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbi5lbmREZXN0aW5vUmVjZW50ZSBiIHtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgY29sb3I6IGRhcmtncmF5O1xufVxuXG4udGl0dWxvRGVzdGlub1JlY2VudGUge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuYnV0dG9uLmJ0bi5idG4tcHJpbWFyeS5idG4tbGcucm91bmRlZC1waWxsLmVzY29saGVyRGVzdGlubyB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xuICBvdXRsaW5lOiAwO1xufVxuXG4ubmF2YmFyLWRhcmsgLm5hdmJhci10b2dnbGVyIHtcbiAgYmFja2dyb3VuZDogIzAwMWM0ZTtcbn1cblxuLm5hdmJhci1kYXJrIHNwYW4ubmF2YmFyLXRvZ2dsZXItaWNvbiB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG5cbi5uYXZiYXItY29sbGFwc2UuY29sbGFwc2luZyB7XG4gIGxlZnQ6IC03NSU7XG4gIHRyYW5zaXRpb246IGhlaWdodCAwcyBlYXNlO1xufVxuXG4ubmF2YmFyLWNvbGxhcHNlLnNob3cge1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIGxlZnQ6IDA7XG4gIHRyYW5zaXRpb246IGxlZnQgMzAwbXMgZWFzZS1pbi1vdXQ7XG59XG5cbi5uYXZiYXItdG9nZ2xlci5jb2xsYXBzZWQgfiAubmF2YmFyLWNvbGxhcHNlIHtcbiAgdHJhbnNpdGlvbjogbGVmdCA1MDBtcyBlYXNlLWluLW91dDtcbn1cblxuLmVsc2Uge1xuICBmb250LXNpemU6IDEuMnJlbTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBhZGRpbmctdG9wOiAyMnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIG1hcmdpbjogMjBweCAwO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICMxMjNiN2Q7XG59XG5cbi5lbHNldWFpTGV2YSB7XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xuICBkaXNwbGF5OiBibG9jaztcbiAgcGFkZGluZy10b3A6IDJweDtcbiAgcGFkZGluZy1sZWZ0OiAxMDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgbWFyZ2luOiAxNXB4IDA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzEyM2I3ZDtcbn1cblxuc3BhbiB7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBwYWRkaW5nLXRvcDogM3B4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICMxMjNiN2Q7XG59XG5cbmgxIHtcbiAgZm9udC1zaXplOiAxLjVyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgbWFyZ2luOiAxMHB4IDA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzEyM2I3ZDtcbn1cblxuLm5hdmJhci5iZy1kYXJrIHtcbiAgYmFja2dyb3VuZDogIzAwMWM0ZSAhaW1wb3J0YW50O1xufVxuXG5pb24tbWVudS1idXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIHdpZHRoOiA1MHB4O1xuICBoZWlnaHQ6IDUwcHg7XG59XG5cbi5uYXZiYXItYnJhbmQge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBhZGRpbmctdG9wOiAwLjMxMjVyZW07XG4gIHBhZGRpbmctYm90dG9tOiAwLjMxMjVyZW07XG4gIG1hcmdpbi1yaWdodDogMXJlbTtcbiAgZm9udC1zaXplOiAxLjI1cmVtO1xuICBsaW5lLWhlaWdodDogaW5oZXJpdDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbn1cblxuLm5hdmJhciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIHBhZGRpbmc6IDAuNXJlbSAxcmVtO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/corrida/corrida.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/corrida/corrida.page.ts ***!
  \***********************************************/
/*! exports provided: CorridaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CorridaPage", function() { return CorridaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/user/user.service */ "./src/app/core/user/user.service.ts");
/* harmony import */ var _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/call-number/ngx */ "./node_modules/@ionic-native/call-number/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var src_app_services_viagem_viagemService__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/viagem/viagemService */ "./src/app/services/viagem/viagemService.ts");
/* harmony import */ var rx_polling__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rx-polling */ "./node_modules/rx-polling/lib/index.js");
/* harmony import */ var rx_polling__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(rx_polling__WEBPACK_IMPORTED_MODULE_8__);










let CorridaPage = class CorridaPage {
    constructor(platform, loadCtrl, ngZone, navCtrl, menuCtrl, popoverCtrl, alertCtrl, modalCtrl, toastCtrl, userService, loadingController, router, viagemService, route, callNumber) {
        this.platform = platform;
        this.loadCtrl = loadCtrl;
        this.ngZone = ngZone;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.userService = userService;
        this.loadingController = loadingController;
        this.router = router;
        this.viagemService = viagemService;
        this.route = route;
        this.callNumber = callNumber;
        this.search = '';
        this.searchOrigem = '';
        this.googleAutocomplete = new google.maps.places.AutocompleteService();
        this.searchResults = new Array();
        this.searchResultsOrigem = new Array();
        this.googleDirectionsService = new google.maps.DirectionsService();
        this.matrix = new google.maps.DistanceMatrixService();
        this.valor = new Array();
        this.valores = [];
        this.user$ = userService.getUser();
        this.user$.subscribe(usuario => {
            this.usuarioLogado = usuario;
            this.user_id = this.usuarioLogado.id;
        });
        this.route.params.subscribe(parametros => {
            this.id_viagem = parametros['id_viagem'];
            this.viagemService.motoristaPassageiro(this.id_viagem).subscribe(viagem => {
                this.viagem = viagem;
                this.calcRoute(viagem.origem);
            });
            const request$ = this.viagemService.viagem(this.id_viagem);
            const options = { interval: 10000 };
            rx_polling__WEBPACK_IMPORTED_MODULE_8___default()(request$, options)
                .subscribe((v) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                //  
                if (v.status == 'CANCELADO') {
                    this.viagemService.cancelaPassageiro(this.id_viagem).subscribe();
                    this.router.navigate(['home']);
                    const toast = yield (yield this.toastCtrl.create({
                        message: 'VIAGEM CANCELADA PELO PASSAGEIRO!',
                        duration: 15000, position: 'middle',
                        color: 'danger'
                    })).present();
                }
            }), (error) => {
                console.error(error);
            });
        });
    }
    ionViewWillEnter() {
        this.menuCtrl.enable(true);
    }
    ngOnInit() {
        this.mapElement = this.mapElement.nativeElement;
        this.mapElement.style.width = this.platform.width() + 'px';
        this.mapElement.style.height = this.platform.height() + 'px';
        this.loadMap();
    }
    loadMap() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loading = yield this.loadCtrl.create({ message: 'Por favor aguarde ...' });
            yield this.loading.present();
            _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["Environment"].setEnv({
                'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyC0inbEk4sOpHIT8lUyG3PI4XziAvl3CI0',
                'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyC0inbEk4sOpHIT8lUyG3PI4XziAvl3CI0'
            });
            const mapOptions = {
                controls: {
                    zoom: false
                }
            };
            this.map = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMaps"].create(this.mapElement, mapOptions);
            try {
                yield this.map.one(_ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsEvent"].MAP_READY);
                this.addOrigenMarker();
            }
            catch (error) {
                console.error(error);
            }
        });
    }
    addOrigenMarker() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                const myLocation = yield this.map.getMyLocation();
                console.log('origem' + myLocation.latLng);
                yield this.map.moveCamera({
                    target: myLocation.latLng,
                    zoom: 18
                });
                this.originMarker = this.map.addMarkerSync({
                    title: 'Origem',
                    icon: '#000',
                    animation: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsAnimation"].DROP,
                    position: myLocation.latLng
                });
            }
            catch (error) {
                console.error(error);
            }
            finally {
                this.loading.dismiss();
            }
        });
    }
    calcRouteOrigem() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const info = yield _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["Geocoder"].geocode({ address: this.origem.description });
            this.originMarker = this.map.addMarkerSync({
                title: this.origem.description,
                icon: '#000',
                animation: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsAnimation"].DROP,
                position: info[0].position
            });
        });
    }
    calcRoute(origem) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.search = '';
            this.destination = origem;
            const info = yield _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["Geocoder"].geocode({ address: this.destination });
            let markerDestination = this.map.addMarkerSync({
                title: this.destination,
                icon: '#000',
                animation: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsAnimation"].DROP,
                position: info[0].position
            });
            this.markerDestination = this.map.addMarkerSync({
                title: this.destination,
                icon: '#000',
                animation: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsAnimation"].DROP,
                position: info[0].position
            });
            this.googleDirectionsService.route({
                origin: this.originMarker.getPosition(),
                destination: markerDestination.getPosition(),
                travelMode: 'DRIVING'
            }, (results) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const points = new Array();
                const routes = results.routes[0].overview_path;
                for (let i = 0; i < routes.length; i++) {
                    points[i] = {
                        lat: routes[i].lat(),
                        lng: routes[i].lng()
                    };
                }
                yield this.map.addPolyline({
                    points: points,
                    color: 'red',
                    width: 3
                });
                var origin1 = new google.maps.LatLng(this.originMarker.getPosition().lat, this.originMarker.getPosition().lng);
                var destinationB = new google.maps.LatLng(markerDestination.getPosition().lat, markerDestination.getPosition().lng);
                console.log('origem moto' + origin1);
                console.log('origem pass' + destinationB);
                this.map.moveCamera({ target: points });
                this.map.panBy(0, 100);
            }));
        });
    }
    inciarCorrida() {
        this.viagemService.iniciarCorrida(this.id_viagem).subscribe(viagem => {
            console.log('iniciar corrida: ' + viagem);
        });
        this.router.navigate(['/corrida_final/' + this.id_viagem + '']);
    }
    back() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                yield this.map.clear();
                location.reload();
                this.addOrigenMarker();
            }
            catch (error) {
                console.error(error);
            }
        });
    }
    ligar(n) {
        this.callNumber.callNumber(n, true)
            .then(() => console.log('Launched dialer!'))
            .catch(() => console.log('Error launching dialer'));
    }
    cancelar() {
        this.viagemService.cancelaMotorista(this.id_viagem).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.router.navigate(['home']);
            const toast = yield (yield this.toastCtrl.create({
                message: 'Viagem Cancelada',
                duration: 4000, position: 'middle',
                color: 'success'
            })).present();
        }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(err);
            location.reload();
            const toast = yield (yield this.toastCtrl.create({
                message: 'Erro ao cancelar Viagem',
                duration: 4000, position: 'middle',
                color: 'danger'
            })).present();
        }));
    }
    cheguei() {
        this.viagemService.cheguei(this.id_viagem).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield (yield this.toastCtrl.create({
                message: 'Mensagem Cheguei enviada!',
                duration: 4000, position: 'middle',
                color: 'success'
            })).present();
        }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield (yield this.toastCtrl.create({
                message: 'Erro ao cancelar Viagem',
                duration: 4000, position: 'middle',
                color: 'danger'
            })).present();
        }));
    }
    navegar() {
        window.open('https://www.waze.com/ul?ll=' + this.markerDestination.getPosition().lat + '%2C' + this.markerDestination.getPosition().lng + '&navigate=yes&zoom=17', '_system');
    }
};
CorridaPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_services_viagem_viagemService__WEBPACK_IMPORTED_MODULE_7__["ViagemService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_6__["CallNumber"] }
];
CorridaPage.propDecorators = {
    mapElement: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ['map', { static: true },] }]
};
CorridaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-corrida',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./corrida.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/corrida/corrida.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./corrida.page.scss */ "./src/app/pages/corrida/corrida.page.scss")).default]
    })
], CorridaPage);



/***/ })

}]);
//# sourceMappingURL=pages-corrida-corrida-routing-module-es2015.js.map