(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-acordos-acordos-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/acordos/acordos.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/acordos/acordos.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button text=\"\" color=\"light\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <h1>Consentimento legal</h1>\r\n\r\n  <div>\r\n    <p class=\"declaracao\">Declaração de checagem de antecedentes</p>\r\n    <p class=\"termo\">\r\n     Ao clicar na caixa de seleção abaixo e enviar o formulário de registro,\r\n    eu concordo com a coleta e o processamento, por parte do UaiLeva e seus prestadores\r\n    de serviços, dos meus dados pessoais, incluindo dados sensíveis, com o objetivo de\r\n    avaliar a minha elegibilidade ao uso ou continuidade de uso, da plataforma, bem como \r\n    para as finalidades descritas na Declaração de Privacidade do Motorista Parceiro do UaiLeva.\r\n    Declaro a veracidade e a precisão das informações e dos documentos enviados ao UaiLeva para\r\n    fins de registro.</p>\r\n  </div>\r\n\r\n  <ion-item>\r\n    <ion-checkbox item-start (ionChange)=\"enableBtn($event)\"></ion-checkbox>&nbsp;\r\n    <ion-label>Estou ciente e aceito</ion-label>\r\n  </ion-item><br>\r\n\r\n\r\n  <ion-button [disabled]=\"checkedButton\" (click)=\"acordo();\" class=\"botao\">Aceito</ion-button>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/acordos/acordos-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/acordos/acordos-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: AcordosPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcordosPageRoutingModule", function() { return AcordosPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _acordos_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./acordos.page */ "./src/app/pages/acordos/acordos.page.ts");




const routes = [
    {
        path: '',
        component: _acordos_page__WEBPACK_IMPORTED_MODULE_3__["AcordosPage"]
    }
];
let AcordosPageRoutingModule = class AcordosPageRoutingModule {
};
AcordosPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AcordosPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/acordos/acordos.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/acordos/acordos.module.ts ***!
  \*************************************************/
/*! exports provided: AcordosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcordosPageModule", function() { return AcordosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _acordos_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./acordos-routing.module */ "./src/app/pages/acordos/acordos-routing.module.ts");
/* harmony import */ var _acordos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./acordos.page */ "./src/app/pages/acordos/acordos.page.ts");







let AcordosPageModule = class AcordosPageModule {
};
AcordosPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _acordos_routing_module__WEBPACK_IMPORTED_MODULE_5__["AcordosPageRoutingModule"]
        ],
        declarations: [_acordos_page__WEBPACK_IMPORTED_MODULE_6__["AcordosPage"]]
    })
], AcordosPageModule);



/***/ }),

/***/ "./src/app/pages/acordos/acordos.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/acordos/acordos.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: var(--ion-color-light);\n  --background: url(\"/assets/img/bg-uai-leva2.png\") #eaeaea no-repeat bottom 75px left 45px;\n}\n\nion-toolbar {\n  --background: #001c4e !important;\n}\n\nion-header {\n  --background: #001c4e !important;\n}\n\nion-item {\n  --ion-item-background: transparent;\n  --border-style: var(--border-style);\n  --border: 0 none;\n  --box-shadow: 0 0 0 0;\n  --outline: 0;\n}\n\nion-icon {\n  color: gray;\n}\n\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  margin: 15px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n\n.declaracao {\n  font-size: 1.1rem;\n  padding: 10px;\n  color: #123b7d;\n}\n\n.termo {\n  padding: 10px;\n  padding-top: 0px;\n  color: black;\n  margin-top: -20px;\n  text-align: justify;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWNvcmRvcy9hY29yZG9zLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG9DQUFBO0VBQ0EseUZBQUE7QUFDSjs7QUFFQTtFQUNJLGdDQUFBO0FBQ0o7O0FBRUE7RUFDSSxnQ0FBQTtBQUNKOztBQUVBO0VBQ0ksa0NBQUE7RUFDQSxtQ0FBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBRUEsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQUFKOztBQUdBO0VBQ0ksaUJBQUE7RUFFQSxhQUFBO0VBRUEsY0FBQTtBQUZKOztBQUtBO0VBQ0ksYUFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUFGSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Fjb3Jkb3MvYWNvcmRvcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcbiAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1nL2JnLXVhaS1sZXZhMi5wbmdcIikgI2VhZWFlYSBuby1yZXBlYXQgYm90dG9tIDc1cHggbGVmdCA0NXB4O1xyXG59XHJcblxyXG5pb24tdG9vbGJhciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGUgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWhlYWRlciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGUgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWl0ZW0ge1xyXG4gICAgLS1pb24taXRlbS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIC0tYm9yZGVyLXN0eWxlOiB2YXIoLS1ib3JkZXItc3R5bGUpO1xyXG4gICAgLS1ib3JkZXI6IDAgbm9uZTsgXHJcbiAgICAtLWJveC1zaGFkb3c6IDAgMCAwIDA7IFxyXG4gICAgLS1vdXRsaW5lOiAwO1xyXG59XHJcblxyXG5pb24taWNvbiB7XHJcbiAgICBjb2xvcjogZ3JheTtcclxufVxyXG5cclxuaDEge1xyXG4gICAgZm9udC1zaXplOiAxLjVyZW07XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAvL3RleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBtYXJnaW46IDE1cHggMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbn1cclxuXHJcbi5kZWNsYXJhY2FvIHtcclxuICAgIGZvbnQtc2l6ZTogMS4xcmVtO1xyXG4gICAgLy9mb250LXdlaWdodDogYm9sZDtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAvL3BhZGRpbmctYm90dG9tOiAwcHg7XHJcbiAgICBjb2xvcjogIzEyM2I3ZDtcclxufVxyXG5cclxuLnRlcm1vIHtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgbWFyZ2luLXRvcDogLTIwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/pages/acordos/acordos.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/acordos/acordos.page.ts ***!
  \***********************************************/
/*! exports provided: AcordosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcordosPage", function() { return AcordosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_services_user_registerService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/user/registerService */ "./src/app/services/user/registerService.ts");





let AcordosPage = class AcordosPage {
    constructor(route, signupService, toastCtrl, router) {
        this.route = route;
        this.signupService = signupService;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.checkedButton = true;
    }
    ngOnInit() {
        this.route.params.subscribe(parametros => {
            this.phone = parametros['phone'];
        });
    }
    enableBtn(event) {
        if (event.checked) {
            this.checkedButton = !this.checkedButton;
        }
        else {
            this.checkedButton = !this.checkedButton;
        }
    }
    acordo() {
        this.signupService.acordo(this.phone).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield (yield this.toastCtrl.create({
                message: 'Usuário cadastrado com sucesso! Agora é só fazer o login!',
                duration: 4000, position: 'top',
                color: 'success'
            })).present();
            this.router.navigate(['/foto-perfil/' + this.phone + '']);
        }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(err);
            const toast = yield (yield this.toastCtrl.create({
                message: 'Por favor confira se a sua internet está funcionando e tente cadastrar novamente! UaiLevacopia agradece!',
                duration: 4000, position: 'top',
                color: 'danger'
            })).present();
        }));
    }
};
AcordosPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_app_services_user_registerService__WEBPACK_IMPORTED_MODULE_4__["RegisterService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
AcordosPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-acordos',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./acordos.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/acordos/acordos.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./acordos.page.scss */ "./src/app/pages/acordos/acordos.page.scss")).default]
    })
], AcordosPage);



/***/ })

}]);
//# sourceMappingURL=pages-acordos-acordos-module-es2015.js.map