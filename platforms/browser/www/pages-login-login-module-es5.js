(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesLoginLoginPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content padding >\r\n  <div class=\"logoLogin\">\r\n    <img src=\"/assets/img/logo-branco.png\" alt=\"Uai Leva\">\r\n  </div>\r\n\r\n  <h1>Motorista</h1>\r\n  <form  [formGroup]=\"onLoginForm\" style=\"padding-left: 10px; padding-right: 10px; margin-top: 40%;\"> <!---->\r\n    <ion-item> \r\n      <ion-input type=\"tel\"  name=\"telefone\" formControlName=\"userName\" placeholder=\"Telefone\" [brmasker]=\"{phone: true}\"><!---->\r\n        <ion-icon name=\"phone-portrait-outline\" size=\"small\" slot=\"start\"></ion-icon>&nbsp;\r\n      </ion-input>\r\n      <p ion-text class=\"text08\" *ngIf=\"onLoginForm.get('userName').touched && onLoginForm.get('userName').hasError('required')\">\r\n        <ion-text color=\"danger\">Campo obrigatório!</ion-text>\r\n      </p>\r\n    </ion-item>\r\n    <p></p>\r\n    <ion-item>\r\n      <ion-input [type]=\"tipo ? 'text' : 'password'\"  formControlName=\"password\" name=\"senha\" placeholder=\"Senha\"><!---->\r\n        <ion-icon name=\"lock-closed-outline\" size=\"small\" slot=\"start\"></ion-icon>&nbsp;\r\n      </ion-input>\r\n      <ion-icon [name]=\"tipo ? 'eye-outline' : 'eye-off-outline'\" slot=\"end\" size=\"small\" (click)=\"mostrarSenha()\" style=\"padding-top: 5px;\"></ion-icon>\r\n      <p ion-text class=\"text08\" *ngIf=\"onLoginForm.get('password').touched && onLoginForm.get('password').hasError('required')\">\r\n        <ion-text color=\"danger\">Campo obrigatório!</ion-text>\r\n      </p>\r\n\r\n    </ion-item>\r\n  </form>\r\n\r\n  <p></p>\r\n\r\n  <ion-button (click)=\"goToHome()\" class=\"botao\">\r\n    <ion-icon name=\"enter-outline\" slot=\"end\" style=\"color: #001c4e;\"></ion-icon>Entrar\r\n  </ion-button>\r\n  <p></p>\r\n  <a style=\"text-align: right; font-size: 0.9em; color: white; display: block; padding-right: 20px;\" (click)=\"forgotPass()\" >Esqueceu a senha?</a>\r\n  <br><br>\r\n  <ion-button (click)=\"cadastro()\" class=\"cadastrar\"><ion-icon name=\"caret-forward\" size=\"small\" slot=\"end\" style=\"color: #b1ff49;\"></ion-icon>&nbsp;Quero me cadastrar</ion-button>\r\n\r\n\r\n\r\n  <!--<ion-grid>\r\n            <ion-button shape=\"round\" expand=\"full\" color=\"tertiary\" (click)=\"loginFb()\">\r\n                <ion-icon slot=\"start\" name=\"logo-facebook\" style=\"color: #1b2c7b;\" [src]=\"user.img\" alt=\"\"></ion-icon>&nbsp;\r\n                <h3>LOGIN COM FACEBOOK</h3>\r\n              </ion-button><p></p>\r\n              <ion-button shape=\"round\" expand=\"full\" color=\"tertiary\">\r\n                <ion-icon slot=\"start\" name=\"logo-google\" style=\"color: #1b2c7b;\"></ion-icon>&nbsp;\r\n                <h3>LOGIN COM GMAIL</h3>\r\n              </ion-button>\r\n        </ion-grid>-->\r\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/core/auth/auth.service.ts":
    /*!*******************************************!*\
      !*** ./src/app/core/auth/auth.service.ts ***!
      \*******************************************/

    /*! exports provided: AuthService */

    /***/
    function srcAppCoreAuthAuthServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthService", function () {
        return AuthService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../user/user.service */
      "./src/app/core/user/user.service.ts");

      var API_URL = 'https://uaileva.com.br/api'; //const API_URL = "http://191.252.3.12:3000"; 

      var AuthService = /*#__PURE__*/function () {
        function AuthService(http, userService) {
          _classCallCheck(this, AuthService);

          this.http = http;
          this.userService = userService;
        }

        _createClass(AuthService, [{
          key: "authenticate",
          value: function authenticate(userName, password) {
            var _this = this;

            console.log("".concat(API_URL, "/motorista/login"));
            return this.http.post("".concat(API_URL, "/motorista/login"), {
              userName: userName,
              password: password
            }, {
              observe: 'response'
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (res) {
              var authToken = res.headers.get('x-access-token');

              _this.userService.setToken(authToken);

              console.log("User ".concat(userName, " authenticated with token ").concat(authToken));
            }));
          }
        }, {
          key: "findAllUsers",
          value: function findAllUsers() {
            return this.http.get("".concat(API_URL, "/motorista/findAllUsers"), {
              observe: 'response'
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (res) {
              console.log("All users" + res);
            }));
          }
        }]);

        return AuthService;
      }();

      AuthService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }, {
          type: _user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]
        }];
      };

      AuthService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], AuthService);
      /***/
    },

    /***/
    "./src/app/core/plataform-dector/plataform-dector.service.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/core/plataform-dector/plataform-dector.service.ts ***!
      \*******************************************************************/

    /*! exports provided: PlatformDectorService */

    /***/
    function srcAppCorePlataformDectorPlataformDectorServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PlatformDectorService", function () {
        return PlatformDectorService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

      var PlatformDectorService = /*#__PURE__*/function () {
        function PlatformDectorService(platformId) {
          _classCallCheck(this, PlatformDectorService);

          this.platformId = platformId;
        }

        _createClass(PlatformDectorService, [{
          key: "isPlataformBrowser",
          value: function isPlataformBrowser() {
            return Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.platformId);
          }
        }]);

        return PlatformDectorService;
      }();

      PlatformDectorService.ctorParameters = function () {
        return [{
          type: String,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"],
            args: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["PLATFORM_ID"]]
          }]
        }];
      };

      PlatformDectorService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], PlatformDectorService);
      /***/
    },

    /***/
    "./src/app/pages/login/login-routing.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/pages/login/login-routing.module.ts ***!
      \*****************************************************/

    /*! exports provided: LoginPageRoutingModule */

    /***/
    function srcAppPagesLoginLoginRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function () {
        return LoginPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/pages/login/login.page.ts");

      var routes = [{
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
      }];

      var LoginPageRoutingModule = function LoginPageRoutingModule() {
        _classCallCheck(this, LoginPageRoutingModule);
      };

      LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/login/login.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/pages/login/login.module.ts ***!
      \*********************************************/

    /*! exports provided: LoginPageModule */

    /***/
    function srcAppPagesLoginLoginModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
        return LoginPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-routing.module */
      "./src/app/pages/login/login-routing.module.ts");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/pages/login/login.page.ts");
      /* harmony import */


      var br_mask__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! br-mask */
      "./node_modules/br-mask/__ivy_ngcc__/dist/index.js");

      var LoginPageModule = function LoginPageModule() {
        _classCallCheck(this, LoginPageModule);
      };

      LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], br_mask__WEBPACK_IMPORTED_MODULE_7__["BrMaskerModule"]],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
      })], LoginPageModule);
      /***/
    },

    /***/
    "./src/app/pages/login/login.page.scss":
    /*!*********************************************!*\
      !*** ./src/app/pages/login/login.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesLoginLoginPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --background: var(--ion-color-light);\n  --background: url(\"/assets/img/bg-login.jpg\") #eaeaea no-repeat;\n}\n\nion-toolbar {\n  --background: #001c4e !important;\n}\n\nion-header {\n  --background: #001c4e !important;\n}\n\nion-item {\n  border: 1px solid lightgray;\n  border-radius: 50px;\n  --highlight-height: 0px;\n  /*--highlight-color-focused: var(--ion-color-primary, #001c4e);\n  --highlight-color-valid: var(--ion-color-success, #2dd36f);\n  --highlight-color-invalid: var(--ion-color-danger, #eb445a);*/\n}\n\nion-icon {\n  color: gray;\n}\n\n.logoLogin img {\n  margin: 30px auto;\n  display: block;\n  width: 30%;\n}\n\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 15px 0;\n  font-weight: bold;\n  color: white;\n}\n\n.botaoCadastro {\n  --background: #b1ff49;\n  --border: none;\n  font-weight: bold;\n  display: block;\n  height: 48px;\n  width: 95%;\n  margin: auto;\n  --border-radius: 50px;\n  --background-activated: #001c4e;\n  --color-activated: white;\n  --background-hover: #001c4e;\n  --color-hover: white;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n\n.cadastrar {\n  --background: none;\n  --color: #b1ff49;\n  font-weight: bold;\n  display: block;\n  height: 48px;\n  width: 95%;\n  margin: auto;\n  --border: none;\n  --background-activated: none;\n  --color-activated:#b1ff49;\n  --background-hover: none;\n  --color-hover: #b1ff49;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksb0NBQUE7RUFDQSwrREFBQTtBQUNKOztBQUVBO0VBQ0ksZ0NBQUE7QUFDSjs7QUFFQTtFQUNJLGdDQUFBO0FBQ0o7O0FBRUE7RUFDSSwyQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQTs7K0RBQUE7QUFHSjs7QUFFQTtFQUNJLFdBQUE7QUFDSjs7QUFFQTtFQUNJLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLFVBQUE7QUFDSjs7QUFFQTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLHFCQUFBO0VBQ0EsY0FBQTtFQUVBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0VBQ0EsK0JBQUE7RUFDQSx3QkFBQTtFQUNBLDJCQUFBO0VBQ0Esb0JBQUE7RUFDQSxxQkFBQTtFQUNBLFVBQUE7QUFBSjs7QUFHQTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsNEJBQUE7RUFDQSx5QkFBQTtFQUNBLHdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQkFBQTtFQUNBLFVBQUE7QUFBSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxuICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWcvYmctbG9naW4uanBnXCIpICNlYWVhZWEgbm8tcmVwZWF0O1xyXG59XHJcblxyXG5pb24tdG9vbGJhciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGUgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWhlYWRlciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGUgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWl0ZW0ge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgIC0taGlnaGxpZ2h0LWhlaWdodDogMHB4O1xyXG4gICAgLyotLWhpZ2hsaWdodC1jb2xvci1mb2N1c2VkOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSwgIzAwMWM0ZSk7XHJcbiAgICAtLWhpZ2hsaWdodC1jb2xvci12YWxpZDogdmFyKC0taW9uLWNvbG9yLXN1Y2Nlc3MsICMyZGQzNmYpO1xyXG4gICAgLS1oaWdobGlnaHQtY29sb3ItaW52YWxpZDogdmFyKC0taW9uLWNvbG9yLWRhbmdlciwgI2ViNDQ1YSk7Ki9cclxufVxyXG5cclxuaW9uLWljb24ge1xyXG4gICAgY29sb3I6IGdyYXk7XHJcbn1cclxuXHJcbi5sb2dvTG9naW4gaW1nIHtcclxuICAgIG1hcmdpbjogMzBweCBhdXRvO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB3aWR0aDogMzAlO1xyXG59XHJcblxyXG5oMSB7XHJcbiAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBtYXJnaW46IDE1cHggMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgY29sb3I6IHdoaXRlOyAvLyMxMjNiN2Q7XHJcbn1cclxuXHJcbi5ib3Rhb0NhZGFzdHJvIHtcclxuICAgIC0tYmFja2dyb3VuZDogI2IxZmY0OTsgIC8vIzAwMWM0ZTtcclxuICAgIC0tYm9yZGVyOiBub25lO1xyXG4gICAgLy8tLWNvbG9yOiAjMDAxYzRlOyAvL3doaXRlO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGhlaWdodDogNDhweDtcclxuICAgIHdpZHRoOiA5NSU7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICAtLWJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiAjMDAxYzRlO1xyXG4gICAgLS1jb2xvci1hY3RpdmF0ZWQ6IHdoaXRlO1xyXG4gICAgLS1iYWNrZ3JvdW5kLWhvdmVyOiAjMDAxYzRlO1xyXG4gICAgLS1jb2xvci1ob3Zlcjogd2hpdGU7XHJcbiAgICAtLWJveC1zaGFkb3c6IDAgMCAwIDA7IFxyXG4gICAgb3V0bGluZTogMDtcclxufVxyXG5cclxuLmNhZGFzdHJhciB7XHJcbiAgICAtLWJhY2tncm91bmQ6IG5vbmU7XHJcbiAgICAtLWNvbG9yOiAjYjFmZjQ5OyAvL3doaXRlO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGhlaWdodDogNDhweDtcclxuICAgIHdpZHRoOiA5NSU7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICAtLWJvcmRlcjogbm9uZTtcclxuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6IG5vbmU7XHJcbiAgICAtLWNvbG9yLWFjdGl2YXRlZDojYjFmZjQ5O1xyXG4gICAgLS1iYWNrZ3JvdW5kLWhvdmVyOiBub25lO1xyXG4gICAgLS1jb2xvci1ob3ZlcjogICNiMWZmNDk7XHJcbiAgICAtLWJveC1zaGFkb3c6IDAgMCAwIDA7IFxyXG4gICAgb3V0bGluZTogMDtcclxufVxyXG5cclxuIl19 */";
      /***/
    },

    /***/
    "./src/app/pages/login/login.page.ts":
    /*!*******************************************!*\
      !*** ./src/app/pages/login/login.page.ts ***!
      \*******************************************/

    /*! exports provided: LoginPage */

    /***/
    function srcAppPagesLoginLoginPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
        return LoginPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_app_core_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/core/auth/auth.service */
      "./src/app/core/auth/auth.service.ts");
      /* harmony import */


      var src_app_core_plataform_dector_plataform_dector_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/core/plataform-dector/plataform-dector.service */
      "./src/app/core/plataform-dector/plataform-dector.service.ts");
      /* harmony import */


      var src_app_services_user_recuperar_senha__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/user/recuperar-senha */
      "./src/app/services/user/recuperar-senha.ts");

      var LoginPage = /*#__PURE__*/function () {
        function LoginPage(router, navCtrl, menuCtrl, toastCtrl, alertCtrl, loadingCtrl, formBuilder, authService, platformDetectorService, recuperarSenha) {
          _classCallCheck(this, LoginPage);

          this.router = router;
          this.navCtrl = navCtrl;
          this.menuCtrl = menuCtrl;
          this.toastCtrl = toastCtrl;
          this.alertCtrl = alertCtrl;
          this.loadingCtrl = loadingCtrl;
          this.formBuilder = formBuilder;
          this.authService = authService;
          this.platformDetectorService = platformDetectorService;
          this.recuperarSenha = recuperarSenha;
          this.users = [];
        }

        _createClass(LoginPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.onLoginForm = this.formBuilder.group({
              'userName': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
              'password': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])]
            });
            this.nome = window.localStorage.getItem('nome');
            this.senha = window.localStorage.getItem('senha');
          }
        }, {
          key: "mostrarSenha",
          value: function mostrarSenha() {
            this.tipo = !this.tipo;
          }
        }, {
          key: "goToHome",
          value: function goToHome() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this2 = this;

              var userName, password, toast, a;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      userName = this.onLoginForm.get('userName').value;
                      password = this.onLoginForm.get('password').value;
                      window.localStorage.setItem('nome', this.onLoginForm.get('userName').value);
                      window.localStorage.setItem('senha', this.onLoginForm.get('password').value);

                      if (!(userName == null || password == null)) {
                        _context2.next = 9;
                        break;
                      }

                      _context2.next = 7;
                      return this.toastCtrl.create({
                        message: 'Campos obrigatórios.',
                        duration: 4000,
                        position: 'top',
                        color: 'danger'
                      });

                    case 7:
                      toast = _context2.sent;
                      toast.present();

                    case 9:
                      a = userName.replace('-', '').replace(' ', '').replace('(', '').replace(')', '');
                      this.authService.authenticate(a, password).subscribe(function () {
                        return _this2.navCtrl.navigateRoot('/home');
                      }, function (err) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                          var toast;
                          return regeneratorRuntime.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  console.log('entrou aqui' + err.message);
                                  this.onLoginForm.reset();
                                  this.platformDetectorService.isPlataformBrowser();
                                  _context.next = 5;
                                  return this.toastCtrl.create({
                                    message: 'Se já fez seu cadastro aguarde liberação, caso contrário venha fazer parte do nosso time!',
                                    duration: 4000,
                                    position: 'top',
                                    color: 'danger'
                                  });

                                case 5:
                                  toast = _context.sent;
                                  toast.present();

                                case 7:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee, this);
                        }));
                      });

                    case 11:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "forgotPass",
          value: function forgotPass() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var _this3 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      _context6.next = 2;
                      return this.alertCtrl.create({
                        header: 'Esqueceu sua senha?',
                        message: 'Informe seu e-mail para a recuperação.',
                        inputs: [{
                          name: 'email',
                          type: 'email',
                          placeholder: 'E-mail'
                        }],
                        buttons: [{
                          text: 'Cancelar',
                          role: 'cancel',
                          cssClass: 'secondary',
                          handler: function handler() {
                            console.log('Confirmar Cancelamento?');
                          }
                        }, {
                          text: 'Confirmar',
                          handler: function handler(data) {
                            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                              var _this4 = this;

                              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                                while (1) {
                                  switch (_context5.prev = _context5.next) {
                                    case 0:
                                      console.log(data.email);
                                      this.recuperarSenha.sendemail(data.email).subscribe(function () {
                                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this4, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                                          var toast;
                                          return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                            while (1) {
                                              switch (_context3.prev = _context3.next) {
                                                case 0:
                                                  console.log('entrou aqui');
                                                  this.navCtrl.navigateRoot('');
                                                  _context3.next = 4;
                                                  return this.toastCtrl.create({
                                                    message: 'Email enviado com sucesso!',
                                                    duration: 4000,
                                                    position: 'top',
                                                    color: 'success'
                                                  });

                                                case 4:
                                                  toast = _context3.sent;
                                                  toast.present();

                                                case 6:
                                                case "end":
                                                  return _context3.stop();
                                              }
                                            }
                                          }, _callee3, this);
                                        }));
                                      }, function (err) {
                                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this4, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                                          var toast;
                                          return regeneratorRuntime.wrap(function _callee4$(_context4) {
                                            while (1) {
                                              switch (_context4.prev = _context4.next) {
                                                case 0:
                                                  _context4.next = 2;
                                                  return this.toastCtrl.create({
                                                    message: 'Usuário não cadastrado, por favor cadastre-se no APP!',
                                                    duration: 4000,
                                                    position: 'top',
                                                    color: 'danger'
                                                  });

                                                case 2:
                                                  toast = _context4.sent;
                                                  toast.present();

                                                case 4:
                                                case "end":
                                                  return _context4.stop();
                                              }
                                            }
                                          }, _callee4, this);
                                        }));
                                      });

                                    case 2:
                                    case "end":
                                      return _context5.stop();
                                  }
                                }
                              }, _callee5, this);
                            }));
                          }
                        }]
                      });

                    case 2:
                      alert = _context6.sent;
                      _context6.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }, {
          key: "cadastro",
          value: function cadastro() {
            this.router.navigate(['cadastro']);
          }
        }]);

        return LoginPage;
      }();

      LoginPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
        }, {
          type: src_app_core_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]
        }, {
          type: src_app_core_plataform_dector_plataform_dector_service__WEBPACK_IMPORTED_MODULE_6__["PlatformDectorService"]
        }, {
          type: src_app_services_user_recuperar_senha__WEBPACK_IMPORTED_MODULE_7__["RecuperarSenhaService"]
        }];
      };

      LoginPage.propDecorators = {
        userNameInput: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['userNameInput']
        }]
      };
      LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login.page.scss */
        "./src/app/pages/login/login.page.scss"))["default"]]
      })], LoginPage);
      /***/
    },

    /***/
    "./src/app/services/user/recuperar-senha.ts":
    /*!**************************************************!*\
      !*** ./src/app/services/user/recuperar-senha.ts ***!
      \**************************************************/

    /*! exports provided: RecuperarSenhaService */

    /***/
    function srcAppServicesUserRecuperarSenhaTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RecuperarSenhaService", function () {
        return RecuperarSenhaService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

      var API_URL = "https://uaileva.com.br/api"; //const API_URL = "http://191.252.3.12:3000"; 

      var RecuperarSenhaService = /*#__PURE__*/function () {
        function RecuperarSenhaService(http) {
          _classCallCheck(this, RecuperarSenhaService);

          this.http = http;
        }

        _createClass(RecuperarSenhaService, [{
          key: "sendemail",
          value: function sendemail(email) {
            console.log(email);
            return this.http.post("".concat(API_URL, "/motorista/sendEmail"), {
              email: email
            });
          }
        }]);

        return RecuperarSenhaService;
      }();

      RecuperarSenhaService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      RecuperarSenhaService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], RecuperarSenhaService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-login-login-module-es5.js.map