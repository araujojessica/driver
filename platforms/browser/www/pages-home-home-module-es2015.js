(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (" <ion-header>\r\n  <nav class=\"navbar navbar-expand-lg navbar-light bg-dark\">\r\n      <a class=\"navbar-brand\" href=\"#\">\r\n      <img src=\"/assets/img/logo-branco.png\" style=\"width: 2.5em;\"/>\r\n      </a>\r\n      <ion-buttons slot=\"end\">\r\n        <ion-menu-button color=\"medium\"></ion-menu-button>\r\n      </ion-buttons>\r\n  </nav>\r\n</ion-header>\r\n<ion-content>\r\n  <div #map>\r\n    <div class=\"boxConteudo\" [hidden]=\"destination\">\r\n      <div class=\"saudacao\">\r\n          <ion-text style=\"color: #001c4e;\" *ngIf=\"(user$ | async) as user;\">\r\n              Olá, {{user.full_name}}\r\n          </ion-text> \r\n          <br>\r\n          <ion-text style=\"color: #001c4e;\">\r\n            Que seja um dia abençoado!\r\n        </ion-text> \r\n          <br>\r\n          <br>\r\n          <ion-button (click)=\"aceitarViagem()\">aceitar</ion-button>\r\n      </div>\r\n\r\n\r\n    </div>\r\n\r\n    <div  *ngFor= \"let viagem of viagem\">\r\n      <div class=\"vibra\">\r\n              <br>\r\n              <p><b>Passageiro: {{viagem.user_full_name}}</b></p>\r\n              <p><b>Origem:</b> {{viagem.origem}}</p>\r\n              <p><b>Destino:</b> {{viagem.destino}}</p>\r\n              <p><b>Valor da corrida:</b> R$ {{viagem.valor_corrida}}</p>\r\n              <p><b>Forma de pagamento:</b> {{viagem.forma_pagamento}}</p>\r\n              <ion-button class=\"botao\" (click) = \"aceitar(viagem.id_usuario,viagem.id_viagem);\">ACEITAR CORRIDA</ion-button>\r\n              <br><br>\r\n      </div>\r\n  </div>\r\n      <button (click)=\"comecar();\" class=\"comecar\" id = \"comecar\" style=\"display: block;\">COMEÇAR</button>\r\n      <button (click)=\"encerrar();\" class=\"comecar\" id = \"encerrar\" style=\"display: none;\">ENCERRAR</button>\r\n      \r\n  </div>\r\n  <style>\r\n    body {\r\n    background: #824438;\r\n  }\r\n  \r\n  .vibra {\r\n    margin: 10px auto;\r\n    width: 300px;\r\n    /*animation: vibrador 0.82s cubic-bezier(.36,.07,.19,.97) both;*/\r\n    animation-iteration-count: infinite;\r\n    transform: translate3d(0, 0, 0);\r\n    backface-visibility: hidden;\r\n    perspective: 1000px;\r\n    background:white;\r\n    text-align:center;\r\n    height:300px;\r\n    border-radius: 15px;\r\n\r\n  }\r\n\r\n  p, b {\r\n    color:#001c4e;\r\n  }\r\n  \r\n  @keyframes vibrador {\r\n    10%, 90% {\r\n      transform: translate3d(-1px, 0, 0);\r\n    }\r\n    \r\n    20%, 80% {\r\n      transform: translate3d(2px, 0, 0);\r\n    }\r\n  \r\n    30%, 50%, 70% {\r\n      transform: translate3d(-4px, 0, 0);\r\n    }\r\n  \r\n    40%, 60% {\r\n      transform: translate3d(4px, 0, 0);\r\n    }\r\n  }\r\n  </style>\r\n  <!--<ion-buttons class=\"ion-margin-top\" slot=\"start\" (click)=\"back()\" [hidden]=\"!destination\" style=\"position: fixed;\">\r\n        <ion-button text=\"\" icon=\"close\" style=\"height: 50px; width: 50px; border-radius: 50px; background-color: white;\">\r\n            <ion-icon name=\"arrow-back\" style=\"color: #001c4e; size: 5em;\"></ion-icon>\r\n        </ion-button>\r\n    </ion-buttons>\r\n       \r\n    <br>\r\n\r\n    \r\n    <button type=\"button\" class=\"btn btn-primary btn-lg rounded-pill pegarCarro\" (click)=\"ligar('35998401989');\">\r\n      LIGAR\r\n    </button>\r\n  </div>\r\n\r\n      \r\n  </div>\r\n\r\n\r\n\r\n  -->\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/home/home-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/home/home-routing.module.ts ***!
  \***************************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"],
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], HomePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/pages/home/home-routing.module.ts");







let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_6__["HomePageRoutingModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]],
        entryComponents: []
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/pages/home/home.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@charset \"UTF-8\";\n#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n#container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n#container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #8c8c8c;\n  margin: 0;\n}\n#container a {\n  text-decoration: none;\n}\n#map {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  background: #eee;\n}\n.boxConteudo {\n  background: white;\n  padding: 10px;\n  border-radius: 20px;\n  outline: 0;\n  box-shadow: 0px 0px 10px #e4e4e4;\n  display: block;\n  margin-left: 13px;\n  margin-right: auto;\n  z-index: 9999 !important;\n  /* número máximo é 9999 */\n  width: calc(100% - 25px) !important;\n  margin-top: 15px;\n}\n.pagamentos {\n  background: white;\n  padding: 10px;\n  border-radius: 20px;\n  box-shadow: 0px 0px 10px #e4e4e4;\n  position: fixed !important;\n  z-index: 9999 !important;\n  /* número máximo é 9999 */\n  width: 100%;\n  height: 224px;\n  margin-top: 90%;\n}\nbutton.btn.btn-primary.btn-lg.rounded-pill.escolherDestino {\n  font-size: 15px;\n}\n.boxConteudo label {\n  text-transform: uppercase;\n  font-size: 12px;\n}\n.boxConteudo .form-control {\n  height: calc(1.5em + .75rem + 4px);\n}\n.buttonArea button {\n  width: 100%;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.buttonArea svg {\n  font-size: 25px;\n  margin-right: 5px;\n}\nbutton.navbar-toggler {\n  border-radius: 100px !important;\n  height: 55px !important;\n  background: #ffffff;\n  border: #92f328 solid 0px;\n  outline: none;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.boxConteudo .btn {\n  font-weight: bold;\n}\n.mapa iframe {\n  border: 0;\n  width: 100%;\n  height: 100vh;\n  position: relative;\n  top: 0px;\n  z-index: -1;\n}\n.saudacao {\n  text-align: center;\n}\n.homePassageiro .boxConteudo {\n  position: absolute !important;\n  top: 30px;\n  width: calc(100% - 0px);\n  margin: 0 auto;\n  padding: 15px 5px;\n}\n.homePassageiro .buttonArea button.rounded-pill {\n  font-size: 15px;\n}\nbutton.escolherDestino {\n  background: #001c4e;\n  border-color: #001c4e;\n  width: calc(100% - 60px) !important;\n  float: left;\n  background-color: white;\n  border-radius: 100px;\n  width: 50px;\n  height: 50px;\n  color: #fff;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\nbutton.pegarCarro {\n  background: #001c4e;\n  border-color: #001c4e;\n  width: 100%;\n  border-radius: 100px;\n  height: 50px;\n  color: #fff;\n  font-size: 18px;\n  --box-shadow: 0 0 0 0;\n  --outline: 0;\n}\nbutton.embarqueRapido {\n  float: right;\n  padding: 0px 12px;\n  width: 45px !important;\n  height: 45px;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\nbutton.embarqueRapido svg {\n  width: 19px;\n  color: #416313;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\nbutton.btn.btn-primary.btn-lg.rounded-pill.embarqueRapido {\n  background: #b1ff49;\n  border: none;\n  border-radius: 100%;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.formDestino {\n  border: none;\n  margin-top: 5px;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.endDestinoRecente {\n  font-size: 11px;\n  background: #eee;\n  padding: 14px;\n  border-radius: 5px;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.endDestinoRecente b {\n  font-weight: normal;\n  color: darkgray;\n}\n.tituloDestinoRecente {\n  text-transform: uppercase;\n  font-weight: bold;\n}\n.navbar-collapse {\n  position: fixed;\n  top: 0px;\n  left: 0;\n  padding-left: 15px;\n  padding-right: 15px;\n  padding-bottom: 15px;\n  width: 75%;\n  height: 100%;\n  background: #001c4e;\n  text-transform: uppercase;\n  z-index: 9999;\n  box-shadow: 0px 0px 90px black;\n}\n.navbar-collapse a.nav-link {\n  color: white !important;\n  font-weight: bold;\n  font-size: 19px;\n}\n.navbar-dark .navbar-toggler {\n  background: #001c4e;\n}\n.navbar-dark span.navbar-toggler-icon {\n  padding: 15px;\n}\n.navbar-collapse.collapsing {\n  left: -75%;\n  transition: height 0s ease;\n}\n.navbar-collapse.show {\n  margin-top: 0px;\n  left: 0;\n  transition: left 300ms ease-in-out;\n}\n.navbar-toggler.collapsed ~ .navbar-collapse {\n  transition: left 500ms ease-in-out;\n}\n.navbar-light .navbar-toggler-icon {\n  padding: 15px;\n  background-color: white !important;\n  border-radius: 100px;\n}\n.animacao {\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n  margin-top: 50%;\n  width: 50px;\n  height: 50px;\n  border: 7px solid #a9a9a9;\n  border-top-color: #001c4e;\n  border-radius: 50px;\n  -webkit-animation: is-rotating 1.5s infinite;\n          animation: is-rotating 1.5s infinite;\n}\n.position {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: block;\n  position: absolute;\n  top: 5px;\n  left: 10px;\n}\n.navbar-collapse {\n  position: fixed;\n  top: 0px;\n  left: 0;\n  padding-left: 15px;\n  padding-right: 15px;\n  padding-bottom: 15px;\n  width: 75%;\n  height: 100%;\n  background: #001c4e;\n  text-transform: uppercase;\n  z-index: 9999;\n  box-shadow: 0px 0px 90px black;\n}\n.navbar-collapse a.nav-link {\n  color: white !important;\n  font-weight: bold;\n  font-size: 19px;\n}\nbutton.escolherDestino {\n  background: #001c4e;\n  border-color: #001c4e;\n  width: calc(100% - 60px) !important;\n  float: left;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.formDestino {\n  border: none;\n  margin-top: 5px;\n}\n.endDestinoRecente {\n  font-size: 11px;\n  background: #eee;\n  padding: 14px;\n  border-radius: 5px;\n}\n.endDestinoRecente b {\n  font-weight: normal;\n  color: darkgray;\n}\n.tituloDestinoRecente {\n  text-transform: uppercase;\n  font-weight: bold;\n}\nbutton.btn.btn-primary.btn-lg.rounded-pill.escolherDestino {\n  font-size: 15px;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.navbar-dark .navbar-toggler {\n  background: #001c4e;\n}\n.navbar-dark span.navbar-toggler-icon {\n  padding: 15px;\n}\n.navbar-collapse.collapsing {\n  left: -75%;\n  transition: height 0s ease;\n}\n.navbar-collapse.show {\n  margin-top: 0px;\n  left: 0;\n  transition: left 300ms ease-in-out;\n}\n.navbar-toggler.collapsed ~ .navbar-collapse {\n  transition: left 500ms ease-in-out;\n}\n.else {\n  font-size: 1.2rem;\n  display: block;\n  padding-top: 22px;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 20px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n.elseuaiLeva {\n  font-size: 1.2rem;\n  display: block;\n  padding-top: 2px;\n  padding-left: 100px;\n  text-transform: uppercase;\n  margin: 15px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\nspan {\n  font-size: 1rem;\n  display: block;\n  text-align: right;\n  text-transform: uppercase;\n  padding-top: 3px;\n  font-weight: bold;\n  color: #123b7d;\n}\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 10px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n.navbar.bg-dark {\n  background: #001c4e !important;\n}\nion-menu-button {\n  background-color: white;\n  border-radius: 100px;\n  width: 50px;\n  height: 50px;\n}\n.navbar-brand {\n  display: inline-block;\n  padding-top: 0.3125rem;\n  padding-bottom: 0.3125rem;\n  margin-right: 1rem;\n  font-size: 1.25rem;\n  line-height: inherit;\n  white-space: nowrap;\n}\n.navbar {\n  position: relative;\n  display: flex;\n  flex-wrap: wrap;\n  align-items: center;\n  justify-content: space-between;\n  padding: 0.5rem 1rem;\n}\n.comecar {\n  position: fixed;\n  width: 100%;\n  bottom: 0;\n  background: #001c4e;\n  padding: 21px;\n  color: #fff;\n  font-size: 20px;\n  border-radius: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUFBaEI7RUFDRSxrQkFBQTtFQUVBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxRQUFBO0VBQ0EsMkJBQUE7QUFDRjtBQUVBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FBQ0Y7QUFFQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUVBLGNBQUE7RUFFQSxTQUFBO0FBREY7QUFJQTtFQUNFLHFCQUFBO0FBREY7QUFJQTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQURGO0FBS0E7RUFDRSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUVFLFVBQUE7RUFDRixnQ0FBQTtFQUdBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0Esd0JBQUE7RUFBMEIseUJBQUE7RUFDMUIsbUNBQUE7RUFDQSxnQkFBQTtBQUpGO0FBT0E7RUFDRSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGdDQUFBO0VBQ0EsMEJBQUE7RUFDQSx3QkFBQTtFQUEwQix5QkFBQTtFQUMxQixXQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7QUFIRjtBQU9BO0VBQ0UsZUFBQTtBQUpGO0FBTUE7RUFDRSx5QkFBQTtFQUNBLGVBQUE7QUFIRjtBQU1BO0VBQ0Usa0NBQUE7QUFIRjtBQU1BO0VBRUUsV0FBQTtFQUdBLHFCQUFBO0VBQ0UsVUFBQTtBQU5KO0FBU0E7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUFOQTtBQVNBO0VBQ0EsK0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxhQUFBO0VBQ0EscUJBQUE7RUFDRSxVQUFBO0FBTkY7QUFTQTtFQUNBLGlCQUFBO0FBTkE7QUFTQTtFQUNFLFNBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFdBQUE7QUFORjtBQVNBO0VBQ0Usa0JBQUE7QUFORjtBQVNBO0VBQ0UsNkJBQUE7RUFDQSxTQUFBO0VBQ0EsdUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUFORjtBQVNBO0VBQ0UsZUFBQTtBQU5GO0FBU0E7RUFDRSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0EsbUNBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7RUFDQSxvQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHFCQUFBO0VBQ0UsVUFBQTtBQU5KO0FBVUE7RUFDRSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLG9CQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EscUJBQUE7RUFDRSxZQUFBO0FBUEo7QUFVQTtFQUNFLFlBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0VBQ0UsVUFBQTtBQVBKO0FBVUE7RUFDRSxXQUFBO0VBQ0EsY0FBQTtFQUNBLHFCQUFBO0VBQ0UsVUFBQTtBQVBKO0FBVUE7RUFDRyxtQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0MsVUFBQTtBQVBKO0FBVUE7RUFDRSxZQUFBO0VBQ0EsZUFBQTtFQUNBLHFCQUFBO0VBQ0UsVUFBQTtBQVBKO0FBVUE7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNFLFVBQUE7QUFQSjtBQVVBO0VBQ0UsbUJBQUE7RUFDQSxlQUFBO0FBUEY7QUFVQTtFQUNFLHlCQUFBO0VBQ0EsaUJBQUE7QUFQRjtBQVVBO0VBQ0UsZUFBQTtFQUNBLFFBQUE7RUFDQSxPQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLDhCQUFBO0FBUEY7QUFVQTtFQUNBLHVCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FBUEE7QUFXQTtFQUNFLG1CQUFBO0FBUkY7QUFXQTtFQUNFLGFBQUE7QUFSRjtBQWFBO0VBQ0UsVUFBQTtFQUNBLDBCQUFBO0FBVkY7QUFhQTtFQUNFLGVBQUE7RUFDQSxPQUFBO0VBQ0Esa0NBQUE7QUFWRjtBQWFBO0VBQ0Usa0NBQUE7QUFWRjtBQWFBO0VBQ0UsYUFBQTtFQUNBLGtDQUFBO0VBQ0Esb0JBQUE7QUFWRjtBQWNBO0VBQ0UsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBRUEsV0FBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSw0Q0FBQTtVQUFBLG9DQUFBO0FBWkY7QUFlQTtFQUNFLDBCQUFBO0VBQUEsdUJBQUE7RUFBQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxVQUFBO0FBWkY7QUFlQTtFQUNFLGVBQUE7RUFDQSxRQUFBO0VBQ0EsT0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSw4QkFBQTtBQVpGO0FBZUE7RUFDQSx1QkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQVpBO0FBZUE7RUFDRSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0EsbUNBQUE7RUFDQSxXQUFBO0VBQ0EscUJBQUE7RUFDRSxVQUFBO0FBWko7QUFlQTtFQUNFLFlBQUE7RUFDQSxlQUFBO0FBWkY7QUFjQTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtBQVhGO0FBY0E7RUFDRSxtQkFBQTtFQUNBLGVBQUE7QUFYRjtBQWNBO0VBQ0UseUJBQUE7RUFDQSxpQkFBQTtBQVhGO0FBYUE7RUFDRSxlQUFBO0VBQ0EscUJBQUE7RUFDRSxVQUFBO0FBVko7QUFhQTtFQUNFLG1CQUFBO0FBVkY7QUFhQTtFQUNFLGFBQUE7QUFWRjtBQWVBO0VBQ0UsVUFBQTtFQUNBLDBCQUFBO0FBWkY7QUFlQTtFQUNFLGVBQUE7RUFDQSxPQUFBO0VBQ0Esa0NBQUE7QUFaRjtBQWVBO0VBQ0Usa0NBQUE7QUFaRjtBQWVBO0VBQ0UsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQVpGO0FBZUU7RUFDSSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUVBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBYk47QUFnQkU7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQWJGO0FBZ0JFO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQWJGO0FBZ0JFO0VBQ0ksOEJBQUE7QUFiTjtBQWdCRTtFQUNFLHVCQUFBO0VBQ0Esb0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQWJKO0FBZ0JBO0VBQ0UscUJBQUE7RUFDQSxzQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EsbUJBQUE7QUFiRjtBQWdCQTtFQUNFLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0Esb0JBQUE7QUFiRjtBQWdCQTtFQUNFLGVBQUE7RUFDRSxXQUFBO0VBQ0EsU0FBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUFiSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAY2hhcnNldCBcIlVURi04XCI7XG4jY29udGFpbmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuXG4jY29udGFpbmVyIHN0cm9uZyB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbGluZS1oZWlnaHQ6IDI2cHg7XG59XG5cbiNjb250YWluZXIgcCB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbGluZS1oZWlnaHQ6IDIycHg7XG4gIGNvbG9yOiAjOGM4YzhjO1xuICBtYXJnaW46IDA7XG59XG5cbiNjb250YWluZXIgYSB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cblxuI21hcCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgYmFja2dyb3VuZDogI2VlZTtcbn1cblxuLmJveENvbnRldWRvIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIG91dGxpbmU6IDA7XG4gIGJveC1zaGFkb3c6IDBweCAwcHggMTBweCAjZTRlNGU0O1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luLWxlZnQ6IDEzcHg7XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbiAgei1pbmRleDogOTk5OSAhaW1wb3J0YW50O1xuICAvKiBuw7ptZXJvIG3DoXhpbW8gw6kgOTk5OSAqL1xuICB3aWR0aDogY2FsYygxMDAlIC0gMjVweCkgIWltcG9ydGFudDtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cblxuLnBhZ2FtZW50b3Mge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgYm94LXNoYWRvdzogMHB4IDBweCAxMHB4ICNlNGU0ZTQ7XG4gIHBvc2l0aW9uOiBmaXhlZCAhaW1wb3J0YW50O1xuICB6LWluZGV4OiA5OTk5ICFpbXBvcnRhbnQ7XG4gIC8qIG7Dum1lcm8gbcOheGltbyDDqSA5OTk5ICovXG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDIyNHB4O1xuICBtYXJnaW4tdG9wOiA5MCU7XG59XG5cbmJ1dHRvbi5idG4uYnRuLXByaW1hcnkuYnRuLWxnLnJvdW5kZWQtcGlsbC5lc2NvbGhlckRlc3Rpbm8ge1xuICBmb250LXNpemU6IDE1cHg7XG59XG5cbi5ib3hDb250ZXVkbyBsYWJlbCB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLmJveENvbnRldWRvIC5mb3JtLWNvbnRyb2wge1xuICBoZWlnaHQ6IGNhbGMoMS41ZW0gKyAuNzVyZW0gKyA0cHgpO1xufVxuXG4uYnV0dG9uQXJlYSBidXR0b24ge1xuICB3aWR0aDogMTAwJTtcbiAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xuICBvdXRsaW5lOiAwO1xufVxuXG4uYnV0dG9uQXJlYSBzdmcge1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG5idXR0b24ubmF2YmFyLXRvZ2dsZXIge1xuICBib3JkZXItcmFkaXVzOiAxMDBweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDU1cHggIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgYm9yZGVyOiAjOTJmMzI4IHNvbGlkIDBweDtcbiAgb3V0bGluZTogbm9uZTtcbiAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xuICBvdXRsaW5lOiAwO1xufVxuXG4uYm94Q29udGV1ZG8gLmJ0biB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4ubWFwYSBpZnJhbWUge1xuICBib3JkZXI6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogMHB4O1xuICB6LWluZGV4OiAtMTtcbn1cblxuLnNhdWRhY2FvIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uaG9tZVBhc3NhZ2Vpcm8gLmJveENvbnRldWRvIHtcbiAgcG9zaXRpb246IGFic29sdXRlICFpbXBvcnRhbnQ7XG4gIHRvcDogMzBweDtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDBweCk7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBwYWRkaW5nOiAxNXB4IDVweDtcbn1cblxuLmhvbWVQYXNzYWdlaXJvIC5idXR0b25BcmVhIGJ1dHRvbi5yb3VuZGVkLXBpbGwge1xuICBmb250LXNpemU6IDE1cHg7XG59XG5cbmJ1dHRvbi5lc2NvbGhlckRlc3Rpbm8ge1xuICBiYWNrZ3JvdW5kOiAjMDAxYzRlO1xuICBib3JkZXItY29sb3I6ICMwMDFjNGU7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSA2MHB4KSAhaW1wb3J0YW50O1xuICBmbG9hdDogbGVmdDtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xuICB3aWR0aDogNTBweDtcbiAgaGVpZ2h0OiA1MHB4O1xuICBjb2xvcjogI2ZmZjtcbiAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xuICBvdXRsaW5lOiAwO1xufVxuXG5idXR0b24ucGVnYXJDYXJybyB7XG4gIGJhY2tncm91bmQ6ICMwMDFjNGU7XG4gIGJvcmRlci1jb2xvcjogIzAwMWM0ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xuICBoZWlnaHQ6IDUwcHg7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXNpemU6IDE4cHg7XG4gIC0tYm94LXNoYWRvdzogMCAwIDAgMDtcbiAgLS1vdXRsaW5lOiAwO1xufVxuXG5idXR0b24uZW1iYXJxdWVSYXBpZG8ge1xuICBmbG9hdDogcmlnaHQ7XG4gIHBhZGRpbmc6IDBweCAxMnB4O1xuICB3aWR0aDogNDVweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDQ1cHg7XG4gIC0tYm94LXNoYWRvdzogMCAwIDAgMDtcbiAgb3V0bGluZTogMDtcbn1cblxuYnV0dG9uLmVtYmFycXVlUmFwaWRvIHN2ZyB7XG4gIHdpZHRoOiAxOXB4O1xuICBjb2xvcjogIzQxNjMxMztcbiAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xuICBvdXRsaW5lOiAwO1xufVxuXG5idXR0b24uYnRuLmJ0bi1wcmltYXJ5LmJ0bi1sZy5yb3VuZGVkLXBpbGwuZW1iYXJxdWVSYXBpZG8ge1xuICBiYWNrZ3JvdW5kOiAjYjFmZjQ5O1xuICBib3JkZXI6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gIC0tYm94LXNoYWRvdzogMCAwIDAgMDtcbiAgb3V0bGluZTogMDtcbn1cblxuLmZvcm1EZXN0aW5vIHtcbiAgYm9yZGVyOiBub25lO1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIC0tYm94LXNoYWRvdzogMCAwIDAgMDtcbiAgb3V0bGluZTogMDtcbn1cblxuLmVuZERlc3Rpbm9SZWNlbnRlIHtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBiYWNrZ3JvdW5kOiAjZWVlO1xuICBwYWRkaW5nOiAxNHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIC0tYm94LXNoYWRvdzogMCAwIDAgMDtcbiAgb3V0bGluZTogMDtcbn1cblxuLmVuZERlc3Rpbm9SZWNlbnRlIGIge1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBjb2xvcjogZGFya2dyYXk7XG59XG5cbi50aXR1bG9EZXN0aW5vUmVjZW50ZSB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4ubmF2YmFyLWNvbGxhcHNlIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB0b3A6IDBweDtcbiAgbGVmdDogMDtcbiAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgd2lkdGg6IDc1JTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBiYWNrZ3JvdW5kOiAjMDAxYzRlO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICB6LWluZGV4OiA5OTk5O1xuICBib3gtc2hhZG93OiAwcHggMHB4IDkwcHggYmxhY2s7XG59XG5cbi5uYXZiYXItY29sbGFwc2UgYS5uYXYtbGluayB7XG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxOXB4O1xufVxuXG4ubmF2YmFyLWRhcmsgLm5hdmJhci10b2dnbGVyIHtcbiAgYmFja2dyb3VuZDogIzAwMWM0ZTtcbn1cblxuLm5hdmJhci1kYXJrIHNwYW4ubmF2YmFyLXRvZ2dsZXItaWNvbiB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG5cbi5uYXZiYXItY29sbGFwc2UuY29sbGFwc2luZyB7XG4gIGxlZnQ6IC03NSU7XG4gIHRyYW5zaXRpb246IGhlaWdodCAwcyBlYXNlO1xufVxuXG4ubmF2YmFyLWNvbGxhcHNlLnNob3cge1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIGxlZnQ6IDA7XG4gIHRyYW5zaXRpb246IGxlZnQgMzAwbXMgZWFzZS1pbi1vdXQ7XG59XG5cbi5uYXZiYXItdG9nZ2xlci5jb2xsYXBzZWQgfiAubmF2YmFyLWNvbGxhcHNlIHtcbiAgdHJhbnNpdGlvbjogbGVmdCA1MDBtcyBlYXNlLWluLW91dDtcbn1cblxuLm5hdmJhci1saWdodCAubmF2YmFyLXRvZ2dsZXItaWNvbiB7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xufVxuXG4uYW5pbWFjYW8ge1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbiAgbWFyZ2luLXRvcDogNTAlO1xuICB3aWR0aDogNTBweDtcbiAgaGVpZ2h0OiA1MHB4O1xuICBib3JkZXI6IDdweCBzb2xpZCAjYTlhOWE5O1xuICBib3JkZXItdG9wLWNvbG9yOiAjMDAxYzRlO1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICBhbmltYXRpb246IGlzLXJvdGF0aW5nIDEuNXMgaW5maW5pdGU7XG59XG5cbi5wb3NpdGlvbiB7XG4gIHdpZHRoOiBmaXQtY29udGVudDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1cHg7XG4gIGxlZnQ6IDEwcHg7XG59XG5cbi5uYXZiYXItY29sbGFwc2Uge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHRvcDogMHB4O1xuICBsZWZ0OiAwO1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE1cHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICB3aWR0aDogNzUlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQ6ICMwMDFjNGU7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIHotaW5kZXg6IDk5OTk7XG4gIGJveC1zaGFkb3c6IDBweCAwcHggOTBweCBibGFjaztcbn1cblxuLm5hdmJhci1jb2xsYXBzZSBhLm5hdi1saW5rIHtcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDE5cHg7XG59XG5cbmJ1dHRvbi5lc2NvbGhlckRlc3Rpbm8ge1xuICBiYWNrZ3JvdW5kOiAjMDAxYzRlO1xuICBib3JkZXItY29sb3I6ICMwMDFjNGU7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSA2MHB4KSAhaW1wb3J0YW50O1xuICBmbG9hdDogbGVmdDtcbiAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xuICBvdXRsaW5lOiAwO1xufVxuXG4uZm9ybURlc3Rpbm8ge1xuICBib3JkZXI6IG5vbmU7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cblxuLmVuZERlc3Rpbm9SZWNlbnRlIHtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBiYWNrZ3JvdW5kOiAjZWVlO1xuICBwYWRkaW5nOiAxNHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbi5lbmREZXN0aW5vUmVjZW50ZSBiIHtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgY29sb3I6IGRhcmtncmF5O1xufVxuXG4udGl0dWxvRGVzdGlub1JlY2VudGUge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuYnV0dG9uLmJ0bi5idG4tcHJpbWFyeS5idG4tbGcucm91bmRlZC1waWxsLmVzY29saGVyRGVzdGlubyB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xuICBvdXRsaW5lOiAwO1xufVxuXG4ubmF2YmFyLWRhcmsgLm5hdmJhci10b2dnbGVyIHtcbiAgYmFja2dyb3VuZDogIzAwMWM0ZTtcbn1cblxuLm5hdmJhci1kYXJrIHNwYW4ubmF2YmFyLXRvZ2dsZXItaWNvbiB7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG5cbi5uYXZiYXItY29sbGFwc2UuY29sbGFwc2luZyB7XG4gIGxlZnQ6IC03NSU7XG4gIHRyYW5zaXRpb246IGhlaWdodCAwcyBlYXNlO1xufVxuXG4ubmF2YmFyLWNvbGxhcHNlLnNob3cge1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIGxlZnQ6IDA7XG4gIHRyYW5zaXRpb246IGxlZnQgMzAwbXMgZWFzZS1pbi1vdXQ7XG59XG5cbi5uYXZiYXItdG9nZ2xlci5jb2xsYXBzZWQgfiAubmF2YmFyLWNvbGxhcHNlIHtcbiAgdHJhbnNpdGlvbjogbGVmdCA1MDBtcyBlYXNlLWluLW91dDtcbn1cblxuLmVsc2Uge1xuICBmb250LXNpemU6IDEuMnJlbTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBhZGRpbmctdG9wOiAyMnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIG1hcmdpbjogMjBweCAwO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICMxMjNiN2Q7XG59XG5cbi5lbHNldWFpTGV2YSB7XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xuICBkaXNwbGF5OiBibG9jaztcbiAgcGFkZGluZy10b3A6IDJweDtcbiAgcGFkZGluZy1sZWZ0OiAxMDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgbWFyZ2luOiAxNXB4IDA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzEyM2I3ZDtcbn1cblxuc3BhbiB7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBwYWRkaW5nLXRvcDogM3B4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICMxMjNiN2Q7XG59XG5cbmgxIHtcbiAgZm9udC1zaXplOiAxLjVyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgbWFyZ2luOiAxMHB4IDA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzEyM2I3ZDtcbn1cblxuLm5hdmJhci5iZy1kYXJrIHtcbiAgYmFja2dyb3VuZDogIzAwMWM0ZSAhaW1wb3J0YW50O1xufVxuXG5pb24tbWVudS1idXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIHdpZHRoOiA1MHB4O1xuICBoZWlnaHQ6IDUwcHg7XG59XG5cbi5uYXZiYXItYnJhbmQge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBhZGRpbmctdG9wOiAwLjMxMjVyZW07XG4gIHBhZGRpbmctYm90dG9tOiAwLjMxMjVyZW07XG4gIG1hcmdpbi1yaWdodDogMXJlbTtcbiAgZm9udC1zaXplOiAxLjI1cmVtO1xuICBsaW5lLWhlaWdodDogaW5oZXJpdDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbn1cblxuLm5hdmJhciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIHBhZGRpbmc6IDAuNXJlbSAxcmVtO1xufVxuXG4uY29tZWNhciB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvdHRvbTogMDtcbiAgYmFja2dyb3VuZDogIzAwMWM0ZTtcbiAgcGFkZGluZzogMjFweDtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgYm9yZGVyLXJhZGl1czogNDBweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/home/home.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _pagamentos_pagamentos_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../pagamentos/pagamentos.page */ "./src/app/pages/pagamentos/pagamentos.page.ts");
/* harmony import */ var src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/core/user/user.service */ "./src/app/core/user/user.service.ts");
/* harmony import */ var src_app_services_viagem_viagemService__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/viagem/viagemService */ "./src/app/services/viagem/viagemService.ts");
/* harmony import */ var _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/call-number/ngx */ "./node_modules/@ionic-native/call-number/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var rx_polling__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rx-polling */ "./node_modules/rx-polling/lib/index.js");
/* harmony import */ var rx_polling__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(rx_polling__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _ionic_native_native_audio_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/native-audio/ngx */ "./node_modules/@ionic-native/native-audio/__ivy_ngcc__/ngx/index.js");












let HomePage = class HomePage {
    constructor(platform, loadCtrl, ngZone, navCtrl, menuCtrl, popoverCtrl, alertCtrl, modalCtrl, toastCtrl, userService, loadingController, router, viagemService, callNumber, nativeAudio) {
        //this.time();
        this.platform = platform;
        this.loadCtrl = loadCtrl;
        this.ngZone = ngZone;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.userService = userService;
        this.loadingController = loadingController;
        this.router = router;
        this.viagemService = viagemService;
        this.callNumber = callNumber;
        this.nativeAudio = nativeAudio;
        this.search = '';
        this.searchOrigem = '';
        this.googleAutocomplete = new google.maps.places.AutocompleteService();
        this.searchResults = new Array();
        this.searchResultsOrigem = new Array();
        this.googleDirectionsService = new google.maps.DirectionsService();
        this.matrix = new google.maps.DistanceMatrixService();
        this.valor = new Array();
        this.valores = [];
        this.ringtones = [];
        this.user$ = userService.getUser();
        this.user$.subscribe(usuario => {
            this.usuarioLogado = usuario;
            this.user_id = this.usuarioLogado.id;
        });
        const request$ = this.viagemService.allViagem();
        const options = { interval: 5000 };
        const r = this.viagemService.findMotorista(this.user_id);
        rx_polling__WEBPACK_IMPORTED_MODULE_9___default()(r, options).subscribe((usuario) => {
            this.u = usuario;
            if (this.u.logado == 'TRUE') {
                document.getElementById("comecar").style.display = "none";
                document.getElementById("encerrar").style.display = "block";
                rx_polling__WEBPACK_IMPORTED_MODULE_9___default()(request$, options)
                    .subscribe((viagem) => {
                    this.viagem = viagem;
                    for (var v of this.viagem) {
                        this.nativeAudio.preloadSimple('papaleguas', 'assets/ringtones/papaleguas.mp3');
                        this.nativeAudio.play('papaleguas').then(() => {
                            console.log('som');
                        });
                        var a = v.origem.split(',');
                        v.origem = a[0] + ',' + a[1];
                        var b = v.destino.split(',');
                        v.destino = b[0] + ',' + b[1] + ' - ' + b[2];
                        if (v.forma_pagamento == undefined || v.forma_pagamento == '' || v.forma_pagamento == 'undefined') {
                            v.forma_pagamento = 'dinheiro';
                        }
                    }
                }, (error) => {
                    console.error(error);
                });
            }
            else {
                document.getElementById("comecar").style.display = "block";
                document.getElementById("encerrar").style.display = "none";
            }
        });
    }
    ionViewWillEnter() {
        this.menuCtrl.enable(true);
        this.user$ = this.userService.getUser();
        this.user$.subscribe(usuario => {
            this.usuarioLogado = usuario;
            this.user_id = this.usuarioLogado.id;
        });
    }
    ngOnInit() {
        this.mapElement = this.mapElement.nativeElement;
        this.mapElement.style.width = this.platform.width() + 'px';
        this.mapElement.style.height = this.platform.height() + 'px';
        this.loadMap();
    }
    loadMap() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loading = yield this.loadCtrl.create({ message: 'Por favor aguarde ...' });
            yield this.loading.present();
            _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["Environment"].setEnv({
                'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyC0inbEk4sOpHIT8lUyG3PI4XziAvl3CI0',
                'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyC0inbEk4sOpHIT8lUyG3PI4XziAvl3CI0'
            });
            const mapOptions = {
                controls: {
                    zoom: false
                }
            };
            this.map = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMaps"].create(this.mapElement, mapOptions);
            try {
                yield this.map.one(_ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsEvent"].MAP_READY);
                this.addOrigenMarker();
            }
            catch (error) {
                console.error(error);
            }
        });
    }
    addOrigenMarker() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                const myLocation = yield this.map.getMyLocation();
                yield this.map.moveCamera({
                    target: myLocation.latLng,
                    zoom: 18
                });
                this.originMarker = this.map.addMarkerSync({
                    title: 'Origem',
                    icon: '#000',
                    animation: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsAnimation"].DROP,
                    position: myLocation.latLng
                });
            }
            catch (error) {
                console.error(error);
            }
            finally {
                this.loading.dismiss();
            }
        });
    }
    searchChangedOrigem() {
        if (!this.searchOrigem.trim().length)
            return;
        this.googleAutocomplete.getPlacePredictions({ input: this.searchOrigem }, predictions => {
            this.ngZone.run(() => {
                this.searchResultsOrigem = predictions;
            });
        });
    }
    searchChanged() {
        if (!this.search.trim().length)
            return;
        this.googleAutocomplete.getPlacePredictions({ input: this.search }, predictions => {
            this.ngZone.run(() => {
                this.searchResults = predictions;
            });
        });
    }
    calcRouteOrigem(item) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.origem = item;
            console.log(this.origem);
            const info = yield _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["Geocoder"].geocode({ address: this.origem.description });
            this.originMarkerDiferente = this.map.addMarkerSync({
                title: this.origem.description,
                icon: '#000',
                animation: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsAnimation"].DROP,
                position: info[0].position
            });
        });
    }
    calcRoute(item) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.search = '';
            this.destination = item;
            const info = yield _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["Geocoder"].geocode({ address: this.destination.description });
            let markerDestination = this.map.addMarkerSync({
                title: this.destination.description,
                icon: '#000',
                animation: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsAnimation"].DROP,
                position: info[0].position
            });
            if (this.originMarkerDiferente == undefined) {
                this.originMarkerDiferente = this.originMarker;
            }
            this.googleDirectionsService.route({
                origin: this.originMarkerDiferente.getPosition(),
                destination: markerDestination.getPosition(),
                travelMode: 'DRIVING'
            }, (results) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const points = new Array();
                const routes = results.routes[0].overview_path;
                for (let i = 0; i < routes.length; i++) {
                    points[i] = {
                        lat: routes[i].lat(),
                        lng: routes[i].lng()
                    };
                }
                yield this.map.addPolyline({
                    points: points,
                    color: 'red',
                    width: 3
                });
                var origin1 = new google.maps.LatLng(this.originMarkerDiferente.getPosition().lat, this.originMarkerDiferente.getPosition().lng);
                var destinationB = new google.maps.LatLng(markerDestination.getPosition().lat, markerDestination.getPosition().lng);
                this.matrix.getDistanceMatrix({
                    origins: [origin1],
                    destinations: [destinationB],
                    travelMode: google.maps.TravelMode.DRIVING
                }, callback);
                function callback(response, status) {
                    let travelDetailsObject;
                    if (status !== "OK") {
                        alert("Error was: " + status);
                    }
                    else {
                        var origins = response.originAddresses;
                        var destinations = response.destinationAddresses;
                        for (var i = 0; i < origins.length; i++) {
                            var results = response.rows[i].elements;
                            for (var j = 0; j < results.length; j++) {
                                var element = results[j];
                                var distance = element.distance.text;
                                var duration = element.duration.text;
                                var from = origins[i];
                                var to = destinations[j];
                                travelDetailsObject = {
                                    distance: distance,
                                    duration: duration
                                };
                            }
                        }
                        this.travelDetailsObject = travelDetailsObject;
                        this.valor = this.travelDetailsObject;
                        this.corrida = 'distancia : ' + this.travelDetailsObject.distance + ' no tempo de : ' + this.travelDetailsObject.duration;
                    }
                }
                this.map.moveCamera({ target: points });
                this.map.panBy(0, 100);
            }));
        });
    }
    ligar(n) {
        this.callNumber.callNumber(n, true)
            .then(() => console.log('Launched dialer!'))
            .catch(() => console.log('Error launching dialer'));
    }
    comecar() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const myLocation = yield this.map.getMyLocation();
            this.viagemService.updateLogado('' + this.user_id).subscribe();
            // this.viagemService.localMotorista(''+this.user_id,myLocation.latLng.lat,myLocation.latLng.lng ).subscribe();
        });
    }
    encerrar() {
        location.reload();
        this.viagemService.updateEncerrado('' + this.user_id).subscribe();
    }
    aceitar(id_usuario, id_viagem) {
        this.viagemService.aceitar(id_usuario, this.user_id, id_viagem).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.router.navigate(['corrida/' + id_viagem + '']);
            const toast = yield (yield this.toastCtrl.create({
                message: 'Iniciando a viagem',
                duration: 4000, position: 'middle',
                color: 'success'
            })).present();
        }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(err);
            const toast = yield (yield this.toastCtrl.create({
                message: 'Viagem aceita por outro motorista!',
                duration: 4000, position: 'middle',
                color: 'danger'
            })).present();
        }));
    }
    back() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                yield this.map.clear();
                this.destination = null;
                this.origem = null;
                this.searchOrigem = null;
                document.getElementById("collapseExample").style.display == 'none';
                this.addOrigenMarker();
            }
            catch (error) {
                console.error(error);
            }
        });
    }
    toggleTeste() {
        if (document.getElementById("collapseExample").style.display == 'none') {
            document.getElementById("collapseExample").style.display = "block";
        }
        else {
            document.getElementById("collapseExample").style.display = "none";
        }
    }
    toggleTeste1() {
        if (document.getElementById("collapseExample1").style.display == 'none') {
            document.getElementById("collapseExample1").style.display = "block";
        }
        else {
            document.getElementById("collapseExample1").style.display = "none";
        }
    }
    toggleCarregar() {
        if (document.getElementById("carregando").style.display == 'none') {
            document.getElementById("carregando").style.display = "block";
        }
        else {
            document.getElementById("carregando").style.display = "none";
        }
    }
    logout() {
        // this.userService.logout();
        this.router.navigate(['']);
    }
    showModalPagamentos() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _pagamentos_pagamentos_page__WEBPACK_IMPORTED_MODULE_5__["PagamentosPage"]
            });
            modal.present();
        });
    }
    /*time(){
      const today = new Date();
      const h = today.getHours();
      const m = today.getMinutes;
      this.hora = "{{h}}:{{m}}";
      setTimeout('time()', 500);
    }*/
    aceitarViagem() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                mode: 'md',
                cssClass: 'custom-viagem',
                subHeader: '16:30',
                message: ` <p><b>Passageiro:</b>this.usuarioLogado.full_name</p>
                <p><b>Valor:</b> R$ 7,53</p>
                <p><b>Origem:</b> Av. Doutor Arthur Ribeiro Guimarães, 328, Jardim Noronha</p>
                <p><b>Destino:</b> Rua das Rosas, 355, Jardim Yara</p>`,
                buttons: [
                    {
                        text: 'Recusar',
                        cssClass: 'custom-recusar',
                        handler: () => {
                            console.log('recusou viagem');
                        }
                    },
                    {
                        text: 'Aceitar',
                        cssClass: 'custom-aceitar',
                        handler: () => {
                            console.log('aceitar viagem');
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
};
HomePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_services_viagem_viagemService__WEBPACK_IMPORTED_MODULE_7__["ViagemService"] },
    { type: _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_8__["CallNumber"] },
    { type: _ionic_native_native_audio_ngx__WEBPACK_IMPORTED_MODULE_10__["NativeAudio"] }
];
HomePage.propDecorators = {
    mapElement: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ['map', { static: true },] }]
};
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.page.scss */ "./src/app/pages/home/home.page.scss")).default]
    })
], HomePage);



/***/ })

}]);
//# sourceMappingURL=pages-home-home-module-es2015.js.map