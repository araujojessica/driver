(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-minhas-viagens-minhas-viagens-module"],{

/***/ "./src/app/pages/minhas-viagens/minhas-viagens-routing.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/minhas-viagens/minhas-viagens-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: MinhasViagensPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MinhasViagensPageRoutingModule", function() { return MinhasViagensPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _minhas_viagens_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./minhas-viagens.page */ "./src/app/pages/minhas-viagens/minhas-viagens.page.ts");




const routes = [
    {
        path: '',
        component: _minhas_viagens_page__WEBPACK_IMPORTED_MODULE_3__["MinhasViagensPage"]
    }
];
let MinhasViagensPageRoutingModule = class MinhasViagensPageRoutingModule {
};
MinhasViagensPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MinhasViagensPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/minhas-viagens/minhas-viagens.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/minhas-viagens/minhas-viagens.module.ts ***!
  \***************************************************************/
/*! exports provided: MinhasViagensPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MinhasViagensPageModule", function() { return MinhasViagensPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _minhas_viagens_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./minhas-viagens-routing.module */ "./src/app/pages/minhas-viagens/minhas-viagens-routing.module.ts");
/* harmony import */ var _minhas_viagens_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./minhas-viagens.page */ "./src/app/pages/minhas-viagens/minhas-viagens.page.ts");







let MinhasViagensPageModule = class MinhasViagensPageModule {
};
MinhasViagensPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _minhas_viagens_routing_module__WEBPACK_IMPORTED_MODULE_5__["MinhasViagensPageRoutingModule"]
        ],
        declarations: [_minhas_viagens_page__WEBPACK_IMPORTED_MODULE_6__["MinhasViagensPage"]],
        exports: [_minhas_viagens_page__WEBPACK_IMPORTED_MODULE_6__["MinhasViagensPage"]]
    })
], MinhasViagensPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-minhas-viagens-minhas-viagens-module-es2015.js.map