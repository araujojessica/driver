(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (" <ion-header>\r\n  <nav class=\"navbar navbar-expand-lg navbar-light bg-dark\">\r\n      <a class=\"navbar-brand\" href=\"#\">\r\n      <img src=\"/assets/img/logo-branco.png\" style=\"width: 2.5em;\"/>\r\n      </a>\r\n      <ion-buttons slot=\"end\">\r\n        <ion-menu-button color=\"medium\"></ion-menu-button>\r\n      </ion-buttons>\r\n  </nav>\r\n</ion-header>\r\n<ion-content>\r\n  <div #map>\r\n    <div class=\"boxConteudo\" [hidden]=\"destination\">\r\n      <div class=\"saudacao\">\r\n          <ion-text style=\"color: #001c4e;\" *ngIf=\"(user$ | async) as user;\">\r\n              Olá, {{user.full_name}}\r\n          </ion-text> \r\n          <br>\r\n          <ion-text style=\"color: #001c4e;\">\r\n            Que seja um dia abençoado!\r\n        </ion-text> \r\n          <br>\r\n      </div>\r\n\r\n\r\n    </div>\r\n\r\n    <div  *ngFor= \"let viagem of viagem\">\r\n      <div class=\"vibra\">\r\n              <p><b>{{viagem.full_name}}</b></p>\r\n              <p><b>Origem:</b> {{viagem.origem}}</p>\r\n              <p><b>Destino:</b> {{viagem.destino}}</p>\r\n              <p><b>Valor da corrida:</b> R$ {{viagem.valor_corrida}}</p>\r\n              <p><b>Forma de pagamento:</b> {{viagem.forma_pagamento}}</p>\r\n              <ion-button class=\"botao\" (click) = \"aceitar(viagem.id_usuario,viagem.id_viagem);\">ACEITAR CORRIDA</ion-button>\r\n      </div>\r\n            \r\n  </div>\r\n      <button (click)=\"comecar();\" class=\"comecar\" id = \"comecar\" style=\"display: block;\">COMEÇAR</button>\r\n      <button (click)=\"encerrar();\" class=\"comecar\" id = \"encerrar\" style=\"display: none;\">ENCERRAR</button>\r\n  </div>\r\n  <style>\r\n    body {\r\n    background: #824438;\r\n  }\r\n  \r\n  .vibra {\r\n    margin: 120px auto;\r\n    width: 300px;\r\n    animation: vibrador 0.82s cubic-bezier(.36,.07,.19,.97) both;\r\n    animation-iteration-count: infinite;\r\n    transform: translate3d(0, 0, 0);\r\n    backface-visibility: hidden;\r\n    perspective: 1000px;\r\n    background:white;\r\n    text-align:center;\r\n    height:270px;\r\n    border-radius: 15px;\r\n\r\n  }\r\n\r\n  p, b {\r\n    color:#001c4e;\r\n  }\r\n  \r\n  @keyframes vibrador {\r\n    10%, 90% {\r\n      transform: translate3d(-1px, 0, 0);\r\n    }\r\n    \r\n    20%, 80% {\r\n      transform: translate3d(2px, 0, 0);\r\n    }\r\n  \r\n    30%, 50%, 70% {\r\n      transform: translate3d(-4px, 0, 0);\r\n    }\r\n  \r\n    40%, 60% {\r\n      transform: translate3d(4px, 0, 0);\r\n    }\r\n  }\r\n  </style>\r\n  <!--<ion-buttons class=\"ion-margin-top\" slot=\"start\" (click)=\"back()\" [hidden]=\"!destination\" style=\"position: fixed;\">\r\n        <ion-button text=\"\" icon=\"close\" style=\"height: 50px; width: 50px; border-radius: 50px; background-color: white;\">\r\n            <ion-icon name=\"arrow-back\" style=\"color: #001c4e; size: 5em;\"></ion-icon>\r\n        </ion-button>\r\n    </ion-buttons>\r\n       \r\n    <br>\r\n\r\n    \r\n    <button type=\"button\" class=\"btn btn-primary btn-lg rounded-pill pegarCarro\" (click)=\"ligar('35998401989');\">\r\n      LIGAR\r\n    </button>\r\n  </div>\r\n\r\n      \r\n  </div>\r\n\r\n\r\n\r\n  -->\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/home/home-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/home/home-routing.module.ts ***!
  \***************************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"],
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], HomePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/pages/home/home-routing.module.ts");







let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_6__["HomePageRoutingModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]],
        entryComponents: []
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/pages/home/home.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@charset \"UTF-8\";\n#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n#container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n#container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #8c8c8c;\n  margin: 0;\n}\n#container a {\n  text-decoration: none;\n}\n#map {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  background: #eee;\n}\n.boxConteudo {\n  background: white;\n  padding: 10px;\n  border-radius: 20px;\n  outline: 0;\n  box-shadow: 0px 0px 10px #e4e4e4;\n  position: absolute !important;\n  display: block;\n  margin-left: 13px;\n  margin-right: auto;\n  z-index: 9999 !important;\n  /* número máximo é 9999 */\n  width: calc(100% - 25px) !important;\n  margin-top: 15px;\n}\n.pagamentos {\n  background: white;\n  padding: 10px;\n  border-radius: 20px;\n  box-shadow: 0px 0px 10px #e4e4e4;\n  position: fixed !important;\n  z-index: 9999 !important;\n  /* número máximo é 9999 */\n  width: 100%;\n  height: 224px;\n  margin-top: 90%;\n}\nbutton.btn.btn-primary.btn-lg.rounded-pill.escolherDestino {\n  font-size: 15px;\n}\n.boxConteudo label {\n  text-transform: uppercase;\n  font-size: 12px;\n}\n.boxConteudo .form-control {\n  height: calc(1.5em + .75rem + 4px);\n}\n.buttonArea button {\n  width: 100%;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.buttonArea svg {\n  font-size: 25px;\n  margin-right: 5px;\n}\nbutton.navbar-toggler {\n  border-radius: 100px !important;\n  height: 55px !important;\n  background: #ffffff;\n  border: #92f328 solid 0px;\n  outline: none;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.boxConteudo .btn {\n  font-weight: bold;\n}\n.mapa iframe {\n  border: 0;\n  width: 100%;\n  height: 100vh;\n  position: relative;\n  top: 0px;\n  z-index: -1;\n}\n.saudacao {\n  text-align: center;\n}\n.homePassageiro .boxConteudo {\n  position: absolute !important;\n  top: 30px;\n  width: calc(100% - 0px);\n  margin: 0 auto;\n  padding: 15px 5px;\n}\n.homePassageiro .buttonArea button.rounded-pill {\n  font-size: 15px;\n}\nbutton.escolherDestino {\n  background: #001c4e;\n  border-color: #001c4e;\n  width: calc(100% - 60px) !important;\n  float: left;\n  background-color: white;\n  border-radius: 100px;\n  width: 50px;\n  height: 50px;\n  color: #fff;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\nbutton.pegarCarro {\n  background: #001c4e;\n  border-color: #001c4e;\n  width: 100%;\n  border-radius: 100px;\n  height: 50px;\n  color: #fff;\n  font-size: 18px;\n  --box-shadow: 0 0 0 0;\n  --outline: 0;\n}\nbutton.embarqueRapido {\n  float: right;\n  padding: 0px 12px;\n  width: 45px !important;\n  height: 45px;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\nbutton.embarqueRapido svg {\n  width: 19px;\n  color: #416313;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\nbutton.btn.btn-primary.btn-lg.rounded-pill.embarqueRapido {\n  background: #b1ff49;\n  border: none;\n  border-radius: 100%;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.formDestino {\n  border: none;\n  margin-top: 5px;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.endDestinoRecente {\n  font-size: 11px;\n  background: #eee;\n  padding: 14px;\n  border-radius: 5px;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.endDestinoRecente b {\n  font-weight: normal;\n  color: darkgray;\n}\n.tituloDestinoRecente {\n  text-transform: uppercase;\n  font-weight: bold;\n}\n.navbar-collapse {\n  position: fixed;\n  top: 0px;\n  left: 0;\n  padding-left: 15px;\n  padding-right: 15px;\n  padding-bottom: 15px;\n  width: 75%;\n  height: 100%;\n  background: #001c4e;\n  text-transform: uppercase;\n  z-index: 9999;\n  box-shadow: 0px 0px 90px black;\n}\n.navbar-collapse a.nav-link {\n  color: white !important;\n  font-weight: bold;\n  font-size: 19px;\n}\n.navbar-dark .navbar-toggler {\n  background: #001c4e;\n}\n.navbar-dark span.navbar-toggler-icon {\n  padding: 15px;\n}\n.navbar-collapse.collapsing {\n  left: -75%;\n  transition: height 0s ease;\n}\n.navbar-collapse.show {\n  margin-top: 0px;\n  left: 0;\n  transition: left 300ms ease-in-out;\n}\n.navbar-toggler.collapsed ~ .navbar-collapse {\n  transition: left 500ms ease-in-out;\n}\n.navbar-light .navbar-toggler-icon {\n  padding: 15px;\n  background-color: white !important;\n  border-radius: 100px;\n}\n.animacao {\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n  margin-top: 50%;\n  width: 50px;\n  height: 50px;\n  border: 7px solid #a9a9a9;\n  border-top-color: #001c4e;\n  border-radius: 50px;\n  -webkit-animation: is-rotating 1.5s infinite;\n          animation: is-rotating 1.5s infinite;\n}\n.position {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  display: block;\n  position: absolute;\n  top: 5px;\n  left: 10px;\n}\n.navbar-collapse {\n  position: fixed;\n  top: 0px;\n  left: 0;\n  padding-left: 15px;\n  padding-right: 15px;\n  padding-bottom: 15px;\n  width: 75%;\n  height: 100%;\n  background: #001c4e;\n  text-transform: uppercase;\n  z-index: 9999;\n  box-shadow: 0px 0px 90px black;\n}\n.navbar-collapse a.nav-link {\n  color: white !important;\n  font-weight: bold;\n  font-size: 19px;\n}\nbutton.escolherDestino {\n  background: #001c4e;\n  border-color: #001c4e;\n  width: calc(100% - 60px) !important;\n  float: left;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.formDestino {\n  border: none;\n  margin-top: 5px;\n}\n.endDestinoRecente {\n  font-size: 11px;\n  background: #eee;\n  padding: 14px;\n  border-radius: 5px;\n}\n.endDestinoRecente b {\n  font-weight: normal;\n  color: darkgray;\n}\n.tituloDestinoRecente {\n  text-transform: uppercase;\n  font-weight: bold;\n}\nbutton.btn.btn-primary.btn-lg.rounded-pill.escolherDestino {\n  font-size: 15px;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n.navbar-dark .navbar-toggler {\n  background: #001c4e;\n}\n.navbar-dark span.navbar-toggler-icon {\n  padding: 15px;\n}\n.navbar-collapse.collapsing {\n  left: -75%;\n  transition: height 0s ease;\n}\n.navbar-collapse.show {\n  margin-top: 0px;\n  left: 0;\n  transition: left 300ms ease-in-out;\n}\n.navbar-toggler.collapsed ~ .navbar-collapse {\n  transition: left 500ms ease-in-out;\n}\n.else {\n  font-size: 1.2rem;\n  display: block;\n  padding-top: 22px;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 20px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n.elseuaiLeva {\n  font-size: 1.2rem;\n  display: block;\n  padding-top: 2px;\n  padding-left: 100px;\n  text-transform: uppercase;\n  margin: 15px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\nspan {\n  font-size: 1rem;\n  display: block;\n  text-align: right;\n  text-transform: uppercase;\n  padding-top: 3px;\n  font-weight: bold;\n  color: #123b7d;\n}\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 10px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n.navbar.bg-dark {\n  background: #001c4e !important;\n}\nion-menu-button {\n  background-color: white;\n  border-radius: 100px;\n  width: 50px;\n  height: 50px;\n}\n.navbar-brand {\n  display: inline-block;\n  padding-top: 0.3125rem;\n  padding-bottom: 0.3125rem;\n  margin-right: 1rem;\n  font-size: 1.25rem;\n  line-height: inherit;\n  white-space: nowrap;\n}\n.navbar {\n  position: relative;\n  display: flex;\n  flex-wrap: wrap;\n  align-items: center;\n  justify-content: space-between;\n  padding: 0.5rem 1rem;\n}\n.comecar {\n  position: fixed;\n  width: 100%;\n  bottom: 10%;\n  background: #001c4e;\n  padding: 21px;\n  color: #fff;\n  font-size: 20px;\n  border-radius: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUFBaEI7RUFDRSxrQkFBQTtFQUVBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxRQUFBO0VBQ0EsMkJBQUE7QUFDRjtBQUVBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FBQ0Y7QUFFQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUVBLGNBQUE7RUFFQSxTQUFBO0FBREY7QUFJQTtFQUNFLHFCQUFBO0FBREY7QUFJQTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQURGO0FBS0E7RUFDRSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUVFLFVBQUE7RUFDRixnQ0FBQTtFQUNBLDZCQUFBO0VBRUEsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSx3QkFBQTtFQUEwQix5QkFBQTtFQUMxQixtQ0FBQTtFQUNBLGdCQUFBO0FBSEY7QUFNQTtFQUNFLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0NBQUE7RUFDQSwwQkFBQTtFQUNBLHdCQUFBO0VBQTBCLHlCQUFBO0VBQzFCLFdBQUE7RUFDQSxhQUFBO0VBQ0EsZUFBQTtBQUZGO0FBTUE7RUFDRSxlQUFBO0FBSEY7QUFLQTtFQUNFLHlCQUFBO0VBQ0EsZUFBQTtBQUZGO0FBS0E7RUFDRSxrQ0FBQTtBQUZGO0FBS0E7RUFFRSxXQUFBO0VBR0EscUJBQUE7RUFDRSxVQUFBO0FBTEo7QUFRQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQUxBO0FBUUE7RUFDQSwrQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSxxQkFBQTtFQUNFLFVBQUE7QUFMRjtBQVFBO0VBQ0EsaUJBQUE7QUFMQTtBQVFBO0VBQ0UsU0FBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsV0FBQTtBQUxGO0FBUUE7RUFDRSxrQkFBQTtBQUxGO0FBUUE7RUFDRSw2QkFBQTtFQUNBLFNBQUE7RUFDQSx1QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQUxGO0FBUUE7RUFDRSxlQUFBO0FBTEY7QUFRQTtFQUNFLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxtQ0FBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtFQUNBLG9CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EscUJBQUE7RUFDRSxVQUFBO0FBTEo7QUFTQTtFQUNFLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0Esb0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxxQkFBQTtFQUNFLFlBQUE7QUFOSjtBQVNBO0VBQ0UsWUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7RUFDRSxVQUFBO0FBTko7QUFTQTtFQUNFLFdBQUE7RUFDQSxjQUFBO0VBQ0EscUJBQUE7RUFDRSxVQUFBO0FBTko7QUFTQTtFQUNHLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7RUFDQyxVQUFBO0FBTko7QUFTQTtFQUNFLFlBQUE7RUFDQSxlQUFBO0VBQ0EscUJBQUE7RUFDRSxVQUFBO0FBTko7QUFTQTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0UsVUFBQTtBQU5KO0FBU0E7RUFDRSxtQkFBQTtFQUNBLGVBQUE7QUFORjtBQVNBO0VBQ0UseUJBQUE7RUFDQSxpQkFBQTtBQU5GO0FBU0E7RUFDRSxlQUFBO0VBQ0EsUUFBQTtFQUNBLE9BQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0Esb0JBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxhQUFBO0VBQ0EsOEJBQUE7QUFORjtBQVNBO0VBQ0EsdUJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUFOQTtBQVVBO0VBQ0UsbUJBQUE7QUFQRjtBQVVBO0VBQ0UsYUFBQTtBQVBGO0FBWUE7RUFDRSxVQUFBO0VBQ0EsMEJBQUE7QUFURjtBQVlBO0VBQ0UsZUFBQTtFQUNBLE9BQUE7RUFDQSxrQ0FBQTtBQVRGO0FBWUE7RUFDRSxrQ0FBQTtBQVRGO0FBWUE7RUFDRSxhQUFBO0VBQ0Esa0NBQUE7RUFDQSxvQkFBQTtBQVRGO0FBYUE7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFFQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLDRDQUFBO1VBQUEsb0NBQUE7QUFYRjtBQWNBO0VBQ0UsMEJBQUE7RUFBQSx1QkFBQTtFQUFBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7QUFYRjtBQWNBO0VBQ0UsZUFBQTtFQUNBLFFBQUE7RUFDQSxPQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLDhCQUFBO0FBWEY7QUFjQTtFQUNBLHVCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FBWEE7QUFjQTtFQUNFLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxtQ0FBQTtFQUNBLFdBQUE7RUFDQSxxQkFBQTtFQUNFLFVBQUE7QUFYSjtBQWNBO0VBQ0UsWUFBQTtFQUNBLGVBQUE7QUFYRjtBQWFBO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0FBVkY7QUFhQTtFQUNFLG1CQUFBO0VBQ0EsZUFBQTtBQVZGO0FBYUE7RUFDRSx5QkFBQTtFQUNBLGlCQUFBO0FBVkY7QUFZQTtFQUNFLGVBQUE7RUFDQSxxQkFBQTtFQUNFLFVBQUE7QUFUSjtBQVlBO0VBQ0UsbUJBQUE7QUFURjtBQVlBO0VBQ0UsYUFBQTtBQVRGO0FBY0E7RUFDRSxVQUFBO0VBQ0EsMEJBQUE7QUFYRjtBQWNBO0VBQ0UsZUFBQTtFQUNBLE9BQUE7RUFDQSxrQ0FBQTtBQVhGO0FBY0E7RUFDRSxrQ0FBQTtBQVhGO0FBY0E7RUFDRSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBWEY7QUFjRTtFQUNJLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBRUEsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUFaTjtBQWVFO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUFaRjtBQWVFO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQVpGO0FBZUU7RUFDSSw4QkFBQTtBQVpOO0FBZUU7RUFDRSx1QkFBQTtFQUNBLG9CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFaSjtBQWVBO0VBQ0UscUJBQUE7RUFDQSxzQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EsbUJBQUE7QUFaRjtBQWVBO0VBQ0Usa0JBQUE7RUFDQSxhQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7RUFDQSxvQkFBQTtBQVpGO0FBZUE7RUFDRSxlQUFBO0VBQ0UsV0FBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FBWkoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGNoYXJzZXQgXCJVVEYtOFwiO1xuI2NvbnRhaW5lciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgdG9wOiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbn1cblxuI2NvbnRhaW5lciBzdHJvbmcge1xuICBmb250LXNpemU6IDIwcHg7XG4gIGxpbmUtaGVpZ2h0OiAyNnB4O1xufVxuXG4jY29udGFpbmVyIHAge1xuICBmb250LXNpemU6IDE2cHg7XG4gIGxpbmUtaGVpZ2h0OiAyMnB4O1xuICBjb2xvcjogIzhjOGM4YztcbiAgbWFyZ2luOiAwO1xufVxuXG4jY29udGFpbmVyIGEge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbiNtYXAge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQ6ICNlZWU7XG59XG5cbi5ib3hDb250ZXVkbyB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICBvdXRsaW5lOiAwO1xuICBib3gtc2hhZG93OiAwcHggMHB4IDEwcHggI2U0ZTRlNDtcbiAgcG9zaXRpb246IGFic29sdXRlICFpbXBvcnRhbnQ7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW4tbGVmdDogMTNweDtcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICB6LWluZGV4OiA5OTk5ICFpbXBvcnRhbnQ7XG4gIC8qIG7Dum1lcm8gbcOheGltbyDDqSA5OTk5ICovXG4gIHdpZHRoOiBjYWxjKDEwMCUgLSAyNXB4KSAhaW1wb3J0YW50O1xuICBtYXJnaW4tdG9wOiAxNXB4O1xufVxuXG4ucGFnYW1lbnRvcyB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICBib3gtc2hhZG93OiAwcHggMHB4IDEwcHggI2U0ZTRlNDtcbiAgcG9zaXRpb246IGZpeGVkICFpbXBvcnRhbnQ7XG4gIHotaW5kZXg6IDk5OTkgIWltcG9ydGFudDtcbiAgLyogbsO6bWVybyBtw6F4aW1vIMOpIDk5OTkgKi9cbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMjI0cHg7XG4gIG1hcmdpbi10b3A6IDkwJTtcbn1cblxuYnV0dG9uLmJ0bi5idG4tcHJpbWFyeS5idG4tbGcucm91bmRlZC1waWxsLmVzY29saGVyRGVzdGlubyB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbn1cblxuLmJveENvbnRldWRvIGxhYmVsIHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4uYm94Q29udGV1ZG8gLmZvcm0tY29udHJvbCB7XG4gIGhlaWdodDogY2FsYygxLjVlbSArIC43NXJlbSArIDRweCk7XG59XG5cbi5idXR0b25BcmVhIGJ1dHRvbiB7XG4gIHdpZHRoOiAxMDAlO1xuICAtLWJveC1zaGFkb3c6IDAgMCAwIDA7XG4gIG91dGxpbmU6IDA7XG59XG5cbi5idXR0b25BcmVhIHN2ZyB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbmJ1dHRvbi5uYXZiYXItdG9nZ2xlciB7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogNTVweCAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICBib3JkZXI6ICM5MmYzMjggc29saWQgMHB4O1xuICBvdXRsaW5lOiBub25lO1xuICAtLWJveC1zaGFkb3c6IDAgMCAwIDA7XG4gIG91dGxpbmU6IDA7XG59XG5cbi5ib3hDb250ZXVkbyAuYnRuIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5tYXBhIGlmcmFtZSB7XG4gIGJvcmRlcjogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwdmg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdG9wOiAwcHg7XG4gIHotaW5kZXg6IC0xO1xufVxuXG4uc2F1ZGFjYW8ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5ob21lUGFzc2FnZWlybyAuYm94Q29udGV1ZG8ge1xuICBwb3NpdGlvbjogYWJzb2x1dGUgIWltcG9ydGFudDtcbiAgdG9wOiAzMHB4O1xuICB3aWR0aDogY2FsYygxMDAlIC0gMHB4KTtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHBhZGRpbmc6IDE1cHggNXB4O1xufVxuXG4uaG9tZVBhc3NhZ2Vpcm8gLmJ1dHRvbkFyZWEgYnV0dG9uLnJvdW5kZWQtcGlsbCB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbn1cblxuYnV0dG9uLmVzY29saGVyRGVzdGlubyB7XG4gIGJhY2tncm91bmQ6ICMwMDFjNGU7XG4gIGJvcmRlci1jb2xvcjogIzAwMWM0ZTtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDYwcHgpICFpbXBvcnRhbnQ7XG4gIGZsb2F0OiBsZWZ0O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIHdpZHRoOiA1MHB4O1xuICBoZWlnaHQ6IDUwcHg7XG4gIGNvbG9yOiAjZmZmO1xuICAtLWJveC1zaGFkb3c6IDAgMCAwIDA7XG4gIG91dGxpbmU6IDA7XG59XG5cbmJ1dHRvbi5wZWdhckNhcnJvIHtcbiAgYmFja2dyb3VuZDogIzAwMWM0ZTtcbiAgYm9yZGVyLWNvbG9yOiAjMDAxYzRlO1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIGhlaWdodDogNTBweDtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xuICAtLW91dGxpbmU6IDA7XG59XG5cbmJ1dHRvbi5lbWJhcnF1ZVJhcGlkbyB7XG4gIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZzogMHB4IDEycHg7XG4gIHdpZHRoOiA0NXB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogNDVweDtcbiAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xuICBvdXRsaW5lOiAwO1xufVxuXG5idXR0b24uZW1iYXJxdWVSYXBpZG8gc3ZnIHtcbiAgd2lkdGg6IDE5cHg7XG4gIGNvbG9yOiAjNDE2MzEzO1xuICAtLWJveC1zaGFkb3c6IDAgMCAwIDA7XG4gIG91dGxpbmU6IDA7XG59XG5cbmJ1dHRvbi5idG4uYnRuLXByaW1hcnkuYnRuLWxnLnJvdW5kZWQtcGlsbC5lbWJhcnF1ZVJhcGlkbyB7XG4gIGJhY2tncm91bmQ6ICNiMWZmNDk7XG4gIGJvcmRlcjogbm9uZTtcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xuICBvdXRsaW5lOiAwO1xufVxuXG4uZm9ybURlc3Rpbm8ge1xuICBib3JkZXI6IG5vbmU7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xuICBvdXRsaW5lOiAwO1xufVxuXG4uZW5kRGVzdGlub1JlY2VudGUge1xuICBmb250LXNpemU6IDExcHg7XG4gIGJhY2tncm91bmQ6ICNlZWU7XG4gIHBhZGRpbmc6IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xuICBvdXRsaW5lOiAwO1xufVxuXG4uZW5kRGVzdGlub1JlY2VudGUgYiB7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGNvbG9yOiBkYXJrZ3JheTtcbn1cblxuLnRpdHVsb0Rlc3Rpbm9SZWNlbnRlIHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5uYXZiYXItY29sbGFwc2Uge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHRvcDogMHB4O1xuICBsZWZ0OiAwO1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE1cHg7XG4gIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICB3aWR0aDogNzUlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQ6ICMwMDFjNGU7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIHotaW5kZXg6IDk5OTk7XG4gIGJveC1zaGFkb3c6IDBweCAwcHggOTBweCBibGFjaztcbn1cblxuLm5hdmJhci1jb2xsYXBzZSBhLm5hdi1saW5rIHtcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDE5cHg7XG59XG5cbi5uYXZiYXItZGFyayAubmF2YmFyLXRvZ2dsZXIge1xuICBiYWNrZ3JvdW5kOiAjMDAxYzRlO1xufVxuXG4ubmF2YmFyLWRhcmsgc3Bhbi5uYXZiYXItdG9nZ2xlci1pY29uIHtcbiAgcGFkZGluZzogMTVweDtcbn1cblxuLm5hdmJhci1jb2xsYXBzZS5jb2xsYXBzaW5nIHtcbiAgbGVmdDogLTc1JTtcbiAgdHJhbnNpdGlvbjogaGVpZ2h0IDBzIGVhc2U7XG59XG5cbi5uYXZiYXItY29sbGFwc2Uuc2hvdyB7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbGVmdDogMDtcbiAgdHJhbnNpdGlvbjogbGVmdCAzMDBtcyBlYXNlLWluLW91dDtcbn1cblxuLm5hdmJhci10b2dnbGVyLmNvbGxhcHNlZCB+IC5uYXZiYXItY29sbGFwc2Uge1xuICB0cmFuc2l0aW9uOiBsZWZ0IDUwMG1zIGVhc2UtaW4tb3V0O1xufVxuXG4ubmF2YmFyLWxpZ2h0IC5uYXZiYXItdG9nZ2xlci1pY29uIHtcbiAgcGFkZGluZzogMTVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG59XG5cbi5hbmltYWNhbyB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICBtYXJnaW4tdG9wOiA1MCU7XG4gIHdpZHRoOiA1MHB4O1xuICBoZWlnaHQ6IDUwcHg7XG4gIGJvcmRlcjogN3B4IHNvbGlkICNhOWE5YTk7XG4gIGJvcmRlci10b3AtY29sb3I6ICMwMDFjNGU7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIGFuaW1hdGlvbjogaXMtcm90YXRpbmcgMS41cyBpbmZpbml0ZTtcbn1cblxuLnBvc2l0aW9uIHtcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDVweDtcbiAgbGVmdDogMTBweDtcbn1cblxuLm5hdmJhci1jb2xsYXBzZSB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgdG9wOiAwcHg7XG4gIGxlZnQ6IDA7XG4gIHBhZGRpbmctbGVmdDogMTVweDtcbiAgcGFkZGluZy1yaWdodDogMTVweDtcbiAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gIHdpZHRoOiA3NSU7XG4gIGhlaWdodDogMTAwJTtcbiAgYmFja2dyb3VuZDogIzAwMWM0ZTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgei1pbmRleDogOTk5OTtcbiAgYm94LXNoYWRvdzogMHB4IDBweCA5MHB4IGJsYWNrO1xufVxuXG4ubmF2YmFyLWNvbGxhcHNlIGEubmF2LWxpbmsge1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMTlweDtcbn1cblxuYnV0dG9uLmVzY29saGVyRGVzdGlubyB7XG4gIGJhY2tncm91bmQ6ICMwMDFjNGU7XG4gIGJvcmRlci1jb2xvcjogIzAwMWM0ZTtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDYwcHgpICFpbXBvcnRhbnQ7XG4gIGZsb2F0OiBsZWZ0O1xuICAtLWJveC1zaGFkb3c6IDAgMCAwIDA7XG4gIG91dGxpbmU6IDA7XG59XG5cbi5mb3JtRGVzdGlubyB7XG4gIGJvcmRlcjogbm9uZTtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuXG4uZW5kRGVzdGlub1JlY2VudGUge1xuICBmb250LXNpemU6IDExcHg7XG4gIGJhY2tncm91bmQ6ICNlZWU7XG4gIHBhZGRpbmc6IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cblxuLmVuZERlc3Rpbm9SZWNlbnRlIGIge1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBjb2xvcjogZGFya2dyYXk7XG59XG5cbi50aXR1bG9EZXN0aW5vUmVjZW50ZSB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG5idXR0b24uYnRuLmJ0bi1wcmltYXJ5LmJ0bi1sZy5yb3VuZGVkLXBpbGwuZXNjb2xoZXJEZXN0aW5vIHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICAtLWJveC1zaGFkb3c6IDAgMCAwIDA7XG4gIG91dGxpbmU6IDA7XG59XG5cbi5uYXZiYXItZGFyayAubmF2YmFyLXRvZ2dsZXIge1xuICBiYWNrZ3JvdW5kOiAjMDAxYzRlO1xufVxuXG4ubmF2YmFyLWRhcmsgc3Bhbi5uYXZiYXItdG9nZ2xlci1pY29uIHtcbiAgcGFkZGluZzogMTVweDtcbn1cblxuLm5hdmJhci1jb2xsYXBzZS5jb2xsYXBzaW5nIHtcbiAgbGVmdDogLTc1JTtcbiAgdHJhbnNpdGlvbjogaGVpZ2h0IDBzIGVhc2U7XG59XG5cbi5uYXZiYXItY29sbGFwc2Uuc2hvdyB7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbGVmdDogMDtcbiAgdHJhbnNpdGlvbjogbGVmdCAzMDBtcyBlYXNlLWluLW91dDtcbn1cblxuLm5hdmJhci10b2dnbGVyLmNvbGxhcHNlZCB+IC5uYXZiYXItY29sbGFwc2Uge1xuICB0cmFuc2l0aW9uOiBsZWZ0IDUwMG1zIGVhc2UtaW4tb3V0O1xufVxuXG4uZWxzZSB7XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xuICBkaXNwbGF5OiBibG9jaztcbiAgcGFkZGluZy10b3A6IDIycHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgbWFyZ2luOiAyMHB4IDA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzEyM2I3ZDtcbn1cblxuLmVsc2V1YWlMZXZhIHtcbiAgZm9udC1zaXplOiAxLjJyZW07XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwYWRkaW5nLXRvcDogMnB4O1xuICBwYWRkaW5nLWxlZnQ6IDEwMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBtYXJnaW46IDE1cHggMDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjMTIzYjdkO1xufVxuXG5zcGFuIHtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBkaXNwbGF5OiBibG9jaztcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIHBhZGRpbmctdG9wOiAzcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzEyM2I3ZDtcbn1cblxuaDEge1xuICBmb250LXNpemU6IDEuNXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBtYXJnaW46IDEwcHggMDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjMTIzYjdkO1xufVxuXG4ubmF2YmFyLmJnLWRhcmsge1xuICBiYWNrZ3JvdW5kOiAjMDAxYzRlICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1tZW51LWJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBib3JkZXItcmFkaXVzOiAxMDBweDtcbiAgd2lkdGg6IDUwcHg7XG4gIGhlaWdodDogNTBweDtcbn1cblxuLm5hdmJhci1icmFuZCB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcGFkZGluZy10b3A6IDAuMzEyNXJlbTtcbiAgcGFkZGluZy1ib3R0b206IDAuMzEyNXJlbTtcbiAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xuICBmb250LXNpemU6IDEuMjVyZW07XG4gIGxpbmUtaGVpZ2h0OiBpbmhlcml0O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xufVxuXG4ubmF2YmFyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LXdyYXA6IHdyYXA7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgcGFkZGluZzogMC41cmVtIDFyZW07XG59XG5cbi5jb21lY2FyIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB3aWR0aDogMTAwJTtcbiAgYm90dG9tOiAxMCU7XG4gIGJhY2tncm91bmQ6ICMwMDFjNGU7XG4gIHBhZGRpbmc6IDIxcHg7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXNpemU6IDIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDQwcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/home/home.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _pagamentos_pagamentos_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../pagamentos/pagamentos.page */ "./src/app/pages/pagamentos/pagamentos.page.ts");
/* harmony import */ var src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/core/user/user.service */ "./src/app/core/user/user.service.ts");
/* harmony import */ var src_app_services_viagem_viagemService__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/viagem/viagemService */ "./src/app/services/viagem/viagemService.ts");
/* harmony import */ var _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/call-number/ngx */ "./node_modules/@ionic-native/call-number/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var rx_polling__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rx-polling */ "./node_modules/rx-polling/lib/index.js");
/* harmony import */ var rx_polling__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(rx_polling__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _ionic_native_native_audio_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/native-audio/ngx */ "./node_modules/@ionic-native/native-audio/__ivy_ngcc__/ngx/index.js");












let HomePage = class HomePage {
    constructor(platform, loadCtrl, ngZone, navCtrl, menuCtrl, popoverCtrl, alertCtrl, modalCtrl, toastCtrl, userService, loadingController, router, viagemService, callNumber, nativeAudio) {
        this.platform = platform;
        this.loadCtrl = loadCtrl;
        this.ngZone = ngZone;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.userService = userService;
        this.loadingController = loadingController;
        this.router = router;
        this.viagemService = viagemService;
        this.callNumber = callNumber;
        this.nativeAudio = nativeAudio;
        this.search = '';
        this.searchOrigem = '';
        this.googleAutocomplete = new google.maps.places.AutocompleteService();
        this.searchResults = new Array();
        this.searchResultsOrigem = new Array();
        this.googleDirectionsService = new google.maps.DirectionsService();
        this.matrix = new google.maps.DistanceMatrixService();
        this.valor = new Array();
        this.valores = [];
        this.ringtones = [];
        this.user$ = userService.getUser();
        this.user$.subscribe(usuario => {
            this.usuarioLogado = usuario;
            this.user_id = this.usuarioLogado.id;
        });
        const request$ = this.viagemService.allViagem();
        const options = { interval: 5000 };
        const r = this.viagemService.findMotorista(this.user_id);
        rx_polling__WEBPACK_IMPORTED_MODULE_9___default()(r, options).subscribe((usuario) => {
            this.u = usuario;
            if (this.u.logado == 'TRUE') {
                document.getElementById("comecar").style.display = "none";
                document.getElementById("encerrar").style.display = "block";
                rx_polling__WEBPACK_IMPORTED_MODULE_9___default()(request$, options)
                    .subscribe((viagem) => {
                    this.viagem = viagem;
                    for (var v of this.viagem) {
                        this.nativeAudio.preloadSimple('papaleguas', 'assets/ringtones/papaleguas.mp3');
                        this.nativeAudio.play('papaleguas').then(() => {
                            console.log('som');
                        });
                        var a = v.origem.split(',');
                        v.origem = a[0] + ',' + a[1];
                        var b = v.destino.split(',');
                        v.destino = b[0] + ',' + b[1] + ' - ' + b[2];
                        if (v.forma_pagamento == undefined || v.forma_pagamento == '' || v.forma_pagamento == 'undefined') {
                            v.forma_pagamento = 'dinheiro';
                        }
                    }
                }, (error) => {
                    console.error(error);
                });
            }
            else {
                console.log('saiu');
                document.getElementById("comecar").style.display = "block";
                document.getElementById("encerrar").style.display = "none";
            }
        });
    }
    ionViewWillEnter() {
        this.menuCtrl.enable(true);
        this.user$ = this.userService.getUser();
        this.user$.subscribe(usuario => {
            this.usuarioLogado = usuario;
            this.user_id = this.usuarioLogado.id;
        });
    }
    ngOnInit() {
        this.mapElement = this.mapElement.nativeElement;
        this.mapElement.style.width = this.platform.width() + 'px';
        this.mapElement.style.height = this.platform.height() + 'px';
        this.loadMap();
    }
    loadMap() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loading = yield this.loadCtrl.create({ message: 'Por favor aguarde ...' });
            yield this.loading.present();
            _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["Environment"].setEnv({
                'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyC0inbEk4sOpHIT8lUyG3PI4XziAvl3CI0',
                'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyC0inbEk4sOpHIT8lUyG3PI4XziAvl3CI0'
            });
            const mapOptions = {
                controls: {
                    zoom: false
                }
            };
            this.map = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMaps"].create(this.mapElement, mapOptions);
            try {
                yield this.map.one(_ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsEvent"].MAP_READY);
                this.addOrigenMarker();
            }
            catch (error) {
                console.error(error);
            }
        });
    }
    addOrigenMarker() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                const myLocation = yield this.map.getMyLocation();
                yield this.map.moveCamera({
                    target: myLocation.latLng,
                    zoom: 18
                });
                this.originMarker = this.map.addMarkerSync({
                    title: 'Origem',
                    icon: '#000',
                    animation: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsAnimation"].DROP,
                    position: myLocation.latLng
                });
            }
            catch (error) {
                console.error(error);
            }
            finally {
                this.loading.dismiss();
            }
        });
    }
    searchChangedOrigem() {
        if (!this.searchOrigem.trim().length)
            return;
        this.googleAutocomplete.getPlacePredictions({ input: this.searchOrigem }, predictions => {
            this.ngZone.run(() => {
                this.searchResultsOrigem = predictions;
            });
        });
    }
    searchChanged() {
        if (!this.search.trim().length)
            return;
        this.googleAutocomplete.getPlacePredictions({ input: this.search }, predictions => {
            this.ngZone.run(() => {
                this.searchResults = predictions;
            });
        });
    }
    calcRouteOrigem(item) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.origem = item;
            console.log(this.origem);
            const info = yield _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["Geocoder"].geocode({ address: this.origem.description });
            this.originMarkerDiferente = this.map.addMarkerSync({
                title: this.origem.description,
                icon: '#000',
                animation: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsAnimation"].DROP,
                position: info[0].position
            });
        });
    }
    calcRoute(item) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.search = '';
            this.destination = item;
            const info = yield _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["Geocoder"].geocode({ address: this.destination.description });
            let markerDestination = this.map.addMarkerSync({
                title: this.destination.description,
                icon: '#000',
                animation: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__["GoogleMapsAnimation"].DROP,
                position: info[0].position
            });
            console.log('distancia origem: ' + this.originMarker.getPosition().lat + " long :  " + this.originMarker.getPosition().lng);
            console.log('distancia destino: ' + markerDestination.getPosition());
            if (this.originMarkerDiferente == undefined) {
                this.originMarkerDiferente = this.originMarker;
            }
            this.googleDirectionsService.route({
                origin: this.originMarkerDiferente.getPosition(),
                destination: markerDestination.getPosition(),
                travelMode: 'DRIVING'
            }, (results) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const points = new Array();
                const routes = results.routes[0].overview_path;
                for (let i = 0; i < routes.length; i++) {
                    points[i] = {
                        lat: routes[i].lat(),
                        lng: routes[i].lng()
                    };
                }
                yield this.map.addPolyline({
                    points: points,
                    color: 'red',
                    width: 3
                });
                var origin1 = new google.maps.LatLng(this.originMarkerDiferente.getPosition().lat, this.originMarkerDiferente.getPosition().lng);
                var destinationB = new google.maps.LatLng(markerDestination.getPosition().lat, markerDestination.getPosition().lng);
                console.log(origin1);
                console.log(destinationB);
                this.matrix.getDistanceMatrix({
                    origins: [origin1],
                    destinations: [destinationB],
                    travelMode: google.maps.TravelMode.DRIVING
                }, callback);
                function callback(response, status) {
                    let travelDetailsObject;
                    if (status !== "OK") {
                        alert("Error was: " + status);
                    }
                    else {
                        var origins = response.originAddresses;
                        var destinations = response.destinationAddresses;
                        console.log(origins);
                        console.log(destinations);
                        for (var i = 0; i < origins.length; i++) {
                            var results = response.rows[i].elements;
                            for (var j = 0; j < results.length; j++) {
                                var element = results[j];
                                var distance = element.distance.text;
                                var duration = element.duration.text;
                                var from = origins[i];
                                var to = destinations[j];
                                travelDetailsObject = {
                                    distance: distance,
                                    duration: duration
                                };
                            }
                        }
                        this.travelDetailsObject = travelDetailsObject;
                        this.valor = this.travelDetailsObject;
                        this.corrida = 'distancia : ' + this.travelDetailsObject.distance + ' no tempo de : ' + this.travelDetailsObject.duration;
                    }
                }
                this.map.moveCamera({ target: points });
                this.map.panBy(0, 100);
            }));
        });
    }
    ligar(n) {
        this.callNumber.callNumber(n, true)
            .then(() => console.log('Launched dialer!'))
            .catch(() => console.log('Error launching dialer'));
    }
    comecar() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const myLocation = yield this.map.getMyLocation();
            this.viagemService.updateLogado('' + this.user_id).subscribe();
            this.viagemService.localMotorista('' + this.user_id, myLocation.latLng.lat, myLocation.latLng.lng).subscribe();
        });
    }
    encerrar() {
        location.reload();
        this.viagemService.updateEncerrado('' + this.user_id).subscribe();
    }
    aceitar(id_usuario, id_viagem) {
        this.viagemService.aceitar(id_usuario, this.user_id, id_viagem).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.router.navigate(['corrida/' + id_viagem + '']);
            const toast = yield (yield this.toastCtrl.create({
                message: 'Iniciando a viagem',
                duration: 4000, position: 'middle',
                color: 'success'
            })).present();
        }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(err);
            location.reload();
            const toast = yield (yield this.toastCtrl.create({
                message: 'Viagem aceita por outro motorista!',
                duration: 4000, position: 'middle',
                color: 'danger'
            })).present();
        }));
    }
    back() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                yield this.map.clear();
                this.destination = null;
                this.origem = null;
                this.searchOrigem = null;
                document.getElementById("collapseExample").style.display == 'none';
                this.addOrigenMarker();
            }
            catch (error) {
                console.error(error);
            }
        });
    }
    toggleTeste() {
        if (document.getElementById("collapseExample").style.display == 'none') {
            document.getElementById("collapseExample").style.display = "block";
        }
        else {
            document.getElementById("collapseExample").style.display = "none";
        }
    }
    toggleTeste1() {
        if (document.getElementById("collapseExample1").style.display == 'none') {
            document.getElementById("collapseExample1").style.display = "block";
        }
        else {
            document.getElementById("collapseExample1").style.display = "none";
        }
    }
    toggleCarregar() {
        if (document.getElementById("carregando").style.display == 'none') {
            document.getElementById("carregando").style.display = "block";
        }
        else {
            document.getElementById("carregando").style.display = "none";
        }
    }
    carregandoAlerta() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                header: '',
                cssClass: 'carregar',
                subHeader: 'Aguardando motorista',
                message: `<p></p><ion-spinner name="bubbles"></ion-spinner>`,
                buttons: ['Cancelar corrida']
            });
            yield alert.present();
        });
    }
    embarqueRapido() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                header: '',
                cssClass: '',
                subHeader: '',
                message: 'Embarque rápido : <br> • Pegue um UaiLeva <br> • Escaneie o QR Code <br> • Inicie a corrida <br> Registrar o Cartão',
                buttons: ['X']
            });
            yield alert.present();
        });
    }
    logout() {
        // this.userService.logout();
        this.router.navigate(['']);
    }
    showModalPagamentos() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _pagamentos_pagamentos_page__WEBPACK_IMPORTED_MODULE_5__["PagamentosPage"]
            });
            modal.present();
        });
    }
};
HomePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_services_viagem_viagemService__WEBPACK_IMPORTED_MODULE_7__["ViagemService"] },
    { type: _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_8__["CallNumber"] },
    { type: _ionic_native_native_audio_ngx__WEBPACK_IMPORTED_MODULE_10__["NativeAudio"] }
];
HomePage.propDecorators = {
    mapElement: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ['map', { static: true },] }]
};
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.page.scss */ "./src/app/pages/home/home.page.scss")).default]
    })
], HomePage);



/***/ })

}]);
//# sourceMappingURL=pages-home-home-module-es2015.js.map