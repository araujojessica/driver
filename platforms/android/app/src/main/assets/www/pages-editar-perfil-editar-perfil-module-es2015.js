(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-editar-perfil-editar-perfil-module"],{

/***/ "./src/app/pages/editar-perfil/editar-perfil-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/editar-perfil/editar-perfil-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: EditarPerfilPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarPerfilPageRoutingModule", function() { return EditarPerfilPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _editar_perfil_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./editar-perfil.page */ "./src/app/pages/editar-perfil/editar-perfil.page.ts");




const routes = [
    {
        path: '',
        component: _editar_perfil_page__WEBPACK_IMPORTED_MODULE_3__["EditarPerfilPage"]
    }
];
let EditarPerfilPageRoutingModule = class EditarPerfilPageRoutingModule {
};
EditarPerfilPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EditarPerfilPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/editar-perfil/editar-perfil.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/editar-perfil/editar-perfil.module.ts ***!
  \*************************************************************/
/*! exports provided: EditarPerfilPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarPerfilPageModule", function() { return EditarPerfilPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _editar_perfil_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./editar-perfil-routing.module */ "./src/app/pages/editar-perfil/editar-perfil-routing.module.ts");
/* harmony import */ var _editar_perfil_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./editar-perfil.page */ "./src/app/pages/editar-perfil/editar-perfil.page.ts");







let EditarPerfilPageModule = class EditarPerfilPageModule {
};
EditarPerfilPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _editar_perfil_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditarPerfilPageRoutingModule"]
        ],
        declarations: [_editar_perfil_page__WEBPACK_IMPORTED_MODULE_6__["EditarPerfilPage"]],
        exports: [_editar_perfil_page__WEBPACK_IMPORTED_MODULE_6__["EditarPerfilPage"]]
    })
], EditarPerfilPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-editar-perfil-editar-perfil-module-es2015.js.map