(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-foto-perfil-foto-perfil-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/foto-perfil/foto-perfil.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/foto-perfil/foto-perfil.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button text=\"\" color=\"light\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <h1>Tire sua foto de perfil</h1>\r\n\r\n  <div>\r\n    <p class=\"declaracao\">\r\n      Observação: só é possível alterar a foto de perfil depois de enviá-la após solicitação no aplicativo.\r\n      Antes de enviar a foto, verifique se te representa com precisão.\r\n    </p>\r\n\r\n    <p class=\"termo\">\r\n      1. Mostre o rosto todo e a parte de cima dos ombros.<br>\r\n      2. Não use óculos escuros nem chapéu.<br>\r\n      3. Tire a foto em um local bem iluminado.<br>\r\n    </p>\r\n\r\n  </div>\r\n\r\n  <img src=\"/assets/img/teste-avatar.png\"><br>\r\n  <br><br>\r\n  <input id='selecao-arquivo' type=\"file\" (change)= \"file = $event.target.files[0]\" accept=\"image/*\"><br>\r\n  <ion-button class=\"botao\" (click)=\"salvarFoto();\">Enviar foto</ion-button>\r\n\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/foto-perfil/foto-perfil-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/foto-perfil/foto-perfil-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: FotoPerfilPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FotoPerfilPageRoutingModule", function() { return FotoPerfilPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _foto_perfil_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./foto-perfil.page */ "./src/app/pages/foto-perfil/foto-perfil.page.ts");




const routes = [
    {
        path: '',
        component: _foto_perfil_page__WEBPACK_IMPORTED_MODULE_3__["FotoPerfilPage"]
    }
];
let FotoPerfilPageRoutingModule = class FotoPerfilPageRoutingModule {
};
FotoPerfilPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], FotoPerfilPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/foto-perfil/foto-perfil.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/foto-perfil/foto-perfil.module.ts ***!
  \*********************************************************/
/*! exports provided: FotoPerfilPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FotoPerfilPageModule", function() { return FotoPerfilPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _foto_perfil_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./foto-perfil-routing.module */ "./src/app/pages/foto-perfil/foto-perfil-routing.module.ts");
/* harmony import */ var _foto_perfil_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./foto-perfil.page */ "./src/app/pages/foto-perfil/foto-perfil.page.ts");







let FotoPerfilPageModule = class FotoPerfilPageModule {
};
FotoPerfilPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _foto_perfil_routing_module__WEBPACK_IMPORTED_MODULE_5__["FotoPerfilPageRoutingModule"]
        ],
        declarations: [_foto_perfil_page__WEBPACK_IMPORTED_MODULE_6__["FotoPerfilPage"]]
    })
], FotoPerfilPageModule);



/***/ }),

/***/ "./src/app/pages/foto-perfil/foto-perfil.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/foto-perfil/foto-perfil.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: var(--ion-color-light);\n  --background: url(\"/assets/img/bg-uai-leva2.png\") #eaeaea no-repeat bottom 75px left 45px;\n}\n\nion-toolbar {\n  --background: #001c4e !important;\n}\n\nion-header {\n  --background: #001c4e !important;\n}\n\nion-item {\n  --ion-item-background: transparent;\n  --border-style: var(--border-style);\n  --border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n\nion-icon {\n  color: gray;\n}\n\n/*label {\n    background: #b1ff49;  //#001c4e;\n    color: #001c4e; //white;\n    font-weight: bold;\n    display: block;\n    height: 48px;\n    width: 95%;\n    margin: auto;\n    border-radius: 50px;\n    text-align: center;\n    padding: 16px;\n    --box-shadow: 0 0 0 0; \n    outline: 0;\n    text-transform: uppercase;\n    font-size: .9em;\n    cursor: pointer;\n}*/\n\nimg {\n  height: 200px;\n  width: 200px;\n  display: block;\n  margin: auto;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  margin: 15px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n\n.declaracao {\n  font-size: 1.1rem;\n  padding: 10px;\n  color: #123b7d;\n  text-align: justify;\n}\n\n.termo {\n  padding: 10px;\n  padding-top: 0px;\n  color: black;\n  margin-top: -20px;\n  text-align: justify;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZm90by1wZXJmaWwvZm90by1wZXJmaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksb0NBQUE7RUFDQSx5RkFBQTtBQUNKOztBQUVBO0VBQ0ksZ0NBQUE7QUFDSjs7QUFFQTtFQUNJLGdDQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQ0FBQTtFQUNBLG1DQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7QUFDSjs7QUFHQTs7Ozs7Ozs7Ozs7Ozs7OztFQUFBOztBQWtCQTtFQUNJLGFBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtFQUNBLFVBQUE7QUFESjs7QUFJQTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFFQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBRko7O0FBS0E7RUFDSSxpQkFBQTtFQUVBLGFBQUE7RUFFQSxjQUFBO0VBQ0EsbUJBQUE7QUFKSjs7QUFPQTtFQUNJLGFBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FBSkoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9mb3RvLXBlcmZpbC9mb3RvLXBlcmZpbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcbiAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1nL2JnLXVhaS1sZXZhMi5wbmdcIikgI2VhZWFlYSBuby1yZXBlYXQgYm90dG9tIDc1cHggbGVmdCA0NXB4O1xyXG59XHJcblxyXG5pb24tdG9vbGJhciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGUgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWhlYWRlciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGUgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWl0ZW0ge1xyXG4gICAgLS1pb24taXRlbS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIC0tYm9yZGVyLXN0eWxlOiB2YXIoLS1ib3JkZXItc3R5bGUpO1xyXG4gICAgLS1ib3JkZXI6IDAgbm9uZTsgXHJcbiAgICBib3gtc2hhZG93OiAwIDAgMCAwOyBcclxuICAgIG91dGxpbmU6IDA7XHJcbn1cclxuXHJcbmlvbi1pY29uIHtcclxuICAgIGNvbG9yOiBncmF5O1xyXG59XHJcblxyXG5cclxuLypsYWJlbCB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjYjFmZjQ5OyAgLy8jMDAxYzRlO1xyXG4gICAgY29sb3I6ICMwMDFjNGU7IC8vd2hpdGU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgaGVpZ2h0OiA0OHB4O1xyXG4gICAgd2lkdGg6IDk1JTtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBwYWRkaW5nOiAxNnB4O1xyXG4gICAgLS1ib3gtc2hhZG93OiAwIDAgMCAwOyBcclxuICAgIG91dGxpbmU6IDA7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgZm9udC1zaXplOiAuOWVtO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59Ki9cclxuXHJcbmltZyB7XHJcbiAgICBoZWlnaHQ6IDIwMHB4O1xyXG4gICAgd2lkdGg6IDIwMHB4O1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICAtLWJveC1zaGFkb3c6IDAgMCAwIDA7IFxyXG4gICAgb3V0bGluZTogMDtcclxufVxyXG5cclxuaDEge1xyXG4gICAgZm9udC1zaXplOiAxLjVyZW07XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAvL3RleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBtYXJnaW46IDE1cHggMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbn1cclxuXHJcbi5kZWNsYXJhY2FvIHtcclxuICAgIGZvbnQtc2l6ZTogMS4xcmVtO1xyXG4gICAgLy9mb250LXdlaWdodDogYm9sZDtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAvL3BhZGRpbmctYm90dG9tOiAwcHg7XHJcbiAgICBjb2xvcjogIzEyM2I3ZDtcclxuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XHJcbn1cclxuXHJcbi50ZXJtbyB7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgcGFkZGluZy10b3A6IDBweDtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIG1hcmdpbi10b3A6IC0yMHB4O1xyXG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/foto-perfil/foto-perfil.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/foto-perfil/foto-perfil.page.ts ***!
  \*******************************************************/
/*! exports provided: FotoPerfilPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FotoPerfilPage", function() { return FotoPerfilPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_services_user_registerService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/user/registerService */ "./src/app/services/user/registerService.ts");





let FotoPerfilPage = class FotoPerfilPage {
    constructor(route, signupService, toastCtrl, router) {
        this.route = route;
        this.signupService = signupService;
        this.toastCtrl = toastCtrl;
        this.router = router;
    }
    ngOnInit() {
        this.route.params.subscribe(parametros => {
            this.phone = parametros['phone'].replace('-', '').replace(' ', '').replace('(', '').replace(')', '');
        });
    }
    salvarFoto() {
        console.log(this.file);
        let description = `` + this.phone + `/ MOTORISTA / PERFIL`;
        let allowComments = true;
        this.signupService.upload(description, allowComments, this.file).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield (yield this.toastCtrl.create({
                message: 'upload da foto com sucesso!',
                duration: 4000, position: 'top',
                color: 'success'
            })).present();
            this.router.navigate(['/cnh/' + this.phone + '']);
        }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield (yield this.toastCtrl.create({
                message: 'Erro ao fazer upload, por favor verifique sua conexão com a internet!',
                duration: 4000, position: 'top',
                color: 'danger'
            })).present();
        }));
    }
};
FotoPerfilPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_app_services_user_registerService__WEBPACK_IMPORTED_MODULE_4__["RegisterService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
FotoPerfilPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-foto-perfil',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./foto-perfil.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/foto-perfil/foto-perfil.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./foto-perfil.page.scss */ "./src/app/pages/foto-perfil/foto-perfil.page.scss")).default]
    })
], FotoPerfilPage);



/***/ })

}]);
//# sourceMappingURL=pages-foto-perfil-foto-perfil-module-es2015.js.map