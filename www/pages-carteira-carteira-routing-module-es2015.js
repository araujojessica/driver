(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-carteira-carteira-routing-module"],{

/***/ "./src/app/pages/carteira/carteira-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/carteira/carteira-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: CarteiraPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarteiraPageRoutingModule", function() { return CarteiraPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _carteira_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./carteira.page */ "./src/app/pages/carteira/carteira.page.ts");




const routes = [
    {
        path: '',
        component: _carteira_page__WEBPACK_IMPORTED_MODULE_3__["CarteiraPage"]
    }
];
let CarteiraPageRoutingModule = class CarteiraPageRoutingModule {
};
CarteiraPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CarteiraPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-carteira-carteira-routing-module-es2015.js.map