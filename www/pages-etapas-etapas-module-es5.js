(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-etapas-etapas-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/etapas/etapas.page.html":
    /*!*************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/etapas/etapas.page.html ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesEtapasEtapasPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button text=\"\" color=\"light\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <h1>Etapas Obrigatórias</h1>\r\n  <div>\r\n    <p class=\"usuario\">Olá, {{nome}}</p>\r\n    <p class=\"comecar\">Veja aqui o que você precisa fazer para criar sua conta.</p>\r\n  </div>\r\n\r\n  <ion-list>\r\n    <ion-item no-padding (click)=\"toAcordos()\">\r\n      <ion-icon name=\"reader-outline\" slot=\"start\"></ion-icon>\r\n      <ion-label>\r\n        Acordos Legais<br>\r\n        <p>Próxima etapa recomendada</p>\r\n      </ion-label>\r\n      <ion-icon name=\"chevron-forward-outline\" slot=\"end\" size=\"small\" style=\"color: #001c4e;\"></ion-icon>\r\n    </ion-item>\r\n\r\n    <ion-item no-padding (click)=\"toFotoPerfil()\">\r\n      <ion-icon name=\"reader-outline\" slot=\"start\"></ion-icon>\r\n      <ion-label>\r\n        Foto de Perfil<br>\r\n        <p>Tudo pronto para começar</p>\r\n      </ion-label>\r\n      <ion-icon name=\"chevron-forward-outline\" slot=\"end\" size=\"small\" style=\"color: #001c4e;\"></ion-icon>\r\n    </ion-item>\r\n\r\n    <ion-item no-padding (click)=\"toCNH()\">\r\n      <ion-icon name=\"reader-outline\" slot=\"start\"></ion-icon>\r\n      <ion-label>\r\n        Carteira Nacional de Habilitação <br> com EAR - CNH <br>\r\n        <p>Tudo pronto para começar</p>\r\n      </ion-label>\r\n      <ion-icon name=\"chevron-forward-outline\" slot=\"end\" size=\"small\" style=\"color: #001c4e;\"></ion-icon>\r\n    </ion-item>\r\n\r\n    <ion-item no-padding (click)=\"toCRLV()\">\r\n      <ion-icon name=\"reader-outline\" slot=\"start\"></ion-icon>\r\n      <ion-label>\r\n        Certificado de Registro e<br>Licenciamento de Veículo - CRLV<br>\r\n        <p>Tudo pronto para começar</p>\r\n      </ion-label>\r\n      <ion-icon name=\"chevron-forward-outline\" slot=\"end\" size=\"small\" style=\"color: #001c4e;\"></ion-icon>\r\n    </ion-item>\r\n  </ion-list>\r\n  <ion-button class=\"botao\" (click)=\"login();\">Etapas concluídas</ion-button>\r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "./src/app/pages/etapas/etapas-routing.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/pages/etapas/etapas-routing.module.ts ***!
      \*******************************************************/

    /*! exports provided: EtapasPageRoutingModule */

    /***/
    function srcAppPagesEtapasEtapasRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EtapasPageRoutingModule", function () {
        return EtapasPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _etapas_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./etapas.page */
      "./src/app/pages/etapas/etapas.page.ts");

      var routes = [{
        path: '',
        component: _etapas_page__WEBPACK_IMPORTED_MODULE_3__["EtapasPage"]
      }];

      var EtapasPageRoutingModule = function EtapasPageRoutingModule() {
        _classCallCheck(this, EtapasPageRoutingModule);
      };

      EtapasPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], EtapasPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/etapas/etapas.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/pages/etapas/etapas.module.ts ***!
      \***********************************************/

    /*! exports provided: EtapasPageModule */

    /***/
    function srcAppPagesEtapasEtapasModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EtapasPageModule", function () {
        return EtapasPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _etapas_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./etapas-routing.module */
      "./src/app/pages/etapas/etapas-routing.module.ts");
      /* harmony import */


      var _etapas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./etapas.page */
      "./src/app/pages/etapas/etapas.page.ts");

      var EtapasPageModule = function EtapasPageModule() {
        _classCallCheck(this, EtapasPageModule);
      };

      EtapasPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _etapas_routing_module__WEBPACK_IMPORTED_MODULE_5__["EtapasPageRoutingModule"]],
        declarations: [_etapas_page__WEBPACK_IMPORTED_MODULE_6__["EtapasPage"]]
      })], EtapasPageModule);
      /***/
    },

    /***/
    "./src/app/pages/etapas/etapas.page.scss":
    /*!***********************************************!*\
      !*** ./src/app/pages/etapas/etapas.page.scss ***!
      \***********************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesEtapasEtapasPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --background: var(--ion-color-light);\n  --background: url(\"/assets/img/bg-uai-leva2.png\") #eaeaea no-repeat bottom 75px left 45px;\n}\n\nion-toolbar {\n  --background: #001c4e !important;\n}\n\nion-header {\n  --background: #001c4e !important;\n}\n\nion-list {\n  background: transparent;\n  padding: 10px;\n}\n\nion-label {\n  color: #001c4e;\n}\n\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 15px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n\n.usuario {\n  font-size: 1.4rem;\n  font-weight: bold;\n  padding: 10px;\n  padding-bottom: 0px;\n  color: #123b7d;\n}\n\n.comecar {\n  padding: 10px;\n  padding-top: 0px;\n  color: #123b7d;\n  margin-top: -20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZXRhcGFzL2V0YXBhcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxvQ0FBQTtFQUNBLHlGQUFBO0FBQ0o7O0FBRUE7RUFDSSxnQ0FBQTtBQUNKOztBQUVBO0VBQ0ksZ0NBQUE7QUFDSjs7QUFFQTtFQUNJLHVCQUFBO0VBQ0EsYUFBQTtBQUNKOztBQUVBO0VBQ0ksY0FBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUFDSjs7QUFFQTtFQUNJLGFBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZXRhcGFzL2V0YXBhcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcbiAgICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1nL2JnLXVhaS1sZXZhMi5wbmdcIikgI2VhZWFlYSBuby1yZXBlYXQgYm90dG9tIDc1cHggbGVmdCA0NXB4O1xyXG59XHJcblxyXG5pb24tdG9vbGJhciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGUgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWhlYWRlciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGUgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWxpc3Qge1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcblxyXG5pb24tbGFiZWwge1xyXG4gICAgY29sb3I6ICMwMDFjNGU7XHJcbn1cclxuXHJcbmgxIHtcclxuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIG1hcmdpbjogMTVweCAwO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBjb2xvcjogIzEyM2I3ZDtcclxufVxyXG5cclxuLnVzdWFyaW8ge1xyXG4gICAgZm9udC1zaXplOiAxLjRyZW07XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbn1cclxuXHJcbi5jb21lY2FyIHtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbiAgICBtYXJnaW4tdG9wOiAtMjBweDtcclxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/etapas/etapas.page.ts":
    /*!*********************************************!*\
      !*** ./src/app/pages/etapas/etapas.page.ts ***!
      \*********************************************/

    /*! exports provided: EtapasPage */

    /***/
    function srcAppPagesEtapasEtapasPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EtapasPage", function () {
        return EtapasPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var EtapasPage = /*#__PURE__*/function () {
        function EtapasPage(router, route) {
          _classCallCheck(this, EtapasPage);

          this.router = router;
          this.route = route;
        }

        _createClass(EtapasPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.route.params.subscribe(function (parametros) {
              _this.nome = parametros['nome'];
              _this.phone = parametros['phone'];
              console.log('parametro dentro de info : ' + parametros['phone']);
            });
          }
        }, {
          key: "toAcordos",
          value: function toAcordos() {
            this.router.navigate(['/acordos/' + this.phone + '']);
          }
        }, {
          key: "toFotoPerfil",
          value: function toFotoPerfil() {
            this.router.navigate(['/foto-perfil/' + this.phone + '']);
          }
        }, {
          key: "toCNH",
          value: function toCNH() {
            this.router.navigate(['/cnh/' + this.phone + '']);
          }
        }, {
          key: "toCRLV",
          value: function toCRLV() {
            this.router.navigate(['/crlv/' + this.phone + '']);
          }
        }, {
          key: "login",
          value: function login() {
            this.router.navigate(['/']);
          }
        }]);

        return EtapasPage;
      }();

      EtapasPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }];
      };

      EtapasPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-etapas',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./etapas.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/etapas/etapas.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./etapas.page.scss */
        "./src/app/pages/etapas/etapas.page.scss"))["default"]]
      })], EtapasPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-etapas-etapas-module-es5.js.map