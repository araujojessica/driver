import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MeusPagamentosPage } from './meus-pagamentos.page';

describe('MeusPagamentosPage', () => {
  let component: MeusPagamentosPage;
  let fixture: ComponentFixture<MeusPagamentosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeusPagamentosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MeusPagamentosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
