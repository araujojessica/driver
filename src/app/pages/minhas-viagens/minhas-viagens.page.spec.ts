import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MinhasViagensPage } from './minhas-viagens.page';

describe('MinhasViagensPage', () => {
  let component: MinhasViagensPage;
  let fixture: ComponentFixture<MinhasViagensPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinhasViagensPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MinhasViagensPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
