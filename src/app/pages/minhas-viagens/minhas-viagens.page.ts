import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-minhas-viagens',
  templateUrl: './minhas-viagens.page.html',
  styleUrls: ['./minhas-viagens.page.scss'],
})
export class MinhasViagensPage implements OnInit {

  constructor(
    public modalCtrl: ModalController,
  ) { }

  ngOnInit() {
  }

  close(){
    this.modalCtrl.dismiss();
  }
  
}
