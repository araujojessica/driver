import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MinhasViagensPageRoutingModule } from './minhas-viagens-routing.module';

import { MinhasViagensPage } from './minhas-viagens.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MinhasViagensPageRoutingModule
  ],
  declarations: [MinhasViagensPage],
  exports: [MinhasViagensPage]
})
export class MinhasViagensPageModule {}
