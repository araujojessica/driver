import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CNHPageRoutingModule } from './cnh-routing.module';

import { CNHPage } from './cnh.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CNHPageRoutingModule
  ],
  declarations: [CNHPage]
})
export class CNHPageModule {}
