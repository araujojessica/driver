import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { MeusPagamentosPage } from '../meus-pagamentos/meus-pagamentos.page';

@Component({
  selector: 'app-pagamentos',
  templateUrl: './pagamentos.page.html',
  styleUrls: ['./pagamentos.page.scss'],
})
export class PagamentosPage implements OnInit {

  constructor(
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
  }

  close(){
    this.modalCtrl.dismiss();
  }

  async showModalMeusPagamentos(){
    const modal = await this.modalCtrl.create({
      component: MeusPagamentosPage
    });

    return await modal.present();
  }

}
