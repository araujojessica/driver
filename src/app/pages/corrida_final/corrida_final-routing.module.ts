import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CorridaFinalPage } from './corrida_final.page';

const routes: Routes = [
  {
    path: '',
    component: CorridaFinalPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CorridaFinalPageRoutingModule {}
