import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { CorridaFinalPage } from './corrida_final.page';

import { CorridaFinalPageRoutingModule } from './corrida_final-routing.module';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CorridaFinalPageRoutingModule
  ],
  declarations: [CorridaFinalPage],
  entryComponents: []
})
export class CorridaFinalPageModule {}
