import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CorridaFinalPage } from './corrida_final.page';

describe('CorridaFinalPage', () => {
  let component: CorridaFinalPage;
  let fixture: ComponentFixture<CorridaFinalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorridaFinalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CorridaFinalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
