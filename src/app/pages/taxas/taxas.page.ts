import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, ToastController } from '@ionic/angular';
import { TaxaService } from 'src/app/services/taxa/taxa';
import { MeusPagamentosPage } from '../meus-pagamentos/meus-pagamentos.page';

@Component({
  selector: 'app-taxas',
  templateUrl: './taxas.page.html',
  styleUrls: ['./taxas.page.scss'],
})
export class TaxasPage implements OnInit {

  valor_base:string = '';
  valor_km:string = '';
  valor_min:string = '';
  dinamica:string = '';
  taxa: any;
  base:any;
  km:any;
  min:any;
  t: any;

  constructor(
    private modalCtrl: ModalController,
    private taxaService: TaxaService,
    private toastCtrl: ToastController,
    private router: Router
  ) { 

    this.taxaService.taxa().subscribe(taxa => {
      this.taxa = taxa;
      console.log(this.taxa.valor_base)
      this.base = this.taxa.valor_base;
      this.km = this.taxa.valorKM;
      this.min = this.taxa.valorParado;
      this.t = this.taxa.taxa;
    });

  }

  ngOnInit() {
  }

  close(){
    this.modalCtrl.dismiss();
  }

  async showModalMeusPagamentos(){
    const modal = await this.modalCtrl.create({
      component: MeusPagamentosPage
    });

    return await modal.present();
  }

  bandeira(event){
    this.dinamica = event.target.value;
    
  }
  valorBase(event){
    this.valor_base = event.target.value;
  }
  valorKm(event){
    this.valor_km = event.target.value;
  }
  valorMin(event){
    this.valor_min = event.target.value;
  }

  salvar(){

    if(this.valor_base != ''){
      this.taxaService.updatevalorBase(this.valor_base).subscribe( async () => {
        const toast = await (await this.toastCtrl.create({
         message: 'Valor base editado com sucesso!',
         duration: 4000, position: 'top',
         color: 'success'
       })).present();
       this.router.navigate(['/home'])  
     },  
     async err => {
       const toast = await (await this.toastCtrl.create({
         message: 'Erro ao editar valor, por favor verifique sua conexão com a internet!',
         duration: 4000, position: 'top',
         color: 'danger'
       })).present();
     });

   
    }
    if(this.valor_km != ''){
      this.taxaService.updatevalorKm(this.valor_km).subscribe( async () => {
        const toast = await (await this.toastCtrl.create({
         message: 'Valor por km editado com sucesso!',
         duration: 4000, position: 'top',
         color: 'success'
       })).present();
       this.router.navigate(['/home'])  
     },  
     async err => {
       const toast = await (await this.toastCtrl.create({
         message: 'Erro ao editar valor, por favor verifique sua conexão com a internet!',
         duration: 4000, position: 'top',
         color: 'danger'
       })).present();
     });
    }
    if(this.valor_min != ''){
      this.taxaService.updatevalorMin(this.valor_min).subscribe( async () => {
        const toast = await (await this.toastCtrl.create({
         message: 'Valor por min parado editado com sucesso!',
         duration: 4000, position: 'top',
         color: 'success'
       })).present();
       this.router.navigate(['/home'])  
     },  
     async err => {
       const toast = await (await this.toastCtrl.create({
         message: 'Erro ao editar valor, por favor verifique sua conexão com a internet!',
         duration: 4000, position: 'top',
         color: 'danger'
       })).present();
     });
    }

 
  }

  salvarBandeira(){
    if(this.dinamica != ''){
      this.taxaService.updatedinamica(this.dinamica).subscribe( async () => {
        const toast = await (await this.toastCtrl.create({
         message: 'Taxa editado com sucesso!',
         duration: 4000, position: 'top',
         color: 'success'
       })).present();
       this.router.navigate(['/home'])  
     },  
     async err => {
       const toast = await (await this.toastCtrl.create({
         message: 'Erro ao editar taxa, por favor verifique sua conexão com a internet!',
         duration: 4000, position: 'top',
         color: 'danger'
       })).present();
     });
    }
  }

}
