import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TaxasPage } from './taxas.page';

describe('TaxasPage', () => {
  let component: TaxasPage;
  let fixture: ComponentFixture<TaxasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TaxasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
