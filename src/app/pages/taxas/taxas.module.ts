import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PagamentosPageRoutingModule } from './taxas-routing.module';

import { TaxasPage } from './taxas.page';
import { MeusPagamentosPage } from '../meus-pagamentos/meus-pagamentos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PagamentosPageRoutingModule
  ],
  declarations: [TaxasPage],
  exports: [TaxasPage],
  entryComponents: []
})
export class PagamentosPageModule {}
