import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Foto } from 'src/app/interfaces/foto';
import { RegisterService } from 'src/app/services/user/registerService';
@Component({
  selector: 'app-foto-perfil',
  templateUrl: './foto-perfil.page.html',
  styleUrls: ['./foto-perfil.page.scss'],
})
export class FotoPerfilPage implements OnInit {
  
  foto: Foto;
  urlFoto: string;
  file: File;
  phone: string;
  constructor( 
    private route: ActivatedRoute,
    private signupService: RegisterService,
    private toastCtrl: ToastController,
    private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(parametros=> {    
      this.phone = parametros['phone'].replace('-','').replace(' ','').replace('(','').replace(')','');
    });
    
  }

  salvarFoto(){
        console.log(this.file)
        let description = ``+this.phone+`/ MOTORISTA / PERFIL`;
        let allowComments = true;
      this.signupService.upload(description,allowComments, this.file).subscribe(async () => {
          const toast = await (await this.toastCtrl.create({
          message: 'upload da foto com sucesso!',
          duration: 4000, position: 'top',
          color: 'success'
        })).present();  
        this.router.navigate(['/cnh/'+this.phone+'']);
      },  
      async err => {
        const toast = await (await this.toastCtrl.create({
          message: 'Erro ao fazer upload, por favor verifique sua conexão com a internet!',
          duration: 4000, position: 'top',
          color: 'danger'
        })).present();
      });

      
  }

}
