import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CRLVPage } from './crlv.page';

const routes: Routes = [
  {
    path: '',
    component: CRLVPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CRLVPageRoutingModule {}
