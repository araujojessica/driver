import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CRLVPage } from './crlv.page';

describe('CRLVPage', () => {
  let component: CRLVPage;
  let fixture: ComponentFixture<CRLVPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CRLVPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CRLVPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
