import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import polling from 'rx-polling';
import { Observable } from 'rxjs/Rx';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { ViagemService } from 'src/app/services/viagem/viagemService';

@Component({
  selector: 'app-carteira',
  templateUrl: './carteira.page.html',
  styleUrls: ['./carteira.page.scss'],
})
export class CarteiraPage implements OnInit {

  carteira: any;
  user$: Observable<User>;
  usuarioLogado: User;
  totalViagens:string;
  valorTotal: string;
  valorUaileva: string;
  constructor(
    public modalCtrl: ModalController,
    public viagemService: ViagemService,
    private userService: UserService,
  ) { 

    this.user$ = userService.getUser();
    
      this.user$.subscribe( usuario => {
      this.usuarioLogado = usuario;
      
     
    });

    const request$ = this.viagemService.carteiraMotorista(this.usuarioLogado.id);
    const options = { interval: 5000 };
    
    polling(request$, options)
      .subscribe((carteira) => {
        this.carteira = carteira;
        this.totalViagens = this.carteira.total_viagem;
        this.valorTotal = (this.carteira.valor_corrida - this.carteira.valor_corrida * 0.1).toFixed(2);
        this.valorUaileva = (this.carteira.valor_corrida * 0.1).toFixed(2);
        
      }, (error) => {
        console.error(error);
      });


    

  }

  ngOnInit() {
  }

  close(){
    this.modalCtrl.dismiss();
  }

  
}
