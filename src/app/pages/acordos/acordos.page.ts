import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { RegisterService } from 'src/app/services/user/registerService';

@Component({
  selector: 'app-acordos',
  templateUrl: './acordos.page.html',
  styleUrls: ['./acordos.page.scss'],
})
export class AcordosPage implements OnInit {

  checkedButton: boolean = true;
  phone: string;

  constructor(private route: ActivatedRoute,
    private signupService: RegisterService,
    private toastCtrl: ToastController,
    private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(parametros=> {    
      this.phone = parametros['phone'];
    });
  }

  enableBtn(event: { checked: any; }){
      if (event.checked) {
        this.checkedButton = !this.checkedButton;
       
      }

      else{
        this.checkedButton = !this.checkedButton;
      } 
  }
  acordo(){

    this.signupService.acordo(this.phone).subscribe(
        
      async () => {
        
             const toast = await (await this.toastCtrl.create({
                message: 'Usuário cadastrado com sucesso! Agora é só fazer o login!',
                duration: 4000, position: 'top',
                color: 'success'
              })).present();
              this.router.navigate(['/foto-perfil/'+this.phone+'']);
        
      
      },  
      async err => {
        console.log(err)
        const toast = await (await this.toastCtrl.create({
          message: 'Por favor confira se a sua internet está funcionando e tente cadastrar novamente! UaiLevacopia agradece!',
          duration: 4000, position: 'top',
          color: 'danger'
        })).present();
      }
    );
   
    
  }
  
}