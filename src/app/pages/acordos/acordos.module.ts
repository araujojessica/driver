import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AcordosPageRoutingModule } from './acordos-routing.module';

import { AcordosPage } from './acordos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AcordosPageRoutingModule
  ],
  declarations: [AcordosPage]
})
export class AcordosPageModule {}
