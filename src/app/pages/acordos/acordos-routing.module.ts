import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AcordosPage } from './acordos.page';

const routes: Routes = [
  {
    path: '',
    component: AcordosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AcordosPageRoutingModule {}
