import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CorridaPage } from './corrida.page';

describe('CorridaPage', () => {
  let component: CorridaPage;
  let fixture: ComponentFixture<CorridaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorridaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CorridaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
