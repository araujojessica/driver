import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { CorridaPage } from './corrida.page';

import { CorridaPageRoutingModule } from './corrida-routing.module';
import { PagamentosPage } from '../pagamentos/pagamentos.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CorridaPageRoutingModule
  ],
  declarations: [CorridaPage],
  entryComponents: []
})
export class HomePageModule {}
