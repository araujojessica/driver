import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { LoadingController, Platform } from '@ionic/angular';
import {Environment, Geocoder, GoogleMap, GoogleMapOptions, GoogleMaps, GoogleMapsAnimation, GoogleMapsEvent, ILatLng, Marker, MyLocation} from '@ionic-native/google-maps'
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,  
  ModalController, } from '@ionic/angular';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { PagamentosPage } from '../pagamentos/pagamentos.page';
import { UserService } from 'src/app/core/user/user.service';
import { User } from 'src/app/core/user/user';
import { Viagem } from 'src/app/interfaces/viagem';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { ViagemService } from 'src/app/services/viagem/viagemService';
import polling from 'rx-polling';

declare var google: any;

@Component({
  selector: 'app-corrida',
  templateUrl: 'corrida.page.html',
  styleUrls: ['corrida.page.scss'],
})
export class CorridaPage implements OnInit{

  @ViewChild('map',{static:true}) mapElement: any;
  loading: any;
  map: GoogleMap;
  search: string = '';
  searchOrigem: string = '';
  googleAutocomplete = new google.maps.places.AutocompleteService();
  searchResults = new Array<any>();
  searchResultsOrigem = new Array<any>();
  originMarker: Marker;
  originMarkerDiferente: Marker;
  destination: any;
  origem: any;
  googleDirectionsService = new google.maps.DirectionsService();
  matrix = new google.maps.DistanceMatrixService();
  valor = new Array<any>();
  endOrigin: any;
  corrida: any;
  tempoEstimadoCorrida: any;
  valorCorrida: any;
  user$: Observable<User>;
  valores:Object[] = [];
  usuarioLogado: User;
  user_id: number;
  viagem: Viagem;
  origemPass:string;
  destinoPass: string;
  id_viagem: string;
  latitude:any;
  longitude:any;

  constructor(
    private platform: Platform, 
    private loadCtrl: LoadingController,
    private ngZone: NgZone,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    private userService: UserService,
    public loadingController : LoadingController,
    private router: Router,
    private viagemService:ViagemService,
    private route: ActivatedRoute,
    private callNumber: CallNumber
    
    
  ) {

    this.user$ = userService.getUser();
 
    this.user$.subscribe( usuario => {
      this.usuarioLogado = usuario;
      this.user_id = this.usuarioLogado.id;
    });

    this.route.params.subscribe(parametros=> {
      this.id_viagem =  parametros['id_viagem'];
      


      this.viagemService.motoristaPassageiro(this.id_viagem).subscribe(viagem =>{
            this.viagem = viagem;
            this.calcRoute(viagem.origem);
       
      });

    const request$ = this.viagemService.viagem(this.id_viagem);
    const options = { interval: 10000 };

    polling(request$, options)
      .subscribe(async (v) => {
      //  
      if(v.status == 'CANCELADO'){  
        this.viagemService.cancelaPassageiro(this.id_viagem).subscribe();       
         this.router.navigate(['home']);
              const toast = await (await this.toastCtrl.create({
                message: 'VIAGEM CANCELADA PELO PASSAGEIRO!',
                duration: 15000, position: 'middle',
                color: 'danger'
              })).present();

         
         }
      }, (error) => {
        console.error(error);
      });
    
    });
    
    
    
    
    
    
   
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  ngOnInit(): void {

    this.mapElement = this.mapElement.nativeElement;
    this.mapElement.style.width = this.platform.width() + 'px';
    this.mapElement.style.height = this.platform.height() + 'px';
    this.loadMap();
   
    
  }

  async loadMap(){
    this.loading = await this.loadCtrl.create({message: 'Por favor aguarde ...'});
    await this.loading.present();
    
    Environment.setEnv({
      'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyC0inbEk4sOpHIT8lUyG3PI4XziAvl3CI0',
      'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyC0inbEk4sOpHIT8lUyG3PI4XziAvl3CI0'
    });

    const mapOptions: GoogleMapOptions = {
      controls: {
        zoom: false
      }
    }
    this.map = GoogleMaps.create(this.mapElement, mapOptions);

    try {
      await this.map.one(GoogleMapsEvent.MAP_READY);
      this.addOrigenMarker();    
      
    } catch (error) {
      console.error(error)
    }
      
  }
 
  async addOrigenMarker() {

    try {
      const myLocation: MyLocation = await this.map.getMyLocation();
      console.log('origem' + myLocation.latLng)
      await this.map.moveCamera({
        target: myLocation.latLng,
        zoom: 18
      }) ;
     
     this.originMarker=  this.map.addMarkerSync({
        title: 'Origem',
        icon: '#000',
        animation: GoogleMapsAnimation.DROP,
        position: myLocation.latLng
      });
    } catch (error) {
      console.error(error)
    }finally{
      this.loading.dismiss();
    }
    
  }

  markerDestination: Marker;

  async calcRouteOrigem(){
    const info: any = await Geocoder.geocode({address: this.origem.description})
    
    this.originMarker = this.map.addMarkerSync({
      title: this.origem.description,
      icon: '#000',
      animation: GoogleMapsAnimation.DROP,
      position: info[0].position
    });
   
  }
  async calcRoute(origem:any){
    this.search = '';
     
   this.destination = origem;
     const info: any = await Geocoder.geocode({address: this.destination})
      
     let markerDestination:Marker = this.map.addMarkerSync({
     title: this.destination,
     icon: '#000',
     animation: GoogleMapsAnimation.DROP,
     position: info[0].position

     
   });

    this.markerDestination = this.map.addMarkerSync({
    title: this.destination,
    icon: '#000',
    animation: GoogleMapsAnimation.DROP,
    position: info[0].position

    
  });
   this.googleDirectionsService.route({
     origin: this.originMarker.getPosition(),
     destination: markerDestination.getPosition(),
     travelMode: 'DRIVING'
   }, async results =>{
      const points = new Array<ILatLng>();
      const routes = results.routes[0].overview_path;
           
      for(let i = 0; i < routes.length; i++){
        points[i] = {
          lat: routes[i].lat(),
          lng: routes[i].lng()
        }
        
      }
      await this.map.addPolyline({
        points: points,
        color: 'red',
        width: 3
   });
   var origin1 = new google.maps.LatLng(this.originMarker.getPosition().lat, this.originMarker.getPosition().lng);
   var destinationB = new google.maps.LatLng(markerDestination.getPosition().lat, markerDestination.getPosition().lng);
    console.log('origem moto' + origin1)
    console.log('origem pass' +destinationB)
     this.map.moveCamera({target:points});
      this.map.panBy(0,100);
   });
 
  
  }
  inciarCorrida(){
    
    this.viagemService.iniciarCorrida(this.id_viagem).subscribe(viagem =>{     
        console.log('iniciar corrida: ' + viagem)    
     });
     this.router.navigate(['/corrida_final/'+this.id_viagem+''])
  }

  async back(){

    try {
      await this.map.clear();
      location.reload();
      this.addOrigenMarker();
    } catch (error) {
      console.error(error)
    }
  }


  ligar(n:string){
    this.callNumber.callNumber(n, true)
    .then(() => console.log('Launched dialer!'))
    .catch(() => console.log('Error launching dialer'));
  }

  
  cancelar(){

    this.viagemService.cancelaMotorista(this.id_viagem).subscribe( 
      
      async () => {
      this.router.navigate(['home']);
      const toast = await (await this.toastCtrl.create({
        message: 'Viagem Cancelada',
        duration: 4000, position: 'middle',
        color: 'success'
      })).present();
    },  
      async err => {
        console.log(err)
        location.reload();
        const toast = await (await this.toastCtrl.create({
          message: 'Erro ao cancelar Viagem',
          duration: 4000, position: 'middle',
          color: 'danger'
        })).present();
      })
  }

  cheguei(){
    this.viagemService.cheguei(this.id_viagem).subscribe(  async () => {
      const toast = await (await this.toastCtrl.create({
        message: 'Mensagem Cheguei enviada!',
        duration: 4000, position: 'middle',
        color: 'success'
      })).present();
    },  
      async err => {
        const toast = await (await this.toastCtrl.create({
          message: 'Erro ao cancelar Viagem',
          duration: 4000, position: 'middle',
          color: 'danger'
        })).present();
      })
  }

  navegar(){
    window.open('https://www.waze.com/ul?ll='+this.markerDestination.getPosition().lat+'%2C'+this.markerDestination.getPosition().lng+'&navigate=yes&zoom=17', '_system');
  }

  
}
