import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, MenuController, ModalController, NavController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { Foto } from 'src/app/interfaces/foto';
import { newUser } from 'src/app/interfaces/newUser';
import { EditarPerfilService } from 'src/app/services/user/editar-perfil';
import { RegisterService } from 'src/app/services/user/registerService';

@Component({
  selector: 'app-editar-perfil',
  templateUrl: './editar-perfil.page.html',
  styleUrls: ['./editar-perfil.page.scss'],
})
export class EditarPerfilPage implements OnInit {
  tipo: boolean;
  user$: Observable<User>;
  user: User;
  name:string =  '';
  secondName:string = '';
  password: string = '';
  foto: Foto;
  urlFoto: string = '/assets/img/teste-avatar.png' ;
  file: File;
  usuarioLogado: User;
  user_id: number;
  firstName:string;
  secondname:string;
  
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private router: Router,
    private userService: UserService,
    public toastCtrl: ToastController,
    private plt: Platform,
    private signupService: RegisterService,
    private editarService: EditarPerfilService
  ) {

    this.user$ = userService.getUser();
    this.user$.subscribe( pag => {
      this.usuarioLogado = pag;
      this.user_id = this.usuarioLogado.id;
      var a = this.usuarioLogado.full_name.split(" ");
      this.firstName = a[0];
      this.secondname = a[1];
      
    });

    this.signupService.findFotoMotorista(this.usuarioLogado.phone).subscribe(foto => {
      this.foto = foto;
      this.urlFoto = `http://localhost:3000/imgs/`+this.foto.url+``;    
    })
  }

  
  ngOnInit() {}

  salvar(){

  
    console.log(this.name + " = " + this.secondName + "/" + this.password);
    if(this.name != ''){

      this.editarService.updateNome(this.user_id, this.name, this.secondName).subscribe( async () => {
         const toast = await (await this.toastCtrl.create({
          message: 'Usuário editado com sucesso!',
          duration: 4000, position: 'top',
          color: 'success'
        })).present();
        this.router.navigate(['/home'])
        
      
      },  
      async err => {
        const toast = await (await this.toastCtrl.create({
          message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
          duration: 4000, position: 'top',
          color: 'danger'
        })).present();
      });

    }
    
    if(this.password != '') {
      this.editarService.updatePassword(this.user_id, this.password).subscribe( async () => {
        const toast = await (await this.toastCtrl.create({
         message: 'Usuário editado com sucesso!',
         duration: 4000, position: 'top',
         color: 'success'
       })).present();
       this.router.navigate(['/home'])
       
     
     },  
     async err => {
       const toast = await (await this.toastCtrl.create({
         message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
         duration: 4000, position: 'top',
         color: 'danger'
       })).present();
     });
    }

  }

  salvarFoto(){
    console.log(this.file)
    let allowComments = true;
    let description = this.user_id + '/ USER / PEFIL';

    this.signupService.updateFoto(description,allowComments, this.file).subscribe(async () => {
      const toast = await (await this.toastCtrl.create({
       message: 'Foto editada com sucesso!',
       duration: 4000, position: 'top',
       color: 'success'
     })).present();
     this.router.navigate(['/editar-perfil'])
     
   
   },  
   async err => {
     const toast = await (await this.toastCtrl.create({
       message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
       duration: 4000, position: 'top',
       color: 'danger'
     })).present();
   });

   
  }

  exibirOcultar(){
    this.tipo = !this.tipo;
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    this.menuCtrl.close();

  }

  ap(){

    if (document.getElementById("ap").style.display == 'none') {
      document.getElementById("ap").style.display = "block";
    } else {
      document.getElementById("ap").style.display = "none";
    }
  }

  close(){
    this.modalCtrl.dismiss();
  }
  

}
