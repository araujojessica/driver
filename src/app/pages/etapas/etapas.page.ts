import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-etapas',
  templateUrl: './etapas.page.html',
  styleUrls: ['./etapas.page.scss'],
})
export class EtapasPage implements OnInit {

  nome: string;
  phone:string;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {

    this.route.params.subscribe(parametros=> {
      this.nome =  parametros['nome'];
      this.phone =  parametros['phone'];
      console.log('parametro dentro de info : ' + parametros['phone'] );
    });
  }

  toAcordos(){
    this.router.navigate(['/acordos/'+this.phone+'']);
  }

  toFotoPerfil(){
    this.router.navigate(['/foto-perfil/'+this.phone+'']);
  }

  toCNH(){
    this.router.navigate(['/cnh/'+this.phone+'']);
  }

  toCRLV(){
    this.router.navigate(['/crlv/'+this.phone+'']);
  }
  
  login(){
    this.router.navigate(['/']);
  }

}
