import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EtapasPage } from './etapas.page';

describe('EtapasPage', () => {
  let component: EtapasPage;
  let fixture: ComponentFixture<EtapasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtapasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EtapasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
