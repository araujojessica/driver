import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { Foto } from 'src/app/interfaces/foto';
import { RegisterService } from 'src/app/services/user/registerService';
import { EditarPerfilPage } from '../editar-perfil/editar-perfil.page';
import { TaxasPage } from '../taxas/taxas.page';

@Component({
  selector: 'app-configuracoes',
  templateUrl: './configuracoes.page.html',
  styleUrls: ['./configuracoes.page.scss'],
})
export class ConfiguracoesPage implements OnInit {


  user$: Observable<User>;
  usuarioLogado: User;
  foto: Foto;
  urlFoto: string = '/assets/img/teste-avatar.png';
  file: File;
  adm: User;

  constructor(
    public modalCtrl: ModalController,
    private userService: UserService,
    private signupService:RegisterService
  ) { 

    this.user$ = userService.getUser();
    this.user$.subscribe( pag => {
      this.usuarioLogado = pag;  
      console.log(this.usuarioLogado.adm)    
    });

    this.signupService.findFotoMotorista(this.usuarioLogado.phone).subscribe(foto => {
      this.foto = foto;
      this.urlFoto = `https://uaileva.com.br/api/imgs/`+this.foto.url+``;    
    })

    this.signupService.findMotorista(this.usuarioLogado.phone).subscribe(adm => {
      this.adm = adm;
      console.log(this.adm.adm)  
    })

    
  }

  ngOnInit() {
  }

  close(){
    this.modalCtrl.dismiss();
  }

  async toEditarPerfil(){
    const modal = await this.modalCtrl.create({
      component: EditarPerfilPage
    });

    return await modal.present();
  }

  async toTaxas(){

    if(this.adm.adm == 'TRUE'){
        const modal = await this.modalCtrl.create({
          component: TaxasPage
        });

        return await modal.present();
    }

  }

}
