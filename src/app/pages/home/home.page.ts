import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { LoadingController, Platform } from '@ionic/angular';
import {Environment, Geocoder, GoogleMap, GoogleMapOptions, GoogleMaps, GoogleMapsAnimation, GoogleMapsEvent, ILatLng, Marker, MyLocation} from '@ionic-native/google-maps'
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,  
  ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { PagamentosPage } from '../pagamentos/pagamentos.page';
import { UserService } from 'src/app/core/user/user.service';
import { User } from 'src/app/core/user/user';
import { Viagem } from 'src/app/interfaces/viagem';
import { ViagemService } from 'src/app/services/viagem/viagemService';
import { CallNumber } from '@ionic-native/call-number/ngx';
import polling from 'rx-polling';
import { take } from 'rxjs-compat/operator/take';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';


declare var google: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  @ViewChild('map',{static:true}) mapElement: any;
  loading: any;
  map: GoogleMap;
  search: string = '';
  searchOrigem: string = '';
  googleAutocomplete = new google.maps.places.AutocompleteService();
  searchResults = new Array<any>();
  searchResultsOrigem = new Array<any>();
  originMarker: Marker;
  originMarkerDiferente: Marker;
  destination: any;
  origem: any;
  googleDirectionsService = new google.maps.DirectionsService();
  matrix = new google.maps.DistanceMatrixService();
  valor = new Array<any>();
  endOrigin: any;
  corrida: any;
  tempoEstimadoCorrida: any;
  valorCorrida: any;
  user$: Observable<User>;
  valores:Object[] = [];
  usuarioLogado: User;
  user_id: number;
  viagem: Viagem[];
  origemPassageiro: string;
  destinoPassaheiro: string;
  o:any;
  d:any;
  v: Viagem;
  u:User;
  ringtones:any=[];

  constructor(
    private platform: Platform, 
    private loadCtrl: LoadingController,
    private ngZone: NgZone,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    private userService: UserService,
    public loadingController : LoadingController,
    private router: Router,
    private viagemService:ViagemService,
    private callNumber: CallNumber,
    private nativeAudio: NativeAudio,
  ) {

    this.user$ = userService.getUser();
    
    this.user$.subscribe( usuario => {
      this.usuarioLogado = usuario;
      this.user_id = this.usuarioLogado.id;
    });

    const request$ = this.viagemService.allViagem();
    const options = { interval: 5000 };
    
   
    
      const r = this.viagemService.findMotorista(this.user_id);
      polling(r,options).subscribe((usuario) =>{
          this.u = usuario;
          
          if(this.u.logado == 'TRUE'){   
              document.getElementById("comecar").style.display = "none";
              document.getElementById("encerrar").style.display = "block";

              polling(request$, options)
              .subscribe((viagem) => {
                this.viagem = viagem; 
        
                for (var v of this.viagem) {

                   this.nativeAudio.preloadSimple('papaleguas','assets/ringtones/papaleguas.mp3');
                  this.nativeAudio.play('papaleguas').then(() => {
                    console.log('som')
                  })
                 
                  var a = v.origem.split(',');
                    v.origem  = a[0] + ',' + a[1];
        
                  var b = v.destino.split(',');
                    v.destino = b[0] + ',' + b[1] + ' - ' + b[2];
        
                  if(v.forma_pagamento == undefined || v.forma_pagamento == '' || v.forma_pagamento == 'undefined'){
                      v.forma_pagamento = 'dinheiro';
                  }
                  
                  
                   
                
             }
              }, (error) => {
                console.error(error);
              });


          }else{
             document.getElementById("comecar").style.display = "block";
            document.getElementById("encerrar").style.display = "none";
          }
           
          
      });

    



  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    this.user$ = this.userService.getUser();
    
    this.user$.subscribe( usuario => {
      this.usuarioLogado = usuario;
      this.user_id = this.usuarioLogado.id;
    });
  }

  

 
  ngOnInit(): void {

    this.mapElement = this.mapElement.nativeElement;
    this.mapElement.style.width = this.platform.width() + 'px';
    this.mapElement.style.height = this.platform.height() + 'px';
    
    this.loadMap();
    
  }

  async loadMap(){
    this.loading = await this.loadCtrl.create({message: 'Por favor aguarde ...'});
    await this.loading.present();
    
    Environment.setEnv({
      'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyC0inbEk4sOpHIT8lUyG3PI4XziAvl3CI0',
      'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyC0inbEk4sOpHIT8lUyG3PI4XziAvl3CI0'
    });

    const mapOptions: GoogleMapOptions = {
      controls: {
        zoom: false
      }
    }
    this.map = GoogleMaps.create(this.mapElement, mapOptions);

    try {
      await this.map.one(GoogleMapsEvent.MAP_READY);
      this.addOrigenMarker();
      
    } catch (error) {
      console.error(error)
    }
      
  }
 
  async addOrigenMarker() {

    try {
      const myLocation: MyLocation = await this.map.getMyLocation();
      await this.map.moveCamera({
        target: myLocation.latLng,
        zoom: 18
      }) ;
     
     this.originMarker=  this.map.addMarkerSync({
        title: 'Origem',
        icon: '#000',
        animation: GoogleMapsAnimation.DROP,
        position: myLocation.latLng
      });
    } catch (error) {
      console.error(error)
    }finally{
      this.loading.dismiss();
    }
    
  }

  searchChangedOrigem(){
    if (!this.searchOrigem.trim().length) return;
  
    this.googleAutocomplete.getPlacePredictions({input: this.searchOrigem}, predictions => {
      this.ngZone.run(() =>{
        this.searchResultsOrigem = predictions;
       
      });
    });
  }
  searchChanged(){
    if (!this.search.trim().length) return;

    this.googleAutocomplete.getPlacePredictions({input: this.search}, predictions => {
      this.ngZone.run(() =>{
        this.searchResults = predictions;
      });
    
      
    });
  }
  async calcRouteOrigem(item: any){
    this.origem = item;
    console.log(this.origem)
    const info: any = await Geocoder.geocode({address: this.origem.description})
    
    this.originMarkerDiferente = this.map.addMarkerSync({
      title: this.origem.description,
      icon: '#000',
      animation: GoogleMapsAnimation.DROP,
      position: info[0].position
    });
   
  }
  async calcRoute(item: any){
    this.search = '';
     
   this.destination = item;
     const info: any = await Geocoder.geocode({address: this.destination.description})

   let markerDestination: Marker = this.map.addMarkerSync({
     title: this.destination.description,
     icon: '#000',
     animation: GoogleMapsAnimation.DROP,
     position: info[0].position
   });

   if(this.originMarkerDiferente == undefined){
        this.originMarkerDiferente = this.originMarker;
    }

   this.googleDirectionsService.route({
     origin: this.originMarkerDiferente.getPosition(),
     destination: markerDestination.getPosition(),
     travelMode: 'DRIVING'
   }, async results =>{
      const points = new Array<ILatLng>();
      const routes = results.routes[0].overview_path;
     
      
      for(let i = 0; i < routes.length; i++){
        points[i] = {
          lat: routes[i].lat(),
          lng: routes[i].lng()
        }
        
      }
      await this.map.addPolyline({
        points: points,
        color: 'red',
        width: 3
   });
   var origin1 = new google.maps.LatLng(this.originMarkerDiferente.getPosition().lat, this.originMarkerDiferente.getPosition().lng);
   var destinationB = new google.maps.LatLng(markerDestination.getPosition().lat, markerDestination.getPosition().lng);
   
   this.matrix.getDistanceMatrix(
    {
      origins: [origin1],
      destinations: [destinationB],
      travelMode: google.maps.TravelMode.DRIVING
    }, callback);
  
  function callback(response, status) {
    let travelDetailsObject;
    if (status !== "OK") {
      alert("Error was: " + status);
    } else {
      var origins = response.originAddresses;
       var destinations = response.destinationAddresses;
       for (var i = 0; i < origins.length; i++) {
         var results = response.rows[i].elements;
         for (var j = 0; j < results.length; j++) {
            var element = results[j];
            var distance = element.distance.text;
            var duration = element.duration.text;
            var from = origins[i];
            var to = destinations[j];
            travelDetailsObject = {
               distance: distance,
               duration: duration
            }
         }
       }
       this.travelDetailsObject = travelDetailsObject;
       
       this.valor = this.travelDetailsObject;

       this.corrida = 'distancia : ' + this.travelDetailsObject.distance + ' no tempo de : ' + this.travelDetailsObject.duration;
          
    }
    
  }
  
      this.map.moveCamera({target:points});
      this.map.panBy(0,100);
   });
 
  
  }
  ligar(n:string){
    this.callNumber.callNumber(n, true)
    .then(() => console.log('Launched dialer!'))
    .catch(() => console.log('Error launching dialer'));
}

  async comecar(){
    const myLocation: MyLocation = await this.map.getMyLocation();
    this.viagemService.updateLogado(''+this.user_id).subscribe();
   // this.viagemService.localMotorista(''+this.user_id,myLocation.latLng.lat,myLocation.latLng.lng ).subscribe();
    
    
  }

  encerrar(){
    location.reload();
    this.viagemService.updateEncerrado(''+this.user_id).subscribe();   
  }
  aceitar(id_usuario:string,id_viagem: string){

   this.viagemService.aceitar(id_usuario,this.user_id,id_viagem).subscribe( 
      
      async () => {
      this.router.navigate(['corrida/'+id_viagem+'']);
      const toast = await (await this.toastCtrl.create({
        message: 'Iniciando a viagem',
        duration: 4000, position: 'middle',
        color: 'success'
      })).present();
    },  
      async err => {
        console.log(err)
        const toast = await (await this.toastCtrl.create({
          message: 'Viagem aceita por outro motorista!',
          duration: 4000, position: 'middle',
          color: 'danger'
        })).present();
      })
  }
 
 

  async back(){

    try {
      await this.map.clear();
      this.destination = null;
      this.origem = null;
      this.searchOrigem = null;
      document.getElementById("collapseExample").style.display == 'none'
      
      this.addOrigenMarker();
    } catch (error) {
      console.error(error)
    }
  }

  toggleTeste() {
    if (document.getElementById("collapseExample").style.display == 'none') {
        document.getElementById("collapseExample").style.display = "block";

    } else {
        document.getElementById("collapseExample").style.display = "none";
    }
  }

  toggleTeste1() {
    if (document.getElementById("collapseExample1").style.display == 'none') {
        document.getElementById("collapseExample1").style.display = "block";

    } else {
        document.getElementById("collapseExample1").style.display = "none";
    }
  }

  toggleCarregar() {
    if (document.getElementById("carregando").style.display == 'none') {
        document.getElementById("carregando").style.display = "block";

    } else {
        document.getElementById("carregando").style.display = "none";
    }
  }

  logout(){
   // this.userService.logout();
    this.router.navigate(['']);
  }

  async showModalPagamentos(){
    const modal = await this.modalCtrl.create({
      component: PagamentosPage
    });

    modal.present();
  }

  async aceitarViagem(){
    const alert = await this.alertCtrl.create({
      mode: 'md',
      cssClass: 'custom-viagem', //global.scss
      subHeader: '16:30',
      message: ` <p><b>Passageiro:</b>Jéssica</p>
                <p><b>Valor:</b> R$ 7,53</p>
                <p><b>Origem:</b> Av. Doutor Arthur Ribeiro Guimarães, 328, Jardim Noronha</p>
                <p><b>Destino:</b> Rua das Rosas, 355, Jardim Yara</p>`,
      buttons: [
        {
          text: 'Recusar',
          cssClass: 'custom-recusar',
          handler: () => {
            console.log('recusou viagem')
          }
        },
        {
          text: 'Aceitar',
          cssClass: 'custom-aceitar',
          handler: () => {
            console.log('aceitar viagem')
          }
        }
      ]
    });

    await alert.present();
  }

}
