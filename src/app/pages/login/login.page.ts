import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, MenuController, NavController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/core/auth/auth.service';
import { PlatformDectorService } from 'src/app/core/plataform-dector/plataform-dector.service';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { RecuperarSenhaService } from 'src/app/services/user/recuperar-senha';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  tipo: boolean;
  public onLoginForm: FormGroup;
  users = [];
  nome: string; 
  senha: string;
 
  
  @ViewChild('userNameInput') userNameInput: ElementRef<HTMLInputElement>;
  constructor(
    public router: Router,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
	  private formBuilder: FormBuilder,
    private authService: AuthService,
    private platformDetectorService: PlatformDectorService,
    private recuperarSenha: RecuperarSenhaService

  ) { 
   
    

  }

  ngOnInit() {

    this.onLoginForm = this.formBuilder.group({
      'userName': [null, Validators.compose([Validators.required])],
      'password': [null, Validators.compose([Validators.required])]
    });
   
    this.nome = window.localStorage.getItem('nome');
    this.senha = window.localStorage.getItem('senha');
  }

  mostrarSenha(){
    this.tipo = !this.tipo;
  }

  async goToHome(){
    const userName = this.onLoginForm.get('userName').value;
    const password = this.onLoginForm.get('password').value;

    window.localStorage.setItem('nome', this.onLoginForm.get('userName').value);
    window.localStorage.setItem('senha', this.onLoginForm.get('password').value);
    
    if(userName == null || password == null){
      const toast = await this.toastCtrl.create({
        message:'Campos obrigatórios.', 
        duration:4000, position:'top',
        color:'danger'
      });
      toast.present();
    }

    var a = userName.replace('-','').replace(' ','').replace('(','').replace(')','');

   this.authService.authenticate(a, password).subscribe(
            () => this.navCtrl.navigateRoot('/home'),
            async err => {
                console.log('entrou aqui' + err.message);               
                this.onLoginForm.reset();
                this.platformDetectorService.isPlataformBrowser() ;
                const toast = await this.toastCtrl.create({
                        message:'Se já fez seu cadastro aguarde liberação, caso contrário venha fazer parte do nosso time!', 
                        duration:4000, position:'top',
                        color:'danger'
                });               
               toast.present();

            }
            
        ); 
    }

    async forgotPass() {
      const alert = await this.alertCtrl.create({
        header: 'Esqueceu sua senha?',
        message: 'Informe seu e-mail para a recuperação.',
        inputs: [
          {
            name: 'email',
            type: 'email',
            placeholder: 'E-mail'
          }
        ],
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirmar Cancelamento?');
            }
          }, {
            text: 'Confirmar',
            handler: async data => {
              console.log(data.email)
              this.recuperarSenha.sendemail(data.email).subscribe(
                  async () =>{ 
                    console.log('entrou aqui')
                    this.navCtrl.navigateRoot('')
                    const toast = await this.toastCtrl.create({
                              
                      message:'Email enviado com sucesso!', 
                      duration:4000, position:'top',
                      color:'success'
                      });
              
                    
                    toast.present();
                  },
                  async err => {
               
                     const toast = await this.toastCtrl.create({
                              
                              message:'Usuário não cadastrado, por favor cadastre-se no APP!', 
                              duration:4000, position:'top',
                              color:'danger'
                      });
              
                     
                     toast.present();
      
                  }
                  
              );
              
            }
          }
        ]
      });
  
      await alert.present();
  }

  cadastro(){
    this.router.navigate(['cadastro']);
  }

}
