export interface Viagem {

    id_viagem:string;
    id_usuario: string;
    origem: string;
    destino: string;
    valor_corrida: string;
    distancia: string;
    tempo_estimado: string;
    status: string;
    forma_pagamento: string;
    data_corrida: string;
    user_id:  string;
    user_email: string;
    user_password: string;
    user_full_name: string;
    user_phone: string;
    photo: string;
    logado: string;
    data_logado: string;
    user_date: string;

}