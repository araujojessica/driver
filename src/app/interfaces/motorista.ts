export interface Motorista {
  id: string;
  email: string;
  password: string;
  full_name: string;
  cidade: string;
  phone: string;
  photo: string;
  photo_cnh: string;
  photo_crlv: string;
  logado: string;
  status: string;
  acordo: string;
  data_logado: string;
  date_cadastro: string;
  id_motorista: string;
  latitude: string;
  longitude: string;
  date_logado: string;
}