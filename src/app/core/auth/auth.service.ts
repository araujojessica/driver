import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {map, tap} from 'rxjs/operators';
import { UserService } from '../user/user.service';

const API_URL = 'https://uaileva.com.br/api';
//const API_URL = "http://191.252.3.12:3000"; 
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http : HttpClient, private userService: UserService) { }

  authenticate( userName:string, password: string) {
    console.log(`${API_URL}/motorista/login`)
    return this.http.post(`${API_URL}/motorista/login`,{userName, password},{observe:'response'})
      .pipe(tap(res => {
        const authToken = res.headers.get('x-access-token');
        this.userService.setToken(authToken);
        console.log(`User ${userName} authenticated with token ${authToken}`)
      }))
      
  }

  findAllUsers() {

    return this.http.get(`${API_URL}/motorista/findAllUsers`,{observe:'response'})
    .pipe(tap(res => {

      console.log(`All users` + res)
    }))

  }
 

 
}