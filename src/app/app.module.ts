import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
@NgModule({
  declarations: [
    AppComponent
  ],
  entryComponents: [
  ],
  imports: [
    BrowserModule,
   
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
   
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CallNumber,NativeAudio,
    { provide: LocationStrategy,useClass: HashLocationStrategy},
    Geolocation
 
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
