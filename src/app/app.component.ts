import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, ModalController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { MensagensPage } from './pages/mensagens/mensagens.page';
import { MinhasViagensPage } from './pages/minhas-viagens/minhas-viagens.page';
import { MeusPagamentosPage } from './pages/meus-pagamentos/meus-pagamentos.page';
import { AjudaPage } from './pages/ajuda/ajuda.page';
import { ConfiguracoesPage } from './pages/configuracoes/configuracoes.page';
import { AvaliacoesPage } from './pages/avaliacoes/avaliacoes.page';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { Foto } from './interfaces/foto';
import { RegisterService } from './services/user/registerService';
import { CarteiraPage } from './pages/carteira/carteira.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  user$: Observable<User>;
  usuarioLogado: User;
  foto: Foto;
  urlFoto: string = '/assets/img/teste-avatar.png';
  file: File;
  
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public menuCtrl: MenuController,
    public router: Router,
    public modalCtrl: ModalController,
    private userService: UserService,
    private signupService: RegisterService
  ) {
    this.initializeApp();
    this.user$ = userService.getUser();
    this.user$.subscribe( pag => {
      this.usuarioLogado = pag;   
      this.signupService.findFotoMotorista(this.usuarioLogado.phone).subscribe(foto => {
        this.foto = foto;
        this.urlFoto = `https://uaileva.com.br/api/imgs/`+this.foto.url+``;    
      })    
    });

  
  }

  initializeApp(){
    this.platform.ready().then ( () => {
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(true);
      this.statusBar.backgroundColorByHexString('#001c4e');
      this.splashScreen.hide();
    });
  }

  async showModalMensagens(){
    const modal = await this.modalCtrl.create({
      component: MensagensPage
    });

    return await modal.present();
  }

  async toMinhasViagens(){
    const modal = await this.modalCtrl.create({
      component: MinhasViagensPage
    });

    return await modal.present();
  }

  async toCarteira(){
    const modal = await this.modalCtrl.create({
      component: CarteiraPage
    });

    return await modal.present();
  }

  async toMeusPagamentos(){
    const modal = await this.modalCtrl.create({
      component: MeusPagamentosPage
    });

    return await modal.present();
  }

  async toAjuda(){
    const modal = await this.modalCtrl.create({
      component: AjudaPage
    });

    return await modal.present();
  }

  async toConfiguracoes(){
    const modal = await this.modalCtrl.create({
      component: ConfiguracoesPage
    });

    return await modal.present();
  }

  async toAvaliacoes(){
    const modal = await this.modalCtrl.create({
      component: AvaliacoesPage
    });

    return await modal.present();
  }

  logout(){
    this.userService.logout();
    localStorage.removeItem('currentUser');
    this.router.navigate(['']);
    this.menuCtrl.close();
    navigator['app'].exitApp();
  }
  /*funcaoRota(){
    this.router.navigate(['/funcaoRota']);
    this.menuCtrl.toggle();
  }*/
}
