import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { newUser } from "../../interfaces/newUser";
import { User } from "src/app/core/user/user";
import { Foto } from "src/app/interfaces/foto";


const API_URL = "https://uaileva.com.br/api"; 
//const API_URL = "http://191.252.3.12:3000"; 

@Injectable({providedIn: 'root'})
export class EditarPerfilService{

    constructor(private http: HttpClient){}

    updateNome(user_id:number, nome:string,secondName:string){
        
        return this.http.post(`${API_URL}/motorista/updateNome`,{user_id,nome,secondName})
    };

    updatePassword(user_id:number, password:string){        
        return this.http.post(`${API_URL}/motorista/updatePassword`,{user_id,password})
    };
   



}