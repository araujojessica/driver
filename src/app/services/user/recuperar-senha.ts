import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { newUser } from "../../interfaces/newUser";
import { User } from "src/app/core/user/user";
import { Foto } from "src/app/interfaces/foto";


const API_URL = "https://uaileva.com.br/api"; 
//const API_URL = "http://191.252.3.12:3000"; 

@Injectable({providedIn: 'root'})
export class RecuperarSenhaService{

    constructor(private http: HttpClient){}

    sendemail(email:string){
        console.log(email)
        return this.http.post(`${API_URL}/motorista/sendEmail`,{email})
    };
   



}