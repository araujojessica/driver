import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Viagem } from "src/app/interfaces/viagem";
import {Observable} from 'rxjs/Rx';
import { User } from "src/app/core/user/user";



const API_URL = "https://uaileva.com.br/api"; 
//const API_URL = "http://191.252.3.12:3000"; 

@Injectable({providedIn: 'root'})
export class ViagemService {

    constructor(   
        private http: HttpClient
        ){}
    
   
    signup(id_usuario: string,origem:string, destino:string,valor:string, distance:string, duration:string, pagamento:string){
       return this.http.get(API_URL +  `/viagem/signup/`+id_usuario+`/`+origem+`/`+destino+`/`+valor+`/`+distance+`/`+duration+`/`+pagamento+``);
    }

    cancelaViagem(id_usuario){
        return this.http.get(API_URL +  `/viagem/cancela/`+id_usuario+``);
    }

    findViagem(id_usuario){
        return this.http.get<Viagem>(API_URL +  `/viagem/findViagem/`+id_usuario+``);
    }

    allViagem(){
        return this.http.get<Viagem[]>(API_URL +  `/allViagens/`);
    }

    viagem(id_viagem){
        return this.http.get<Viagem>(API_URL +  `/viagem/`+id_viagem);
    }

    updateLogado(user_id:string){
        return this.http.get(API_URL +  `/motorista/logado/`+user_id);
    }

    updateEncerrado(user_id:string){
        return this.http.get(API_URL +  `/motorista/encerrado/`+user_id);
    }

    localMotorista(id_motorista,latitude,longitude){
        return this.http.get(API_URL +  `/motorista/local/`+id_motorista+`/`+latitude+`/`+longitude+``);
    }

    aceitar(id_usuario,id_motorista,id_viagem){
        return this.http.get(API_URL +  `/viagem/aceita/`+id_usuario+`/`+id_motorista+`/`+id_viagem+``);
    }

    findViagemUsuario(id_viagem){
        console.log(id_viagem)
        return this.http.get<Viagem>(API_URL +  `/viagem/aceitada/`+id_viagem+``);
    }

    cancelaMotorista(id_viagem){
        return this.http.get<Viagem>(API_URL +  `/viagem/cancelaMotorista/`+id_viagem+``);
    }

    cancelaPassageiro(id_viagem){
        return this.http.get<Viagem>(API_URL +  `/viagem/cancelaPassageiro/`+id_viagem+``);
    }

  

    findMotorista(id_motorista){
        return this.http.get<User>(API_URL +  `/motorista/achado/`+id_motorista+``);
    }

    carteiraMotorista(id_motorista){
        return this.http.get<any>(API_URL +  `/carteira/`+id_motorista+``);
    }

    motoristaPassageiro(id_viagem){
        console.log(id_viagem)
        return this.http.get<Viagem>(API_URL +  `/viagem/motorista_passageiro/`+id_viagem+``);
    }
    encerraViagem(id_viagem){
        console.log(id_viagem)
        return this.http.get<Viagem>(API_URL +  `/viagem/encerrada/`+id_viagem+``);
    }

    iniciarCorrida(id_viagem){

        return this.http.get<any>(API_URL +  `/viagem/iniciarCorrida/`+id_viagem+``);
    }

    finalCorrida(id_viagem){

        return this.http.get<any>(API_URL +  `/viagem/finalCorrida/`+id_viagem+``);
    }

    cheguei(id_viagem){

        return this.http.get<any>(API_URL +  `/viagem/cheguei/`+id_viagem+``);
    }


   

   





    


}