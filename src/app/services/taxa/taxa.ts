import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { newUser } from "../../interfaces/newUser";
import { User } from "src/app/core/user/user";
import { Foto } from "src/app/interfaces/foto";


const API_URL = "https://uaileva.com.br/api"; 
//const API_URL = "http://191.252.3.12:3000"; 

@Injectable({providedIn: 'root'})
export class TaxaService{

    constructor(private http: HttpClient){}

    updatedinamica(dinamica){
        
        return this.http.get(`${API_URL}/taxa/register/`+dinamica+``)
    };

    updatevalorBase(valor_base){        
        return this.http.get(`${API_URL}/taxa/valorbase/`+valor_base+``)
    };
    updatevalorKm(valor_km){        
        return this.http.get(`${API_URL}/taxa/valorkm/`+valor_km+``)
    };
    updatevalorMin(valor_min){        
        return this.http.get(`${API_URL}/taxa/valormin/`+valor_min+``)
    };

    taxa(){        
        return this.http.get<any>(`${API_URL}/taxa/`)
    };
   



}