import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { newUser } from "../../interfaces/newUser";
import { Viagem } from "src/app/interfaces/viagem";
import { Pagamento } from "src/app/interfaces/pagamento";


const API_URL = "https://uaileva.com.br/api"; 
//const API_URL = "http://191.252.3.12:3000"; 

@Injectable({providedIn: 'root'})
export class PagamentoService{

    constructor(   
        private http: HttpClient
        ){}
    
   
    signup(id: number, escolhido: string){
       return this.http.get(API_URL +  `/user/pagamento/`+id+`/`+escolhido+``);
    }
    pagamentoUsuario(id_usuario: string) {
          return this.http.get<Pagamento>(API_URL+`/user/pagamento/`+id_usuario+``);       
    }

    


}